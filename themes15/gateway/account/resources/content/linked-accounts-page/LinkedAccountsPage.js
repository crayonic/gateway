function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright 2019 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as React from "../../../../common/keycloak/web_modules/react.js";
import { withRouter } from "../../../../common/keycloak/web_modules/react-router-dom.js";
import { Badge, Button, DataList, DataListAction, DataListItemCells, DataListCell, DataListItemRow, Stack, StackItem, Title, TitleLevel, DataListItem } from "../../../../common/keycloak/web_modules/@patternfly/react-core.js";
import { BitbucketIcon, CubeIcon, FacebookIcon, GithubIcon, GitlabIcon, GoogleIcon, InstagramIcon, LinkIcon, LinkedinIcon, MicrosoftIcon, OpenshiftIcon, PaypalIcon, StackOverflowIcon, TwitterIcon, UnlinkIcon } from "../../../../common/keycloak/web_modules/@patternfly/react-icons.js";
import { AccountServiceContext } from "../../account-service/AccountServiceContext.js";
import { Msg } from "../../widgets/Msg.js";
import { ContentPage } from "../ContentPage.js";
import { createRedirect } from "../../util/RedirectUri.js";

/**
 * @author Stan Silvert
 */
class LinkedAccountsPage extends React.Component {
  constructor(props, context) {
    super(props);

    _defineProperty(this, "context", void 0);

    this.context = context;
    this.state = {
      linkedAccounts: [],
      unLinkedAccounts: []
    };
    this.getLinkedAccounts();
  }

  getLinkedAccounts() {
    this.context.doGet("/linked-accounts").then(response => {
      console.log({
        response
      });
      const linkedAccounts = response.data.filter(account => account.connected);
      const unLinkedAccounts = response.data.filter(account => !account.connected);
      this.setState({
        linkedAccounts: linkedAccounts,
        unLinkedAccounts: unLinkedAccounts
      });
    });
  }

  unLinkAccount(account) {
    const url = '/linked-accounts/' + account.providerName;
    this.context.doDelete(url).then(response => {
      console.log({
        response
      });
      this.getLinkedAccounts();
    });
  }

  linkAccount(account) {
    const url = '/linked-accounts/' + account.providerName;
    const redirectUri = createRedirect(this.props.location.pathname);
    this.context.doGet(url, {
      params: {
        providerId: account.providerName,
        redirectUri
      }
    }).then(response => {
      console.log({
        response
      });
      window.location.href = response.data.accountLinkUri;
    });
  }

  render() {
    return (/*#__PURE__*/React.createElement(ContentPage, {
        title: Msg.localize('linkedAccountsTitle'),
        introMessage: Msg.localize('linkedAccountsIntroMessage')
      }, /*#__PURE__*/React.createElement(Stack, {
        gutter: "md"
      }, /*#__PURE__*/React.createElement(StackItem, {
        isFilled: true
      }, /*#__PURE__*/React.createElement(Title, {
        headingLevel: TitleLevel.h2,
        size: "2xl"
      }, /*#__PURE__*/React.createElement(Msg, {
        msgKey: "linkedLoginProviders"
      })), /*#__PURE__*/React.createElement(DataList, {
        id: "linked-idps",
        "aria-label": "foo"
      }, this.makeRows(this.state.linkedAccounts, true))), /*#__PURE__*/React.createElement(StackItem, {
        isFilled: true
      }), /*#__PURE__*/React.createElement(StackItem, {
        isFilled: true
      }, /*#__PURE__*/React.createElement(Title, {
        headingLevel: TitleLevel.h2,
        size: "2xl"
      }, /*#__PURE__*/React.createElement(Msg, {
        msgKey: "unlinkedLoginProviders"
      })), /*#__PURE__*/React.createElement(DataList, {
        id: "unlinked-idps",
        "aria-label": "foo"
      }, this.makeRows(this.state.unLinkedAccounts, false)))))
    );
  }

  emptyRow(isLinked) {
    let isEmptyMessage = '';

    if (isLinked) {
      isEmptyMessage = Msg.localize('linkedEmpty');
    } else {
      isEmptyMessage = Msg.localize('unlinkedEmpty');
    }

    return (/*#__PURE__*/React.createElement(DataListItem, {
        key: "emptyItem",
        "aria-labelledby": "empty-item"
      }, /*#__PURE__*/React.createElement(DataListItemRow, {
        key: "emptyRow"
      }, /*#__PURE__*/React.createElement(DataListItemCells, {
        dataListCells: [/*#__PURE__*/React.createElement(DataListCell, {
          key: "empty"
        }, /*#__PURE__*/React.createElement("strong", null, isEmptyMessage))]
      })))
    );
  }

  makeRows(accounts, isLinked) {
    if (accounts.length === 0) {
      return this.emptyRow(isLinked);
    }

    return (/*#__PURE__*/React.createElement(React.Fragment, null, " ", accounts.map(account => /*#__PURE__*/React.createElement(DataListItem, {
        id: `${account.providerAlias}-idp`,
        key: account.providerName,
        "aria-labelledby": "simple-item1"
      }, /*#__PURE__*/React.createElement(DataListItemRow, {
        key: account.providerName
      }, /*#__PURE__*/React.createElement(DataListItemCells, {
        dataListCells: [/*#__PURE__*/React.createElement(DataListCell, {
          key: "idp"
        }, /*#__PURE__*/React.createElement(Stack, null, /*#__PURE__*/React.createElement(StackItem, {
          isFilled: true
        }, this.findIcon(account)), /*#__PURE__*/React.createElement(StackItem, {
          id: `${account.providerAlias}-idp-name`,
          isFilled: true
        }, /*#__PURE__*/React.createElement("h2", null, /*#__PURE__*/React.createElement("strong", null, account.displayName))))), /*#__PURE__*/React.createElement(DataListCell, {
          key: "badge"
        }, /*#__PURE__*/React.createElement(Stack, null, /*#__PURE__*/React.createElement(StackItem, {
          isFilled: true
        }), /*#__PURE__*/React.createElement(StackItem, {
          id: `${account.providerAlias}-idp-badge`,
          isFilled: true
        }, this.badge(account)))), /*#__PURE__*/React.createElement(DataListCell, {
          key: "username"
        }, /*#__PURE__*/React.createElement(Stack, null, /*#__PURE__*/React.createElement(StackItem, {
          isFilled: true
        }), /*#__PURE__*/React.createElement(StackItem, {
          id: `${account.providerAlias}-idp-username`,
          isFilled: true
        }, account.linkedUsername)))]
      }), /*#__PURE__*/React.createElement(DataListAction, {
        "aria-labelledby": "foo",
        "aria-label": "foo action",
        id: "setPasswordAction"
      }, isLinked && /*#__PURE__*/React.createElement(Button, {
        id: `${account.providerAlias}-idp-unlink`,
        variant: "link",
        onClick: () => this.unLinkAccount(account)
      }, /*#__PURE__*/React.createElement(UnlinkIcon, {
        size: "sm"
      }), " ", /*#__PURE__*/React.createElement(Msg, {
        msgKey: "unLink"
      })), !isLinked && /*#__PURE__*/React.createElement(Button, {
        id: `${account.providerAlias}-idp-link`,
        variant: "link",
        onClick: () => this.linkAccount(account)
      }, /*#__PURE__*/React.createElement(LinkIcon, {
        size: "sm"
      }), " ", /*#__PURE__*/React.createElement(Msg, {
        msgKey: "link"
      })))))), " ")
    );
  }

  badge(account) {
    if (account.social) {
      return (/*#__PURE__*/React.createElement(Badge, null, /*#__PURE__*/React.createElement(Msg, {
          msgKey: "socialLogin"
        }))
      );
    }

    return (/*#__PURE__*/React.createElement(Badge, {
        style: {
          backgroundColor: "green"
        }
      }, /*#__PURE__*/React.createElement(Msg, {
        msgKey: "systemDefined"
      }))
    );
  }

  findIcon(account) {
    const socialIconId = `${account.providerAlias}-idp-icon-social`;
    if (account.providerName.toLowerCase().includes('github')) return (/*#__PURE__*/React.createElement(GithubIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('linkedin')) return (/*#__PURE__*/React.createElement(LinkedinIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('facebook')) return (/*#__PURE__*/React.createElement(FacebookIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('google')) return (/*#__PURE__*/React.createElement(GoogleIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('instagram')) return (/*#__PURE__*/React.createElement(InstagramIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('microsoft')) return (/*#__PURE__*/React.createElement(MicrosoftIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('bitbucket')) return (/*#__PURE__*/React.createElement(BitbucketIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('twitter')) return (/*#__PURE__*/React.createElement(TwitterIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('openshift')) return (/*#__PURE__*/React.createElement(OpenshiftIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('gitlab')) return (/*#__PURE__*/React.createElement(GitlabIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('paypal')) return (/*#__PURE__*/React.createElement(PaypalIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    if (account.providerName.toLowerCase().includes('stackoverflow')) return (/*#__PURE__*/React.createElement(StackOverflowIcon, {
        id: socialIconId,
        size: "xl"
      })
    );
    return (/*#__PURE__*/React.createElement(CubeIcon, {
        id: `${account.providerAlias}-idp-icon-default`,
        size: "xl"
      })
    );
  }

}

_defineProperty(LinkedAccountsPage, "contextType", AccountServiceContext);

;
const LinkedAccountsPagewithRouter = withRouter(LinkedAccountsPage);
export { LinkedAccountsPagewithRouter as LinkedAccountsPage };
//# sourceMappingURL=LinkedAccountsPage.js.map