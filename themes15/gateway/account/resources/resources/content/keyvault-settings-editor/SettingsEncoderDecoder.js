// const TIMEOUT_ENUM_VALUES = [
//   "10 SEC",
//   "20 SEC",
//   "30 SEC",
//   "45 SEC",
//   "1 MIN",
//   "2 MIN",
//   "5 MIN",
//   "10 MIN",
//   "15 MIN",
//   "20 MIN",
//   "30 MIN",
//   "45 MIN",
//   "1 HR",
//   "2 HRS",
//   "3 HRS",
//   "4 HRS",
//   "5 HRS",
//   "8 HRS",
//   "12 HRS",
//   "24 HRS",
//   "NO LIMIT"
// ].map((label, index) => ({ value: index, label }));
const TIMEOUT_ENUM_VALUES = [{
  value: 10,
  label: "10 seconds"
}, {
  value: 20,
  label: "20 seconds"
}, {
  value: 30,
  label: "30 seconds"
}, {
  value: 45,
  label: "45 seconds"
}, {
  value: 1 * 60,
  label: "1 minute"
}, {
  value: 2 * 60,
  label: "2 minutes"
}, {
  value: 5 * 60,
  label: "5 minutes"
}, {
  value: 10 * 60,
  label: "10 minutes"
}, {
  value: 15 * 60,
  label: "15 minutes"
}, {
  value: 20 * 60,
  label: "20 minutes"
}, {
  value: 30 * 60,
  label: "30 minutes"
}, {
  value: 45 * 60,
  label: "45 minutes"
}, {
  value: 1 * 3600,
  label: "1 hour"
}, {
  value: 2 * 3600,
  label: "2 hours"
}, {
  value: 3 * 3600,
  label: "3 hours"
}, {
  value: 4 * 3600,
  label: "4 hours"
}, {
  value: 5 * 3600,
  label: "5 hours"
}, {
  value: 8 * 3600,
  label: "8 hours"
}, {
  value: 12 * 3600,
  label: "12 hours"
}, {
  value: 24 * 3600,
  label: "24 hours"
}, {
  value: 0xFFFFFF,
  label: "No limit"
}];
const TIMEZONE_OFFSET_ENUM_VALUES = [...Array(49).keys()].map(i => ({
  value: (i - 24) * 60,
  label: "UTC+" + (i - 24)
}));
const U2F_OS_ENUM_VALUES = ["U2F_OS_OTHER", "U2F_OS_LINUX_MACOS"].map((label, index) => ({
  value: index,
  label
}));
const UP_UV_ENUM_VALUES = [{
  value: 0,
  label: "UP_UV_OPTIONS_AUTO"
}, {
  value: 1,
  label: "UP_UV_OPTIONS_TOUCH"
}, {
  value: 2,
  label: "UP_UV_OPTIONS_FINGERPRINT"
}, {
  value: 3,
  label: "UP_UV_OPTIONS_PASSCODE"
} // { value: 4, label: "UP_UV_OPTIONS_HANDWRITING", coming_soon: true},
// { value: 5, label: "UP_UV_OPTIONS_VOICE", coming_soon: true},
];
const BOOL_ENUM_VALUES = [{
  value: true,
  label: "ON"
}, {
  value: false,
  label: "OFF"
}];
const SettingsPropertiesDefinitions = Object.entries({
  time: {
    tlv_type: 0,
    data_type: "uint40",
    default_value: 0
  },
  uv_timeout_index: {
    tlv_type: 1,
    data_type: "uint24minmax",
    enum_values: TIMEOUT_ENUM_VALUES,
    default_value: {
      min: 10,
      max: 10
    }
  },
  msc_timeout_index: {
    tlv_type: 2,
    data_type: "uint24minmax",
    enum_values: TIMEOUT_ENUM_VALUES,
    default_value: {
      min: 0xFFFFFF,
      max: 0xFFFFFF
    }
  },
  unlock_timeout_index: {
    tlv_type: 3,
    data_type: "uint24minmax",
    enum_values: TIMEOUT_ENUM_VALUES,
    default_value: {
      min: 20,
      max: 20
    }
  },
  u2f_os: {
    tlv_type: 4,
    data_type: "uint8",
    enum_values: U2F_OS_ENUM_VALUES,
    default_value: 1
  },
  u2f_up: {
    tlv_type: 5,
    data_type: "uint8",
    multiselect: true,
    exclusive_values: [0],
    enum_values: UP_UV_ENUM_VALUES,
    default_value: [1]
  },
  fido_up: {
    tlv_type: 6,
    data_type: "uint8",
    multiselect: true,
    exclusive_values: [0],
    enum_values: UP_UV_ENUM_VALUES,
    default_value: [1]
  },
  fido_uv: {
    tlv_type: 7,
    data_type: "uint8",
    multiselect: true,
    exclusive_values: [0],
    enum_values: UP_UV_ENUM_VALUES,
    default_value: [2, 3]
  },
  ble_enabled: {
    tlv_type: 8,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: true
  },
  ble_use_rpas: {
    tlv_type: 9,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: false
  },
  export_fingerprints_always: {
    tlv_type: 10,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: false
  },
  export_handwriting_always: {
    tlv_type: 11,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: false
  },
  nfc_to_st_safe_bridge: {
    tlv_type: 12,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: true
  },
  client_pin: {
    tlv_type: 13,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: false
  },
  logging_enabled: {
    tlv_type: 14,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: false
  },
  fido_over_usb: {
    tlv_type: 15,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: true
  },
  ccid_over_usb: {
    tlv_type: 16,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: true
  },
  nfc_enabled: {
    tlv_type: 17,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: true
  },
  ble_pairing_over_nfc: {
    tlv_type: 18,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: true
  },
  fido_over_nfc: {
    tlv_type: 19,
    data_type: "bool",
    enum_values: BOOL_ENUM_VALUES,
    default_value: true
  } // timezone_offset: {
  //   tlv_type: 20,
  //   data_type: "int16",
  //   enum_values: TIMEZONE_OFFSET_ENUM_VALUES,
  //   default_value: -(new Date()).getTimezoneOffset()
  // }

}).map(entry => ({
  name: entry[0],
  ...entry[1]
}));
const tlv_to_property = Object.assign({}, {}, ...SettingsPropertiesDefinitions.map(property => ({
  [property.tlv_type]: property
})));

function encodeSettings(settings) {
  return encodeSettingsV1(settings);
}

function encodeSettingsV1(settings) {
  let buffer = [];
  buffer.push(1); // version 1

  for (const property of SettingsPropertiesDefinitions) {
    if (property.name in settings && settings.selected[property.name]) {
      let values = [];

      if (property.multiselect) {
        values = settings[property.name];
      } else {
        values = [settings[property.name]];
      }

      for (const value of values) {
        buffer.push(property.tlv_type);

        if (property.data_type === "bool") {
          buffer.push(value); //https://stackoverflow.com/questions/7820683/convert-boolean-result-into-number-integer
        } else if (property.data_type === "uint8") {
          buffer.push(value);
        } else if (property.data_type === "int16") {
          if (value > 0) {
            buffer.push((value >> 8) % 256);
            buffer.push((value >> 0) % 256);
          } else {
            buffer.push((0xFFFF + value + 1 >> 8) % 256);
            buffer.push((0xFFFF + value + 1 >> 0) % 256);
          }
        } else if (property.data_type === "uint24minmax") {
          let {
            min,
            max
          } = value;
          min = min === 0xFFFFFF ? 0 : min;
          max = max === 0xFFFFFF ? 0 : max; // min

          buffer.push((min >> 16) % 256);
          buffer.push((min >> 8) % 256);
          buffer.push((min >> 0) % 256); // max

          buffer.push((max >> 16) % 256);
          buffer.push((max >> 8) % 256);
          buffer.push((max >> 0) % 256);
        } else if (property.data_type === "uint32") {
          buffer.push((value >> 24) % 256);
          buffer.push((value >> 16) % 256);
          buffer.push((value >> 8) % 256);
          buffer.push((value >> 0) % 256);
        } else if (property.data_type === "uint40") {
          buffer.push(0); // TODO fix for unsigned values greater than 2**32-1

          buffer.push((value >> 24) % 256);
          buffer.push((value >> 16) % 256);
          buffer.push((value >> 8) % 256);
          buffer.push((value >> 0) % 256);
        } else {
          throw "Unknown data type to encode " + property.data_type;
        }
      }
    }
  }

  return Uint8Array.from(buffer);
}

function decodeSettings(buffer) {
  return decodeSettingsV1(buffer);
}

function decodeSettingsV1(buffer) {
  // console.log(SettingsPropertiesDefinitions);
  // console.log(tlv_to_property);
  // console.log(buffer[0], buffer);
  let settings = {};

  if (buffer[0] !== 1) {
    throw "Unsupported version";
  }

  for (let i = 1; i < buffer.length;) {
    // console.log(i, buffer.length);
    // console.log(JSON.stringify(settings));
    if (buffer[i] in tlv_to_property) {
      const property = tlv_to_property[buffer[i]];

      const addPropertyValue = value => {
        // console.log(property.name, settings[property.name], value);
        if (property.multiselect) {
          if (settings[property.name] === undefined) {
            settings[property.name] = [];
          } // console.log(settings[property.name]);


          settings[property.name].push(value);
        } else {
          settings[property.name] = value;
        }
      };

      if (property.data_type === "bool") {
        addPropertyValue(buffer[i + 1] ? true : false);
        i += 2;
      } else if (property.data_type === "uint8") {
        addPropertyValue(buffer[i + 1]);
        i += 2;
      } else if (property.data_type === "int16") {
        let temp_value = buffer[i + 1] * 256 + buffer[i + 2];

        if (temp_value >= 0xF000) {
          temp_value = temp_value - 0xF000;
        }

        addPropertyValue(temp_value);
        i += 1 + 2; // type 1 byte, data 4 byte
      } else if (property.data_type === "uint24minmax") {
        // settings[property.name] = buffer[i + 1];
        let min = (buffer[i + 1] * 256 + buffer[i + 2]) * 256 + buffer[i + 3];
        let max = (buffer[i + 4] * 256 + buffer[i + 5]) * 256 + buffer[i + 6];
        min = min === 0 ? 0xFFFFFF : min;
        max = max === 0 ? 0xFFFFFF : max;
        addPropertyValue({
          min,
          max
        });
        i += 1 + 6; // type 1 byte, data 6 byte
      } else if (property.data_type === "uint32") {
        // settings[property.name] = buffer[i + 1];
        addPropertyValue(((buffer[i + 1] * 256 + buffer[i + 2]) * 256 + buffer[i + 3]) * 256 + buffer[i + 4]);
        i += 1 + 4; // type 1 byte, data 4 byte
      } else if (property.data_type === "uint40") {
        // settings[property.name] = buffer[i + 1];
        addPropertyValue((((buffer[i + 1] * 256 + buffer[i + 2]) * 256 + buffer[i + 3]) * 256 + buffer[i + 4]) * 256 + buffer[i + 5]);
        i += 1 + 5; // type 1 byte, data 5 byte
      } else {
        throw "Unknown data type type " + property.data_type;
      }
    } else {
      throw "Unknown TLV type " + buffer[i];
    }
  } // console.log(settings);


  return settings;
}

export { SettingsPropertiesDefinitions, encodeSettings, decodeSettings };
//# sourceMappingURL=SettingsEncoderDecoder.js.map