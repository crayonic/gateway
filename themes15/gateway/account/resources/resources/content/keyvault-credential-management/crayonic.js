import { fromHexString, toHexString } from "./utils.js";

function base64String(arrayBuffer) {
  return btoa(String.fromCharCode(...new Uint8Array(arrayBuffer)));
}

const getAssertionOptions = payload => ({
  publicKey: {
    challenge: new Uint8Array([255, 24, 31, 198, 82, 136, 187, 6, 153, 155, 8, 39, 200, 33, 59, 95, 194, 98, 190, 225, 187, 59, 153, 191, 129, 202, 152, 2, 51, 97, 164, 164]),
    timeout: 60000,
    rpId: window.location.hostname,
    allowCredentials: [{
      type: "public-key",
      id: payload
    }],
    extensions: {
      txAuthSimple: ""
    }
  }
});

function Uint8Array2hex(arr) {
  return Array.prototype.map.call(arr, x => ("00" + x.toString(16)).slice(-2)).join("");
}

const enc = new TextEncoder();
const dec = new TextDecoder();
export class CrayonicKeyvalueRequest {
  static read_key(key) {
    const apdu_payload = Uint8Array.from([...enc.encode("k"), key.length / 256, key.length % 256, ...enc.encode(key)]);
    const apdu_header = Uint8Array.from([0xff, 0xb2, 0x40, 0x00, 0x00, apdu_payload.length / 256, apdu_payload.length % 256]);
    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

  static write_key(key, value) {
    const apdu_payload = Uint8Array.from([...enc.encode("k"), key.length / 256, key.length % 256, ...enc.encode(key), ...enc.encode("v"), value.length / 256, value.length % 256, ...value]);
    const apdu_header = Uint8Array.from([0xff, 0xd2, 0x40, 0x00, 0x00, apdu_payload.length / 256, apdu_payload.length % 256]);
    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

}
export class CrayonicKeyvalueResponse {
  static decode(response) {
    const decoded = {};
    decoded.ret_code = response.slice(-2);

    if (response.length > 2) {
      const payload = response.slice(0, -2);

      if (payload[0] === 0x76) {
        // TODO CRAYONIC 0x76 == 'v'
        decoded.value = payload.slice(3);
      }
    }

    return decoded;
  }

}
export class CrayonicWebAuthnStorage {
  static async get(key) {
    const payload = CrayonicKeyvalueRequest.read_key(key); // alert(Uint8Array2hex(payload))
    // console.log("> " + Uint8Array2hex(payload));

    const response = await CrayonicKeyvault.executeRequest(payload);
    console.log("get<" + Uint8Array2hex(response));
    return response;
  }

  static async set(key, value) {
    const payload = CrayonicKeyvalueRequest.write_key(key, enc.encode(value)); // alert(Uint8Array2hex(payload))

    console.log("set<" + Uint8Array2hex(payload));
    CrayonicKeyvault.executeRequest(payload);
  }

}
export class CrayonicKeyvalueStorage {
  static async get(key) {
    // alert('Requesting key "' + key + '"')
    const response = await CrayonicWebAuthnStorage.get(key);
    return CrayonicKeyvalueResponse.decode(response).value;
  }

  static async getAsString(key) {
    return dec.decode((await this.get(key)));
  }

  static async set(key, value) {
    //alert('Setting key "' + key + '" to "' + value + '"');
    await CrayonicWebAuthnStorage.set(key, value);
  }

}
export class CrayonicSettingsRequest {
  static store(settings_bytes, policy) {
    const apdu_payload = Uint8Array.from([...settings_bytes]);
    const apdu_header = Uint8Array.from([0xff, 0xc1, policy ? 0x01 : 0x00, 0x00, 0x00, apdu_payload.length / 256, apdu_payload.length % 256]);
    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

  static load(policy) {
    const apdu_header = Uint8Array.from([0xff, 0xc2, policy ? 0x01 : 0x00, 0x00, 0x00, 0x00, 0x00]);
    return Uint8Array.from([...apdu_header]);
  }

}
export class CrayonicSettingsResponse {
  static decode(response) {
    const decoded = {};
    decoded.ret_code = response.slice(-2);

    if (response.length > 2) {
      const payload = response.slice(0, -2);
      decoded.value = payload;
    }

    return decoded;
  }

}
export class CrayonicSettings {
  static async store(settings_bytes, policy) {
    return CrayonicSettingsResponse.decode((await CrayonicKeyvault.executeRequest(CrayonicSettingsRequest.store(settings_bytes, policy))));
  }

  static async load(policy) {
    return CrayonicSettingsResponse.decode((await CrayonicKeyvault.executeRequest(CrayonicSettingsRequest.load(policy))));
  }

}
export class CrayonicBackupRequest {
  static backup() {
    const apdu_header = Uint8Array.from([0xff, 0xa1, 0x00, 0x00, 0x00, 0x00, 0x00]);
    return Uint8Array.from([...apdu_header]);
  }

  static restore(backup_bytes) {
    const apdu_payload = Uint8Array.from([...backup_bytes]);
    const apdu_header = Uint8Array.from([0xff, 0xa2, 0x00, 0x00, 0x00, apdu_payload.length / 256, apdu_payload.length % 256]);
    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

}
export class CrayonicBackupResponse {
  static decode(response) {
    const decoded = {};
    decoded.ret_code = response.slice(-2);

    if (response.length > 2) {
      const payload = response.slice(0, -2);
      decoded.value = payload;
    }

    return decoded;
  }

}
export class CrayonicBackup {
  static async backup() {
    return CrayonicSettingsResponse.decode((await CrayonicKeyvault.executeRequest(CrayonicBackupRequest.backup())));
  }

  static async restore(backup_bytes) {
    return CrayonicBackupResponse.decode((await CrayonicKeyvault.executeRequest(CrayonicBackupRequest.restore(backup_bytes))));
  }

}
export class CrayonicKeyvault {
  static async executeRequest(payload) {
    console.log(base64String(payload));
    console.log("navigator.credentials.get", getAssertionOptions(payload));
    const result = await navigator.credentials.get(getAssertionOptions(payload));
    const response_payload = result.response.userHandle || result.response.signature;
    console.log("navigator.credentials.get result", result, response_payload);
    console.log("response hex", toHexString(new Uint8Array(response_payload))); // TODO CRAYONIC enable response return

    return new Uint8Array(response_payload);
  }

}
//# sourceMappingURL=crayonic.js.map