import React, { Component } from "react";
import { getRealm } from './utils';
import { SettingsEditor } from './SettingsEditor';

class SettingsEditorKeyvaultAccountWrapper extends Component {
  
  render() {
    return (
      <SettingsEditor 
        updateQueryString={false}
        isKeycloakAccountEditor={true}
        apiEndpoint='/cgw/'
        // apiEndpoint='http://localhost:8443/cgw/'
        realm={getRealm()}
      />
    )
  }
}

export { SettingsEditorKeyvaultAccountWrapper };
