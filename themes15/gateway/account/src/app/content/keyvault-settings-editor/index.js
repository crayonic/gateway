import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Button, Box, Container, Grid, GridList } from "@material-ui/core";
import { Input, TextField, Alert } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import DeleteIcon from "@material-ui/icons/Delete";
import SendIcon from "@material-ui/icons/Send";
import SettingsEditor from "./SettingsEditor";

import { CrayonicKeyvalueStorage } from "./storage";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      payloadToSend: "1234567890",
      key: "bitwarden masterPassword user@example.com",
      value: "placeholder value"
    };
  }

  set = async () => {
    await CrayonicKeyvalueStorage.set(this.state.key, this.state.value);
  };

  get = async () => {
    this.setState({ value: "" });
    const value = await CrayonicKeyvalueStorage.getAsString(this.state.key);
    this.setState({ value });
  };

  render() {
    return (
      <div>
        <Container>
          <Grid container spacing={3}>
            {/* <Grid item xs={12}>
              <h1>Key/Value Store</h1>
            </Grid>
            <Container>
              <Grid item xs={6}>
                <TextField
                  required
                  id="standard-required"
                  // label="Required"
                  helperText="storage key"
                  fullWidth={true}
                  value={this.state.key}
                  onChange={(e) => this.setState({ key: e.target.value })}
                />
                <TextField
                  required
                  id="standard-required"
                  // label="Required"
                  helperText="storage value"
                  fullWidth={true}
                  value={this.state.value}
                  onChange={(e) => this.setState({ value: e.target.value })}
                />
              </Grid>
              <Grid item xs={6}>
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={<SendIcon />}
                  onClick={this.get}
                >
                  get
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={<SendIcon />}
                  onClick={this.set}
                >
                  set
                </Button>
              </Grid>
            </Container> */}
            <Grid item xs={12}>
              {/* <h1>KeyVault Settings</h1> */}
              <SettingsEditor />
            </Grid>
          </Grid>
        </Container>
      </div>
    );
  }
}

class AppWrapper extends Component {
    static propTypes = {};

    state = {
        some_value: 'aaa'
    };

    render() {
        return this.props.children;
    }
}

const MOUNT_NODE_PROPERTY_NAME = '_mount_node';
const DEV = process.env.NODE_ENV === 'development';
if (DEV) {
    ReactDOM.render(<AppWrapper><App /></AppWrapper>, document.querySelector("#app"));
} else {
    window.KEYVAULT_SETTINGS_EDITOR = {
        createInstance: (node) => {
            return new Promise((resolve, reject) => {
                let instance = null;
                ReactDOM.render(<AppWrapper ref={(ref) => {instance = ref}}><App /></AppWrapper>, node,
                    () => {
                        instance[MOUNT_NODE_PROPERTY_NAME] = node;
                        resolve(instance);
                    });
            });
        },
        destroyInstance: (ref) => {
            // console.log('destroy', ref);
            return ReactDOM.unmountComponentAtNode(ref[MOUNT_NODE_PROPERTY_NAME]);
        }
    };
}

//usage
// let instance = null;
// window.KEYVAULT_SETTINGS_EDITOR.createInstance(document.querySelector("#app")).then(
//     (ref) => {
//       instance = ref;
//       console.log('ready', ref.state);
//       setTimeout(() => {
//           ref.setState({
//               some_value: 'bbb'
//           }, () => {
//               console.log('value set', ref.state);
//           });
//       }, 500);
//     }
// );
//
// setTimeout(() => {
//     console.log('destroy result', window.KEYVAULT_SETTINGS_EDITOR.destroyInstance(instance));
// }, 5000);