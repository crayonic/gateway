import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  Button,
  ButtonGroup,
  IconButton,
  Box,
  Container,
  Grid,
  GridList,
  Checkbox,
  Tooltip,
  Icon, 
  Card,
  List, ListItem, ListItemAvatar, Avatar, ListItemText, ListItemSecondaryAction, ListSubheader,
  Typography,
  Backdrop, CircularProgress,
  CardContent, MenuItem,
  Select,
  createStyles,
  withStyles
} from "@material-ui/core";

import {
  Fingerprint,
  TouchApp,
  Create,
  RecordVoiceOver,
  Dialpad,
  Block,
  Mic,
  Translate,
  FolderOpen, Save,
  CheckBoxOutlineBlank, CheckBox, SyncAlt,
  AccountCircle, Delete, GetApp, Sync, VpnKey, Refresh, AlternateEmail
} from "@material-ui/icons";
import {
  FormHelperText,
  FormControl,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio
} from "@material-ui/core";

// import { Certificate } from '@fidm/x509';

const PIV_SLOTS = {
  '9a': 'Authentication',
  '9c': 'Digital Signature',
  '9d': 'Key Management',
}

function fileDownload(data, filename, mime, bom) {
  var blobData = (typeof bom !== 'undefined') ? [bom, data] : [data]
  var blob = new Blob(blobData, {type: mime || 'application/octet-stream'});
  if (typeof window.navigator.msSaveBlob !== 'undefined') {
      // IE workaround for "HTML7007: One or more blob URLs were
      // revoked by closing the blob for which they were created.
      // These URLs will no longer resolve as the data backing
      // the URL has been freed."
      window.navigator.msSaveBlob(blob, filename);
  }
  else {
      var blobURL = (window.URL && window.URL.createObjectURL) ? window.URL.createObjectURL(blob) : window.webkitURL.createObjectURL(blob);
      var tempLink = document.createElement('a');
      tempLink.style.display = 'none';
      tempLink.href = blobURL;
      tempLink.setAttribute('download', filename);

      // Safari thinks _blank anchor are pop ups. We only want to set _blank
      // target if the browser does not support the HTML5 download attribute.
      // This allows you to download files in desktop safari if pop up blocking
      // is enabled.
      if (typeof tempLink.download === 'undefined') {
          tempLink.setAttribute('target', '_blank');
      }

      document.body.appendChild(tempLink);
      tempLink.click();

      // Fixes "webkit blob resource error 1"
      setTimeout(function() {
          document.body.removeChild(tempLink);
          window.URL.revokeObjectURL(blobURL);
      }, 200)
  }
}

const styles = (theme) =>
  createStyles({
    backdrop: {
      zIndex: 10000,
      color: '#f00',
    },
  }
);


class Certificates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // credentials: {"timestamp":"Fri, 16 Apr 2021 09:15:21 GMT","sn":"F96E36327FC5","rkCnt":4,"residentKeys":[{"rkIndex":0,"credentialId":"4b56bd87e10b89feb7af65ff5e885f535238f72c277dcd5262e25aa4825a67b1218fe2d82647e6fc646b9ce1488bbe2a879ec8b345a6e1b275628cacf36ddfc946671ee729000000","credentialUserId":"32653337303730632d613337352d346234302d393439642d363761616565623137633562","credentialUserIdCGW":"2e37070c-a375-4b40-949d-67aaeeb17c5b","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"crayonic.io"},{"rkIndex":1,"credentialId":"4b566d2923d63ac739b3d55c67beb357df68e480d4418b9968e1bb6fadd472aec3773a2f49960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d97634b000000","credentialUserId":"33616335616462312d323566352d343738312d396265632d363139303965333563663132","credentialUserIdCGW":"3ac5adb1-25f5-4781-9bec-61909e35cf12","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"},{"rkIndex":2,"credentialId":"4b5675b0db3c333f400ff923c4a65a7f532e4a0a001bdef218e8ad724fba12291d8896f849960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d9763f2000000","credentialUserId":"30626663373635302d303432612d343132642d393862302d626561323566373064623832","credentialUserIdCGW":"0bfc7650-042a-412d-98b0-bea25f70db82","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"},{"rkIndex":3,"credentialId":"4b56ac0084f5a638d1598343c1e599ecec10d6dc70da44f420acffd4debdcf90c5847b5a49960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d97635a020000","credentialUserId":"34313865336637302d323133322d346464662d393566322d366431666530383639616236","credentialUserIdCGW":"418e3f70-2132-4ddf-95f2-6d1fe0869ab6","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"}]}
      account: {},
      isEdited: false
    }
  }

  async componentDidMount() {
    this.setState({isLoading: true})
    await this.fetchAccount()
    this.setState({isLoading: false})
  }

  fetchAccount = async() => {
    try {
      const result = await fetch(
        keycloak.authServerUrl + '/realms/' + keycloak.realm + '/account/', 
        {
          method: 'get',
          headers: {
            "Authorization": `Bearer ${keycloak.token}`,
            "Content-Type": "application/json"
          }
        }
      )
      const account = await result.json();
      console.log(account);
      this.setState({account, isEdited: false})
      return account;
    } catch (error) {
      console.error(error)
      alert("Failed loading account")
    }
  }

  render() {
    const { classes } = this.props;
    const { credentials } = this.state;
    // const residentKeys = credentials.residentKeys || []
    const dense = false;

    console.log('keycloak', keycloak)

    

    return(<>
      <Backdrop className={classes.backdrop} open={this.state.isLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Typography variant="h6" className={classes.title}>
        PIV Certificates
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6} style={{justifyContent: 'space-between'}}>
          <div className={classes.demo}>
            <List dense={dense} subheader={<ListSubheader></ListSubheader>}>
              {Object.keys(PIV_SLOTS).map((piv_slot) => {
                const enrolled = this.state.account.attributes && this.state.account.attributes[`cert_pivslot_${piv_slot}_crt`] !== undefined;
                return (
                  <ListItem key={piv_slot}>
                    {/* <pre>{Certificate.fromPEM(this.state.account.attributes.cert_SIGNATURE_key)}</pre> */}
                    <ListItemAvatar>
                      <Avatar>
                        <VpnKey />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={`${PIV_SLOTS[piv_slot]} (${piv_slot})`}
                      secondary={enrolled ? `Enrolled` : 'N/A'}
                      // secondary={`Enrolled, SCEP ${scep_type}`}
                    />
                    {enrolled ? <ListItemSecondaryAction>
                      <IconButton label="dadsa" edge="end" aria-label="download" onClick={() => {
                        const blobkey = new Blob([this.state.account.attributes[`cert_pivslot_${piv_slot}_key`]], {type : 'application/pkcs8'});
                        fileDownload(blobkey, `${PIV_SLOTS[piv_slot]} (${piv_slot}).key`)
                        const blobcrt = new Blob([this.state.account.attributes[`cert_pivslot_${piv_slot}_crt`]], {type : 'application/x-x509-user-cert'});
                        fileDownload(blobcrt, `${PIV_SLOTS[piv_slot]} (${piv_slot}).crt`)
                      }}>
                        <GetApp />
                      </IconButton>
                      {/* <IconButton edge="end" aria-label="delete" onClick={() => {
                        const blob = new Blob([this.state.account.attributes[`cert_${scep_type}_crt`]], {type : 'application/x-x509-user-cert'});
                        fileDownload(blob, `${scepType2slotName(scep_type)} (${scepType2slotId(scep_type)}).crt`)
                      }}>
                        <GetApp />
                      </IconButton> */}
                      {/* <IconButton edge="end" aria-label="delete">
                        <Refresh />
                      </IconButton>
                        <GetApp />
                      </IconButton>
                      <IconButton edge="end" aria-label="delete">
                        <Sync />
                      </IconButton> */}
                    </ListItemSecondaryAction> : null}
                  </ListItem>
                )
              })}
            </List>
          </div>
        </Grid>
      </Grid>
      {/* <pre>
        {JSON.stringify(this.state.credentials, null, 4)}
      </pre> */}
    </>)
  }
}

const CertificatesWith = withStyles(styles)(Certificates);
export {CertificatesWith as Certificates};
export default withStyles(styles)(Certificates);
