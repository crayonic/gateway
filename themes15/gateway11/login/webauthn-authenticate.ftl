    <#import "template.ftl" as layout>
    <@layout.registrationLayout showAnotherWayIfPresent=false; section>
    <#if section = "title">
     title
    <#elseif section = "header">
        ${kcSanitize(msg("webauthn-login-title"))?no_esc}
    <#elseif section = "form">

    <form id="webauth" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
        <div class="${properties.kcFormGroupClass!}">
            <input type="hidden" id="clientDataJSON" name="clientDataJSON"/>
            <input type="hidden" id="authenticatorData" name="authenticatorData"/>
            <input type="hidden" id="signature" name="signature"/>
            <input type="hidden" id="credentialId" name="credentialId"/>
            <input type="hidden" id="userHandle" name="userHandle"/>
            <input type="hidden" id="error" name="error"/>
        </div>
    </form>

    <#if authenticators??>
        <form id="authn_select" class="${properties.kcFormClass!}">
            <#list authenticators.authenticators as authenticator>
                <input type="hidden" name="authn_use_chk" value="${authenticator.credentialId}"/>
            </#list>
        </form>
    </#if>

    <script type="text/javascript" src="${url.resourcesCommonPath}/node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="${url.resourcesPath}/js/base64url.js"></script>
    <script type="text/javascript">

        window.onload = () => {
            let isUserIdentified = ${isUserIdentified};

            //
            // this section applicable when RK used:
            //
            if (!isUserIdentified) {
                localStorage.setItem('isUserIdentified', 'no');
                doAuthenticate([]);

                return;
            }

            // RK not used:
            localStorage.setItem('isUserIdentified', 'yes');
            checkAllowCredentials();
        };


        // RK not used flow
        // original authentication code:
        function checkAllowCredentials() {
            let allowCredentials = [];
            let authn_use = document.forms['authn_select'].authn_use_chk;

            if (authn_use !== undefined) {
                if (authn_use.length === undefined) {
                    allowCredentials.push({
                        id: base64url.decode(authn_use.value, {loose: true}),
                        type: 'public-key',
                    });
                } else {
                    for (let i = 0; i < authn_use.length; i++) {
                        allowCredentials.push({
                            id: base64url.decode(authn_use[i].value, {loose: true}),
                            type: 'public-key',
                        });
                    }
                }
            }
            
            doAuthenticate(allowCredentials);

        }

    function doAuthenticate(allowCredentials) {

        let challenge = "${challenge}";
        let userVerification = "${userVerification}";
        let rpId = "${rpId}";
        let publicKey = {
            rpId : rpId,
            challenge: base64url.decode(challenge, { loose: true })
        };

        if (allowCredentials.length) {
            publicKey.allowCredentials = allowCredentials;
        }

        if (userVerification !== 'not specified') publicKey.userVerification = userVerification;

        navigator.credentials.get({publicKey})
            .then((result) => {
                window.result = result;

                let clientDataJSON = result.response.clientDataJSON;
                let authenticatorData = result.response.authenticatorData;
                let signature = result.response.signature;

                // DEBUG only! to be removed in PROD
//                localStorage.setItem('login-response-credentialid', result.id);
//                localStorage.setItem('login-response-clientDataJSON', JSON.stringify(clientDataJSON));
                localStorage.setItem('login-response-clientDataJSON', ab2str(clientDataJSON));
//                localStorage.setItem('login-response-authenticatorData', ab2str(authenticatorData));
//                localStorage.setItem('login-response-signature', ab2str(signature));

                // UP + UV result check

                const aData = new Uint8Array(authenticatorData);
                const resultFlag = aData[32];
                localStorage.setItem('login-response-authenticatorData', aData);
                localStorage.setItem('login-response-resultflag', resultFlag);

                const upuv = 'UP = ' + isBitSet(resultFlag, 1) + ', UV = ' + isBitSet(resultFlag, 3);
                localStorage.setItem('login-response-upuv', upuv);

                const signCount = 'Sign Counter: ' + ((aData[34]*65536) + (aData[35]*256) + aData[36]);
                localStorage.setItem('login-response-signcount', signCount);

                $("#clientDataJSON").val(base64url.encode(new Uint8Array(clientDataJSON), { pad: false }));
                $("#authenticatorData").val(base64url.encode(new Uint8Array(authenticatorData), { pad: false }));
                $("#signature").val(base64url.encode(new Uint8Array(signature), { pad: false }));
                $("#credentialId").val(result.id);
                if(result.response.userHandle) {
                    $("#userHandle").val(base64url.encode(new Uint8Array(result.response.userHandle), { pad: false }));
                    localStorage.setItem('login-userHandle', base64url.encode(new Uint8Array(result.response.userHandle), { pad: false }));
                }
                else {
                    localStorage.setItem('login-userHandle', '');                    
                }

//                localStorage.setItem('login-credentialId', result.id);
                localStorage.setItem('login-error', '');

                console.log('webauthn-authenticate.ftl: completed');

                $("#webauth").submit();
            })
            .catch((err) => {
                localStorage.setItem('login-error', err);

                $("#error").val(err);
                $("#webauth").submit();
            })
        ;
    }

    function ab2str(buf) {
        return String.fromCharCode.apply(null, new Uint8Array(buf));
    }


    // n = number, k = bit number starting with 1, e.g. 255, 1 checks for bit 0
    function isBitSet(n, k) {
        if ((n & (1 << (k - 1))) > 0)
            return true;
        else return false;
    }

    </script>
    <#elseif section = "info">

    </#if>
    </@layout.registrationLayout>
