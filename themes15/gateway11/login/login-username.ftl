<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('username') displayInfo=(realm.password && realm.registrationAllowed && !registrationDisabled??); section>
    <#if section = "header">
        ${msg("loginAccountTitle")}
    <#elseif section = "form">
        <div id="kc-form">
            <div id="kc-form-wrapper">

    <#--  <form id="webauth" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">

        <div class="${properties.kcFormGroupClass!}">
            <input type="hidden" id="clientDataJSON" name="clientDataJSON"/>
            <input type="hidden" id="authenticatorData" name="authenticatorData"/>
            <input type="hidden" id="signature" name="signature"/>
            <input type="hidden" id="credentialId" name="credentialId"/>
            <input type="hidden" id="userHandle" name="userHandle"/>
            <input type="hidden" id="error" name="error"/>
            <input type="hidden" id="username" name="username"/>
        </div>
            <input type="button"
        class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}"
        id="registerWebAuthnDirect" value="Usernameless Login" onclick="doAuthenticate([])" autofocus/>

    </form>  -->

    <script type="text/javascript" src="${url.resourcesCommonPath}/node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="${url.resourcesPath}/js/base64url.js"></script>
    <script type="text/javascript">

        // this function is NOT used in passwordless scenario (should be removed in PROD)
        function doAuthenticate(allowCredentials) {

        let challenge = "abymHWnXQWmjjkb-hpF-7wa";
        let userVerification = "required";
        let rpId = window.location.hostname;
        let publicKey = {
            rpId : rpId,
            challenge: base64url.decode(challenge, { loose: true })
        };

        if (allowCredentials.length) {
            publicKey.allowCredentials = allowCredentials;
        }

        if (userVerification !== 'not specified') publicKey.userVerification = userVerification;

        // to be removed in PROD
        localStorage.setItem('test-publickey', publicKey);

        navigator.credentials.get({publicKey})
            .then((result) => {
                window.result = result;

                // to be removed in PROD
                localStorage.setItem('test-result', result);

                let clientDataJSON = result.response.clientDataJSON;
                let authenticatorData = result.response.authenticatorData;
                let signature = result.response.signature;

                $("#clientDataJSON").val(base64url.encode(new Uint8Array(clientDataJSON), { pad: false }));
                $("#authenticatorData").val(base64url.encode(new Uint8Array(authenticatorData), { pad: false }));
                $("#signature").val(base64url.encode(new Uint8Array(signature), { pad: false }));
                $("#credentialId").val(result.id);
                if(result.response.userHandle) {
                    $("#userHandle").val(base64url.encode(new Uint8Array(result.response.userHandle), { pad: false }));
                    localStorage.setItem('login-userHandle', base64url.encode(new Uint8Array(result.response.userHandle), { pad: false }));
                }
                else {
                    localStorage.setItem('login-userHandle', '');                    
                }

                localStorage.setItem('login-error', '');

                // (db read)
                checkUserHandles();
                console.log('login-username.ftl part 1: completed');

            })
            .then((result) => {
//                $("#webauth").submit();
                document.getElementById("kc-form-login").style.visibility = "hidden";
                document.getElementById("registerWebAuthnDirect").style.visibility = "hidden";
                console.log('login-username.ftl redirecting...');

                setTimeout(function(){ 
                    console.log('login-username.ftl part 2: completed');
                    document.getElementById('webauth').username.value = localStorage.getItem('loginUsername');;
                    document.getElementById('kc-form-login').username.value = localStorage.getItem('loginUsername');;
                    $("#kc-form-login").submit();
                 }, 2000);
//                $("#kc-form-login").submit();
            })
            .catch((err) => {
                localStorage.setItem('login-error', err);                   
                $("#error").val(err);
//                $("#webauth").submit();
            })
        ;
    }

    </script>
    
                <#if realm.password>
                    <form id="kc-form-login" onsubmit="login.disabled = true; saveUsername(); return true;" action="${url.loginAction}"
                          method="post">
                <#--  <div id="cgw-option">
                    <p><br>or<br></p>
                </div>  -->
                        <div class="${properties.kcFormGroupClass!}">

                            <label for="username"
                                   class="${properties.kcLabelClass!}"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label>

                            <#if usernameEditDisabled??>
                                <input tabindex="1" id="username"
                                       aria-invalid="<#if message?has_content && message.type = 'error'>true</#if>"
                                       class="${properties.kcInputClass!}" name="username"
                                       value="${(login.username!'')}"
                                       type="text" disabled/>
                            <#else>
                                <input tabindex="1" id="username"
                                       aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"
                                       class="${properties.kcInputClass!}" name="username"
                                       value="${(login.username!'')}"
                                       type="text" autofocus autocomplete="off"/>
                            </#if>

                            <#if messagesPerField.existsError('username')>
                                <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                    ${kcSanitize(messagesPerField.get('username'))?no_esc}
                                </span>
                            </#if>
                        </div>

                        <div id="kc-form-buttons" class="${properties.kcFormGroupClass!}">
                            <input tabindex="4"
                                   class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}"
                                   name="login" id="kc-login" type="submit" value="Login with username"/>
                        </div>

                        <div class="${properties.kcFormGroupClass!} ${properties.kcFormSettingClass!}">
                            <div id="kc-form-options">
                                <#if realm.rememberMe && !usernameEditDisabled??>
                                    <div class="checkbox">
                                        <label>
                                            <#if login.rememberMe??>
                                                <input tabindex="3" id="rememberMe" name="rememberMe" type="checkbox"
                                                       checked> ${msg("rememberMe")}
                                            <#else>
                                                <input tabindex="3" id="rememberMe" name="rememberMe"
                                                       type="checkbox"> ${msg("rememberMe")}
                                            </#if>
                                        </label>
                                    </div>
                                </#if>
                            </div>
                        </div>

                    </form>

                    <#--  login.js should point to your host site -->
                    <script type="text/javascript" src="/login.js"></script>

                    <div id="cgw-check"></div>

                </#if>
        </div>

            <#if realm.password && social.providers??>
                <div id="kc-social-providers" class="${properties.kcFormSocialAccountSectionClass!}">
                    <hr/>
                    <h4>${msg("identity-provider-login-label")}</h4>

                    <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountListGridClass!}</#if>">
                        <#list social.providers as p>
                            <a id="social-${p.alias}" class="${properties.kcFormSocialAccountListButtonClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                               type="button" href="${p.loginUrl}">
                                <#if p.iconClasses?has_content>
                                    <i class="${properties.kcCommonLogoIdP!} ${p.iconClasses!}" aria-hidden="true"></i>
                                    <span class="${properties.kcFormSocialAccountNameClass!} kc-social-icon-text">${p.displayName}</span>
                                <#else>
                                    <span class="${properties.kcFormSocialAccountNameClass!}">${p.displayName}</span>
                                </#if>
                            </a>
                        </#list>
                    </ul>
                </div>
            </#if>

    <#elseif section = "info" >
        <#if realm.password && realm.registrationAllowed && !registrationDisabled??>
            <div id="kc-registration">
                <span>${msg("noAccount")} <a tabindex="6" href="${url.registrationUrl}">${msg("doRegister")}</a></span>
            </div>
        </#if>
    </#if>

</@layout.registrationLayout>
