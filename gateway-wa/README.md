# Web Authentication demo
A demo of passwordless authentication using Web Authentication (WebAuthn).


### localhost: install and run
*Prerequisites:* 
- SSL certs for localhost (localhost.crt, localhost.key) must be present in app root directory. This is required for correct WebAuthn functionality (it works only via HTTPS)

To run the demo, run `npm install` once and then `npm run local`. Demo page can be accessed at
[https://localhost:8000](https://localhost:8000)

The WebAuthn server will be running at [https://localhost:3005/wa](https://localhost:3005/wa)


### online: install and run

*Prerequisites:* 
- env variable `HOST` shall be set to target host, e.g. `crayonic.io`
- env variable `HOST_ADDRESS` shall be set to target host including protocol, e.g. `https://crayonic.io`
- env variable `HOST_API_ROOT` shall be set to webauthn backend endpoint, e.g. `/wa` or `https://crayonic.io/wa`
- env variable `NODE_ENV` shall be set to `prod`
- `apiUrl` (HOST URL) shall be changed in gateway-wa.js (default: const apiUrl = 'https://localhost:3005/wa')
- SSL certs (tls.crt, tls.key) must be present in app root directory
- reverse proxy shall be configured, so that app is accessible

To run the demo on your infrastructure, run `npm install` once and then `npm run start`. 

To view the demo, go to [https://crayonic.io](https://crayonic.io)

The WebAuthn server will be running at [https://crayonic.io:3005/wa](https://crayonic.io:3005/wa)

Note: replace `crayonic.io` with your HOST value in the links above.
