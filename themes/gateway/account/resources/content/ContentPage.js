/*
 * Copyright 2019 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as React from "../../../common/keycloak/web_modules/react.js";
import { Button, Grid, GridItem, Title, Tooltip } from "../../../common/keycloak/web_modules/@patternfly/react-core.js";
import { RedoIcon } from "../../../common/keycloak/web_modules/@patternfly/react-icons.js";
import { Msg } from "../widgets/Msg.js";
import { ContentAlert } from "./ContentAlert.js";

/**
 * @author Stan Silvert ssilvert@redhat.com (C) 2019 Red Hat Inc.
 */
export class ContentPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (/*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ContentAlert, null), /*#__PURE__*/React.createElement("section", {
        id: "page-heading",
        className: "pf-c-page__main-section pf-m-light"
      }, /*#__PURE__*/React.createElement(Grid, null, /*#__PURE__*/React.createElement(GridItem, {
        span: 11
      }, /*#__PURE__*/React.createElement(Title, {
        headingLevel: "h1",
        size: "3xl"
      }, /*#__PURE__*/React.createElement("strong", null, /*#__PURE__*/React.createElement(Msg, {
        msgKey: this.props.title
      })))), this.props.onRefresh && /*#__PURE__*/React.createElement(GridItem, {
        span: 1
      }, /*#__PURE__*/React.createElement(Tooltip, {
        content: /*#__PURE__*/React.createElement(Msg, {
          msgKey: "refreshPage"
        })
      }, /*#__PURE__*/React.createElement(Button, {
        "aria-describedby": "refresh page",
        id: "refresh-page",
        variant: "plain",
        onClick: this.props.onRefresh
      }, /*#__PURE__*/React.createElement(RedoIcon, {
        size: "sm"
      })))), this.props.introMessage && /*#__PURE__*/React.createElement(GridItem, {
        span: 12
      }, " ", /*#__PURE__*/React.createElement(Msg, {
        msgKey: this.props.introMessage
      })))), /*#__PURE__*/React.createElement("section", {
        className: "pf-c-page__main-section pf-m-no-padding-mobile"
      }, this.props.children))
    );
  }

}
;
//# sourceMappingURL=ContentPage.js.map