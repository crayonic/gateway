// Crayonic Gateway backup/restore/settings/userhandle sync
//
// CHM, 3/2021, 7/2021, 11/2021
//
// related theme files:
// - /themes/gateway11/account/account.ftl
// - /themes/gateway/account/index.ftl


// this function runs every time user account console is displayed
// entry point in /themes/gateway11/account/account.ftl 
// and /themes/gateway/account/index.ftl
function gatewayStart() {

    localStorage.setItem('login-error', '');
    localStorage.setItem('loginStatus', '');

    localStorage.setItem('loginRealm', getRealm());

    console.log(JSON.stringify(keycloak));
    // console.log(`gatewayStart(): user/realm: ${keycloak.tokenParsed.preferred_username}/${realm}`);

    try {
        if (keycloak.token) {
            localStorage.setItem('token', keycloak.token);
        }
    } catch (err) {
        console.log('gatewayStart(): keycloak js adapter not initialized');
    }

    // console.log(localStorage.getItem('login-response-upuv'));
    // alert(localStorage.getItem('login-response-upuv') + '\n' + localStorage.getItem('login-response-signcount'));

    // needed for usernameless login
    // updateUserHandles();

    // do we need to do restore?
    // if (localStorage.getItem('performRestore') === 'yes') {
    //     callRestore();
    // } else {
    //     callBackup();
    // }

}

async function tokenCheck() {

    const token = localStorage.getItem('token');

    if (token) {
        const decoded = jwt_decode(token);
        // const isTokenExpired = decoded => Date.now() >= (JSON.parse(atob(decoded.split('.')[1]))).exp * 1000;
        isTokenExpired = (Date.now() >= decoded['exp'] * 1000) ? true : false;

        const realm = (decoded.iss).split('/')[5];
        const username = decoded.preferred_username;

        console.log(`tokenCheck: ${username}/${realm}: ${JSON.stringify(decoded)}`)
    } else {
        isTokenExpired = true;
        console.log(`tokenCheck: token not valid`);
    }

    if (isTokenExpired) {
        console.log(`tokenCheck: token expired`);
    }

    // await initKeycloak();
    
}


async function initKeycloak() {

    const kcHost = window.location.protocol + '//' + window.location.hostname + '/auth';

    const tmp = window.location.href.substring((window.location.href.search(/\/realms\//g))+8);
    realm = tmp.substring(0, tmp.search(/\//g));

    if (!realm)
        realm = 'gateway';

    // const conf = {
    //     "realm": realm,
    //     "auth-server-url": kcHost,
    //     "ssl-required": "external",
    //     // "resource": "gateway",
    //     "public-client": true,
    //     "confidential-port": 0,
    //     "url": kcHost,
    //     "clientId": 'account',
    //     "client_secret": "",
    //     "enable-cors": true
    // };


    // const conf = {
    //     "realm-public-key" : "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTbkO5rSSW/iWrKboNSJj4uXtTFRtCr/ts2twu4wOpVChY/e6qwZ5K72IsGzuU5WD/g7Pcsr+eQINf9NgVQeV2r5fGuqnHN7ousije6iminBmuujbpeD+29fuczTUMiyxAH+RGEVA3qbC2LJ6Erl4LtKGY/Sn48vQDSxozIo8yG65MWn7GudPmGCXzVvJPfv/DGfCwfw4ySKcNWkMgAKDUees8YnpljDd+q0ZBaWBqjctk8mWk/FV0Pl8ExuSqG+3jM/Zr7hYNSmcjwqBfoSHGExs41i5ARUQSW9REfAx428VfdYL/Q0dPJr8lT8hjV523ffcptiaNX24bZabssu/wIDAQAB",
    //     "realm" : realm,
    //     "auth-server-url" : `https://${window.location.hostname}/auth`,
    //     "ssl-required" : "external",
    //     "public-client" : true,
    //     "confidential-port": 0,
    //     "url": kcHost,
    //     "clientId": 'gateway-api',
    //     "resource" : "gateway-api"
    // };

    const conf = {
        "realm": realm,
        "auth-server-url": kcHost,
        "ssl-required": "external",
        "public-client": true,
        "confidential-port": 0,
        "url": kcHost,
        "clientId": 'gateway-www',
        // "clientId": 'account-console',        
        "enable-cors": true
    };

    const token = localStorage.getItem('token');
    // const token = localStorage.getItem('token-account');


    if (token=='undefined') {
        token = '';
        localStorage.removeItem('token');
    }

    if (token) {
        const decoded = jwt_decode(token);
        // const isTokenExpired = decoded => Date.now() >= (JSON.parse(atob(decoded.split('.')[1]))).exp * 1000;
        isTokenExpired = (Date.now() >= decoded['exp'] * 1000) ? true : false;
    }

    if (!token || isTokenExpired) {
        
        if (isTokenExpired) {
            console.log(`token expired`);
        }

        console.log('keycloak init starting');

        kc = new Keycloak(conf);
        // kc.init().then(function(authenticated) {        
        kc.init({onLoad: 'login-required'}).then(function(authenticated) {                  
        // kc.init({onLoad: 'check-sso', silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html'}).then(function(authenticated) {

            // console.log(JSON.stringify(kc));

            if (authenticated) {
                username = kc.tokenParsed.preferred_username;
                localStorage.setItem('loginUsername', username);        
                localStorage.setItem('token', kc.token);

                console.log(`authenticated: ${username}/${realm}`);

                kc.onTokenExpired = () => {
                    console.log('expired '+ new Date());
                    kc.updateToken(50).success((refreshed) => {
                        if (refreshed) {
                            console.log('refreshed '+ new Date());
                        } else {
                            console.log('not refreshed '+ new Date());
                        }
                    }).error(() => {
                        console.error('Failed to refresh token '+ new Date());
                    });
                }

            }
            else {
                username = '';
                localStorage.removeItem('token');                
                console.log(`not authenticated`);
            }


        }).catch(function() {
            console.log('failed to initialize keycloak');
        });

    } else {
        console.log(`token available and valid: ${token}`);
    }

}

// get gateway policies example (protected by admin role)
async function getPolicies() {

    await tokenCheck();

    const url = `https://${window.location.hostname}/cgw/policies?realm=${realm}`;

    var req = new XMLHttpRequest();
    req.open('GET', url, true);
    req.setRequestHeader('Accept', 'application/json');
    req.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));

    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            if (req.status == 200) {
                console.log('Success');
            } else if (req.status == 403) {
                console.log('Forbidden');
            }
        }
    }

    req.onload = function() {
        if (req.status != 200) {
            console.log(`Error ${req.status}: ${req.statusText}`); // e.g. 404: Not Found
        } else {
            console.log(`Policies retrieved:`);
            console.log(`${req.responseText}`);
        }
    };

    req.send();
};


//
// saves userhandle/username pair for usernameless login experience
//
// DEPRECATED
//
async function updateUserHandles() {

    await tokenCheck();

    const user = localStorage.getItem('loginUsername');
    const userHandle = localStorage.getItem('login-userHandle');

    let userHandles = localStorage.getItem('userHandles');
    let userHandleNames = localStorage.getItem('userHandleNames');

    if (!userHandles)
        userHandles = '';

    if (!userHandleNames)
        userHandleNames = '';

    if (user && userHandle) {

        // write userhandle/user pair to DB via Gateway API
        // console.log(`updateUserHandles(): realm: ${getRealm()}, userhandle: ${userHandle}, username: ${user} `);

        // $.post(window.location.protocol + '//' + window.location.hostname + '/cgw/credentials/userhandle',    
        $.post('https://' + window.location.hostname + '/cgw/credentials/userhandle',
            {
                realm: getRealm(),
                userhandle: userHandle,
                username: user
            },
            function( data ) {
                console.log('updateUserHandles(): userhandle/username pair saved/updated');
            }
        )
        .fail(function(jqXHR, textStatus, error){
            console.log('updateUserHandles(): Something went wrong...' + error );
            return false;
        });

/*
        if ((userHandles.includes(userHandle)) && (userHandleNames.includes(user))) {
            // we already have info we need
            console.log('userhandle already exists')
            return;
        } else {
            // we need to create new userhandle/username pair
            
            // remove following in PROD:
            userHandles += `${userHandle},`;
            userHandleNames += `${user},`;
            localStorage.setItem('userHandles', userHandles);
            localStorage.setItem('userHandleNames', userHandleNames);

            // debug - this section needs to be removed in PROD
            if (localStorage.getItem('cgenv') != 'prod') {
                console.log('new userhandle/username pair added');
                return true;
            }            

        }
*/

    }    
}

async function callShowBackups() {
    
    // alert(localStorage.getItem('login-response-upuv') + '\n' + localStorage.getItem('login-response-signcount'));

    await tokenCheck();

    const rpid = window.location.hostname;
    const realm = getRealm();
    const username = localStorage.getItem('loginUsername'); 

    console.log(`Starting showBackups (${realm}/${username}/${rpid})`);

    $.ajax({
        type: "GET",
        url: `https://${rpid}/cgw/backup/meta`,
        data: 
        {
            rpid: rpid,
            realm: realm,
            username: username
        },
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        dataType: 'json',
        success: function (data, status, xhr) {
            try {                
                showBackups(data);
                return true;
            }
            catch (error) {
                console.log('callShowBackups(): ' + error);
                return false;
            }    
        },
        error: function (xhr, status, error) {
            $("#backupResult").html(error);
            console.log('callShowBackups(): ' + error );
            return false;    
       }        
    });

    return true;
}


async function callShowResidentKeys() {
    
    await tokenCheck();

    const rpid = window.location.hostname;   
    const realm = getRealm();
    const username = localStorage.getItem('loginUsername'); 

    console.log(`Starting showRK (${realm}/${username}/${rpid})`);

    $.ajax({
        type: "GET",
        url: `https://${rpid}/cgw/credentials/rk`,
        data: 
        {
            rpid: rpid,
            realm: realm,
            username: username
        },
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        dataType: 'json',
        success: function (data, status, xhr) {
            try {                
                showResidentKeys(data);
                return true;
            }
            catch (error) {
                console.log('callShowResidentKeys(): ' + error);
                return false;
            }    
        },
        error: function (xhr, status, error) {
            $("#residentKeysList").html(error);
            console.log('callShowResidentKeys(): ' + error );
            return false;    
       }        

    });

    return true;
}



// valid for for account console v1 (gateway11 theme)
// gateway account console v2 (react based) has settings built in
//
function callSettings() {
    let w = window.open('https://crayonic.io/keyvaultsettings/', 'Crayonic Keyvault Settings');
}


async function callBackup() {

    await tokenCheck();

    const rpid = window.location.hostname;   
    const realm = getRealm();
    const username = localStorage.getItem('loginUsername'); 

    console.log(`Starting backup (${realm}/${username}/${rpid})`);

    doBackup(realm, username, rpid);

    return true;
}


async function callRestore(backupIndex = 0) {

    await tokenCheck();

    const rpid = window.location.hostname;    
    const realm = getRealm();
    const username = localStorage.getItem('loginUsername'); 

    console.log(`Starting restore (${realm}/${username}/${rpid}), backupIndex = ${backupIndex}`);

    // retrieve restore payload from DB

    $.ajax({
        type: "GET",
        url: `https://${rpid}/cgw/backup/meta`,
        data: 
        {
            rpid: rpid,
            realm: realm,
            username: username
        },
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        dataType: 'json',
        success: function (data, status, xhr) {
            try {                
                doRestore(realm, username, data[backupIndex]);
                return true;
            }
            catch (error) {
                console.log('callRestore(): invalid payload retrieved:' + error);
                return false;
            }    
        },
        error: function (xhr, status, error) {
            $("#backupResult").html(error);
            console.log('callRestore(): ' + error );
            return false;    
       }        

    });

    return true;
}


function doBackup(realm, username, rpId) {

    if ((!realm) || (!username)) {
        $("#backupResult").html('No realm/username selected');
        console.log('No realm/username selected');
        return false;
    }

    let challenge = new Uint8Array([255,24,198,82,136,187,6,153,155,8,39,200,33,59,95,194,98,190,225,187,59,153,191,129,202,152,2,51,97,164,164]);
    // let rpId = window.location.hostname;
    let publicKey = {
        rpId : rpId,
        challenge: challenge
    };

    localStorage.removeItem('backup-error');
    localStorage.removeItem('backupResult');

    let allowCredentials = [];
    allowCredentials.push({
        id: fromHexString("FFA10000000000"),
        type: 'public-key',
    });


    // console.log('Backup allowCredentials (stringify): ' + JSON.stringify(allowCredentials));

    publicKey.allowCredentials = allowCredentials;
    publicKey.userVerification = "required";

    navigator.credentials.get({publicKey})
        .then((result) => {
            window.result = result;

            const signature = result.response.signature;

            // debug
//                localStorage.setItem('backup-credentialId', result.id);
//                localStorage.setItem('backup-signature', signature);
            
            if (signature) {

                const payloadHex = buf2hex(signature);
                if (payloadHex.endsWith('9000')) {
                    const payload = payloadHex.substring(0, (payloadHex.length)-4);
                    const length = (payload.length)/2;  // hex byte = 2 chars

                    $.ajax({
                        type: "POST",
                        url: `https://${rpId}/cgw/backup`,
                        data: 
                        {
                            rpid: rpId,
                            realm: realm,
                            username: username,
                            payload: payload
                        },
                        headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('token')
                        },
                        dataType: 'json',
                        success: function (data, status, xhr) {
                            const msg = `Backup payload (realm/user/rp: ${realm}/${username}/${rpId}, length: ${length}) successfuly provided: ${payload}`;
                            console.log(msg);
                            $("#backupResult").html(msg);
                            // localStorage.setItem('backupResult', msg);                            
    
                            console.log('doBackup(): backup saved to DB: ' + data);
                            return true;
                        },
                        error: function (xhr, status, error) {
                            $("#backupResult").html(error);
                            console.log('doBackup(): ' + error );
                            return false;    
                       }                           
                    });

                    // debug
                    // publicKey.allowCredentials = localStorage.getItem('allowCredentials');
                    // console.log('Listing original allowCredentials: ' + JSON.stringify(publicKey.allowCredentials, null, 4));

                } else {
                    const error = 'Backup error: Incorrect response. Payload (hex): ' + payloadHex;
                    localStorage.setItem('backup-error', error);
                    console.log(error);
                    return false;
                }
                
            } else {
                $("#backupResult").html('Backup error: Incorrect response');
                return false;
            }

        })
        .catch((err) => {
            localStorage.setItem('backup-error', err);
            $("#backupResult").html('Backup error: ' + err);
            return false;

        })
    ;
}


function doRestore(realm, username, data, rpId) {

    if ((!realm) || (!username)) {
        $("#backupResult").html('No realm/username selected');
        console.log('No realm/username selected');
        return false;
    }

    let challenge = new Uint8Array([255,24,198,82,136,187,6,153,155,8,39,200,33,59,95,194,98,190,225,187,59,153,191,129,202,152,2,51,97,164,164]);
    // let rpId = window.location.hostname;
    let publicKey = {
        rpId : rpId,
        challenge: challenge
    };

    localStorage.removeItem('restore-error');
    // localStorage.removeItem('restorePayload');
    localStorage.removeItem('backupResult');

    //
    // get payload (from API call)
    // 
    // const payload = localStorage.getItem('backupPayload');
    const payload = data.backup.payload;
    const sn = data.backup.sn;
    const backupDate = data.backup.timestamp;

    if (!payload.length) {
        console.log('Error: no backup data found - you need to backup KV first.');
        $("#backupResult").html('Error: no backup data found - you need to backup KV first');        
        return false;
    }

    const length = fromHexString(payload).length;
    let restoreRequest = fromHexString("FFA20000000000" + payload);
    restoreRequest[5] = length / 256;
    restoreRequest[6] = length % 256;

    console.log(`doRestore() for realm/user/rp: ${realm}/${username}/${rpId}, SN: ${sn}, backupDate: ${backupDate}`);
    console.log('RestoreRequest (hex): ' + buf2hex(restoreRequest));

    let allowCredentials = [];

    allowCredentials.push({
        id: restoreRequest,
        type: 'public-key',
    });

    publicKey.allowCredentials = allowCredentials;
    publicKey.userVerification = "required";

    // console.log('restore publicKey: ' + JSON.stringify(publicKey));

    navigator.credentials.get({publicKey})
        .then((result) => {
            window.result = result;

            const signature = result.response.signature;

            // debug
            // localStorage.setItem('restore-credentialId', result.id);
            // localStorage.setItem('restore-signature', signature);
            
            if (signature) {
                const responseHex = buf2hex(signature);

                console.log('Restore result: ' + responseHex);

                // Restore completed - now check for status code from KV
                if (responseHex.endsWith('9000')) {

                    // localStorage.setItem('restorePayload', payload);

                    const msg = `KeyVault successfuly restored with following status: ${responseHex}. Restore() metadata: username: ${realm}/${username}, Device SN: ${sn}, Backup Date: ${backupDate}`;

                    $("#backupResult").html(msg);
                    console.log(msg);
                    // localStorage.setItem('backupResult', msg);                            

                    localStorage.removeItem('performRestore'); // clear flag for restore to queue up during next login

                    return true;

                } else {
                    $("#backupResult").html('Restore error: Incorrect response status code returned: ' + responseHex);
                    console.log('Restore error: Incorrect response status code returned:' + responseHex);
                    return false;
                }
                
            } else {
                $("#backupResult").html('Restore error: Incorrect response');
                console.log('Restore error: Incorrect response');
                return false;
            }

        })
        .catch((err) => {
            localStorage.setItem('restore-error', err);
            $("#backupResult").html('Restore error: ' + err);
            return false;
        })
    ;

}    

function showResidentKeys(data) {

    const residentKeys = data.residentKeys;

    if (!residentKeys) {
        $("#residentKeysList").html('No resident keys found');
        console.log('No resident keys found');
        return false;
    }

    let rkList = `<p>Following Discoverable Credentials are extracted from the latest backup created on: <b>${data.timestamp}</b>.</p><p>Keyvault Serial Number: <b>${data.sn}</b></p>`;

    rkList += `
    <table id="rkTable" class="display"><thead>
    <tr><td><h3>#</h3></td><td><h3>Username</h3></td><td><h3>Display Name</h3></td><td><h3>Relaying Party</h3></td><td><h3>User Id</h3></td><td><h3>User Id Crayonic Gateway</h3></td><td><h3>Credential Id</h3></td></tr></thead><tbody>`;
    
    
    residentKeys.map(rk => {
        rkList += `<tr><td>${(rk.rkIndex)+1}</td>`;
        rkList += `<td>${rk.credentialUserName}</td><td>${rk.credentialUserDisplayName}</td><td>${rk.rpId}</td>`;
        rkList += `<td>${rk.credentialUserId.substring(0,30)}...</td><td>${rk.credentialUserIdCGW}</td><td>${rk.credentialId.substring(0,30)}...</td>`;
        rkList += `</tr>`;

    });

    rkList += `</tbody></table>`;

    $("#residentKeysList").html(rkList);
    $('#rkTable').DataTable();    

    return true;

}

function showBackups(data) {

    if (!data) {
        $("#backupsList").html('No backups found');
        console.log('No backups found');
        return false;
    }

    let backupList = '';

    backupList += `<p>Showing last 20 backups...</p>
    <table id="backupsTable" class="display">
    <thead><tr><td><h3>#</h3></td><td><h3>Date/Time</h3></td><td><h3>KeyVault Serial Number</h3></td><td><h3>Backup Length</h3></td><td></td></tr></thead><tbody>`;
    
    data.forEach((element, index) => {
        if (index < 20) {
            backupList += `<tr><td>${index+1}</td><td>${element.backup.timestamp}</td>`;
            backupList += `<td>${element.backup.sn}</td><td>${element.backup.length}</td>`;
            backupList += `<td><button name="restoreButton" value="Restore" onclick="callRestore(${index});">Restore KeyVault</button></td>`;
            backupList += `</tr>`;    
        }

    });

    backupList += `</tbody></table>`;

    $("#backupsList").html(backupList);
    $('#backupsTable').DataTable();    

    return true;

}


// helpers
const fromHexString = (hexString) =>
new Uint8Array(hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)));

const toHexString = (bytes) =>
bytes.reduce((str, byte) => str + byte.toString(16).padStart(2, "0"), "");

function uint8array2hex(buffer) {
// buffer is an ArrayBuffer
return Array.prototype.map
    .call(buffer, (x) => ("00" + x.toString(16)).slice(-2))
    .join("");
}

function buf2hex(buffer) {
    // buffer is an ArrayBuffer
    return Array.prototype.map
        .call(new Uint8Array(buffer), (x) => ("00" + x.toString(16)).slice(-2))
        .join("");
}

// returns active gateway realm based on url
// e.g. account page for gateway realm: https://crayonic.io/auth/realms/gateway/account/#/personal-info
function getRealm() {
    const tmp = window.location.href.substring((window.location.href.search(/\/realms\//g))+8);
    const realm = tmp.substring(0, tmp.search(/\//g));

    if (realm)
        return realm;
    else {
        if (localStorage.getItem('loginRealm')) {
            return localStorage.getItem('loginRealm');
        }
        else {
            return false;
        }
    }

}
