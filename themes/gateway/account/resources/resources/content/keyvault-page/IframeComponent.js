import React from "../../../../common/keycloak/web_modules/react.js";
export default class IframeComponent extends React.Component {
  render() {
    return (/*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("iframe", {
        src: this.props.src,
        height: this.props.height,
        width: this.props.width
      }))
    );
  }

}
//# sourceMappingURL=IframeComponent.js.map