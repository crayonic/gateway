function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "../../../../common/keycloak/web_modules/react.js";
import ReactDOM from "../../../../common/keycloak/web_modules/react-dom.js";
import { Button, ButtonGroup, IconButton, Box, Container, Grid, GridList, Checkbox, Tooltip, Icon, Card, Typography, Backdrop, CircularProgress, CardContent, MenuItem, Select, createStyles, withStyles } from "../../../../common/keycloak/web_modules/@material-ui/core.js";
import { Fingerprint, TouchApp, Create, RecordVoiceOver, Dialpad, Block, Mic, Translate, FolderOpen, Save, CheckBoxOutlineBlank, CheckBox, SyncAlt } from "../../../../common/keycloak/web_modules/@material-ui/icons.js";
import { FormHelperText, FormControl, FormLabel, FormControlLabel, RadioGroup, Radio } from "../../../../common/keycloak/web_modules/@material-ui/core.js";
import { encodeSettings, decodeSettings, SettingsPropertiesDefinitions } from "./SettingsEncoderDecoder.js";
import { CrayonicSettingsRequest, CrayonicSettings } from "./storage.js";
import { fromHexString, toHexString } from "./utils.js"; // import queryString from 'query-string'

const queryString = null;

const styles = theme => createStyles({
  root: {
    flexGrow: 1,
    margin: 10
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  cardTitle: {
    fontSize: 14
  },
  backdrop: {
    zIndex: 10000,
    color: '#f00'
  }
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function Uint8Array2hex(arr) {
  return Array.prototype.map.call(arr, x => ("00" + x.toString(16)).slice(-2)).join("");
}

const PROPERTIES = SettingsPropertiesDefinitions;

function localized(resource) {
  return {
    time: "Time",
    uv_timeout_index: "User verification timeout",
    msc_timeout_index: "Mass storage timeout",
    unlock_timeout_index: "Unlock timeout",
    u2f_os: "U2F OS",
    u2f_up: "U2F User presence",
    fido_up: "FIDO User presence",
    fido_uv: "FIDO User verification",
    ble_use_rpas: "BLE Private Address",
    export_fingerprints_always: "Export FPs always",
    export_handwriting_always: "Export Handwr. always",
    nfc_to_st_safe_bridge: "Smartcard over NFC",
    client_pin: "Client PIN",
    logging_enabled: "USB Logging",
    fido_over_usb: "FIDO over USB",
    ccid_over_usb: "Smartcard over USB",
    ble_enabled: "BLE Enabled",
    nfc_enabled: "NFC Enabled",
    ble_pairing_over_nfc: "BLE Pairing over NFC",
    fido_over_nfc: "FIDO over NFC",
    timezone_offset: "Timezone offset",
    UP_UV_OPTIONS_AUTO: "None",
    UP_UV_OPTIONS_TOUCH: "Touch",
    UP_UV_OPTIONS_FINGERPRINT: "Fingerprint",
    UP_UV_OPTIONS_FINGERPRINT_OR_PASSCODE: "Fingerprint or passcode",
    UP_UV_OPTIONS_PASSCODE: "Passcode",
    UP_UV_OPTIONS_HANDWRITING: "Handwriting (coming soon)",
    UP_UV_OPTIONS_VOICE: "Voice (coming soon)",
    U2F_OS_OTHER: "Other OS",
    U2F_OS_LINUX_MACOS: "Linux/Mac"
  }[resource] || resource;
}

function icon(resource) {
  return {
    UP_UV_OPTIONS_AUTO: /*#__PURE__*/React.createElement(Block, null),
    UP_UV_OPTIONS_TOUCH: /*#__PURE__*/React.createElement(TouchApp, null),
    UP_UV_OPTIONS_FINGERPRINT: /*#__PURE__*/React.createElement(Fingerprint, null),
    UP_UV_OPTIONS_PASSCODE: /*#__PURE__*/React.createElement(Dialpad, null),
    UP_UV_OPTIONS_HANDWRITING: /*#__PURE__*/React.createElement(Translate, null),
    UP_UV_OPTIONS_VOICE: /*#__PURE__*/React.createElement(Mic, null)
  }[resource] || resource;
}

const SETTING_CATEGORIES = {
  "Timeouts": [,
  /*'timezone_offset'*/
  'uv_timeout_index', 'msc_timeout_index', 'unlock_timeout_index'],
  "Fido and U2F": ['u2f_up', 'fido_up', 'fido_uv', 'client_pin', 'u2f_os'],
  "Connectivity": ['fido_over_usb', 'nfc_to_st_safe_bridge', 'ccid_over_usb', 'ble_enabled', 'ble_use_rpas', 'fido_over_nfc', 'nfc_enabled', 'ble_pairing_over_nfc'],
  // "USB": ['fido_over_usb', 'ccid_over_usb'],
  // "Bluetooth": ['ble_enabled', 'ble_use_rpas', 'ble_pairing_over_nfc'],
  // "NFC": ['nfc_enabled', 'nfc_to_st_safe_bridge', 'fido_over_nfc', 'ble_pairing_over_nfc'],
  "Developer options": ['time', 'logging_enabled', 'export_handwriting_always', 'export_fingerprints_always']
};

class SettingsEditor extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "keyPress", event => {
      this.keyPressLog = (this.keyPressLog + event.key).slice(-5).toLowerCase(); // console.log(event);
      // console.log(this.keyPressLog)

      if (this.keyPressLog === "iddqd") {
        this.setState({
          showDeveloperOptions: !this.state.showDeveloperOptions
        });
      }
    });

    _defineProperty(this, "updateQueryString", () => {
      if (!queryString) {
        return;
      }

      if (this.props.updateQueryString) {
        const {
          time,
          ...settings
        } = this.getSettings();

        if (window.history.pushState) {
          const newQueryString = queryString.stringify({
            m: this.state.mode,
            p: toHexString(encodeSettings(settings))
          });
          var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + newQueryString;
          window.history.pushState({
            path: newurl
          }, '', newurl);
        }
      }
    });

    _defineProperty(this, "load_defaults", () => {
      this.setState(Object.assign({
        selected: Object.assign({
          mode: "settings"
        }, ...PROPERTIES.map(property => ({
          [property.name]: true
        })))
      }, ...PROPERTIES.map(property => ({
        [property.name]: property.default_value
      }))));
    });

    _defineProperty(this, "load", async () => {
      this.setState({
        inProgress: true
      });

      if (this.state.mode === 'settings' && !this.props.isKeycloakAccountEditor) {
        try {
          const response = await CrayonicSettings.load(true);

          if (toHexString(response.ret_code) !== "9000") {
            alert("Error occured: " + toHexString(response.ret_code));
          }

          const policy_with_time = decodeSettings(response.value);
          const {
            time,
            ...policy
          } = policy_with_time;
          this.setState({
            policy
          });
          console.log('policy', policy);
        } catch (e) {}
      }

      try {
        const response = await CrayonicSettings.load(this.state.mode === "policy");

        if (toHexString(response.ret_code) !== "9000") {
          alert("Error occured: " + toHexString(response.ret_code));
        }

        const settings = decodeSettings(response.value);
        this.setSettings(settings);
      } catch (e) {}

      this.setState({
        inProgress: false
      });
    });

    _defineProperty(this, "load_forever", async () => {
      let load_success_counter = 0;
      let load_fail_counter = 0;
      let load_forever = true;
      this.setState({
        load_forever: true,
        load_success_counter,
        load_fail_counter
      });

      while (load_forever) {
        try {
          const response = await CrayonicSettings.load(this.state.mode === "policy");

          if (toHexString(response.ret_code) !== "9000") {
            alert("Error occured: " + toHexString(response.ret_code));
          }

          const settings = decodeSettings(response.value);
          load_success_counter += 1;
        } catch (error) {
          load_fail_counter += 1;
        }

        this.setState({
          load_success_counter,
          load_fail_counter
        });
        await sleep(3000);
        load_forever = this.state.load_forever;
      }
    });

    _defineProperty(this, "stop_load_forever", async () => {
      this.setState({
        load_forever: false
      });
    });

    _defineProperty(this, "store", async () => {
      this.setState({
        inProgress: true
      }); // console.log('settings', this.getSettings(), toHexString(encodeSettings(this.getSettings())))
      // console.log('policy', this.getPolicy(), toHexString(encodeSettings(this.getPolicy())))

      try {
        if (this.props.isKeycloakAccountEditor) {
          const response = await CrayonicSettings.store(encodeSettings(this.getPolicy()), true // policy
          );

          if (toHexString(response.ret_code) !== "9000") {
            alert("Error occured while setting policy: " + toHexString(response.ret_code));
          }
        }

        const response = await CrayonicSettings.store(encodeSettings(this.getSettings()), this.state.mode === "policy");

        if (toHexString(response.ret_code) !== "9000") {
          alert("Error occured while storing settings: " + toHexString(response.ret_code));
        }
      } catch (e) {}

      this.setState({
        inProgress: false
      });
    });

    _defineProperty(this, "toggleProperty", name => {
      const selected = { ...this.state.selected
      };
      selected[name] = !selected[name];
      this.setState({
        selected
      });
    });

    _defineProperty(this, "selectAll", () => {
      const selected = { ...this.state.selected
      };

      for (const name of Object.keys(selected)) {
        selected[name] = true;
      }

      this.setState({
        selected
      });
    });

    _defineProperty(this, "selectNone", () => {
      const selected = { ...this.state.selected
      };

      for (const name of Object.keys(selected)) {
        selected[name] = false;
      }

      this.setState({
        selected
      });
    });

    _defineProperty(this, "invertSelection", () => {
      const selected = { ...this.state.selected
      };

      for (const name of Object.keys(selected)) {
        selected[name] = !selected[name];
      }

      this.setState({
        selected
      });
    });

    _defineProperty(this, "getSettings", () => {
      const state_copy = { ...this.state
      };
      state_copy.time = (new Date().getTime() / 1000).toFixed();
      return state_copy;
    });

    _defineProperty(this, "getPolicy", () => {
      const state_copy = { ...this.state.policy
      };
      state_copy.selected = Object.fromEntries(Object.keys(this.state.policy).map(k => [k, true]));
      state_copy.time = (new Date().getTime() / 1000).toFixed();
      return state_copy;
    });

    _defineProperty(this, "setSettings", settings => {
      const settings_copy = { ...settings
      };
      settings_copy.time = settings_copy.time - (new Date().getTime() / 1000).toFixed();
      const selected = { ...this.state.selected
      };

      for (const name of Object.keys(selected)) {
        selected[name] = false;
      }

      for (const name of Object.keys(settings_copy)) {
        selected[name] = true;
      }

      this.setState({ ...settings_copy,
        selected
      });
    });

    this.keyPressLog = "";
    this.state = Object.assign({
      inProgress: false,
      mode: props.mode || "settings",
      policy: {},
      showDeveloperOptions: false,
      selected: Object.assign({
        mode: "settings"
      }, ...PROPERTIES.map(property => ({
        [property.name]: true
      })))
    }, ...PROPERTIES.map(property => ({
      [property.name]: property.default_value
    })));
    console.log('constructor props', props);
  }

  async componentDidMount() {
    document.addEventListener("keydown", this.keyPress, false); // var query = queryString.parse(window.location.search);
    // if (query.p) {
    //   console.log(decodeSettings(
    //     fromHexString(query.p)
    //   ))
    //     this.setSettings(decodeSettings(
    //       fromHexString(query.p)
    //     ));
    // }
    // if (query.m) {
    //   this.setState({mode: query.m});
    // }

    if (this.props.isKeycloakPolicyEditor || this.props.isKeycloakAccountEditor) {
      this.setState({
        inProgress: true
      });

      if (this.props.isKeycloakPolicyEditor) {
        this.setState({
          selected: {}
        });
      }

      try {
        const result = await fetch(this.props.apiEndpoint + 'policy/?realm=' + this.props.realm, {
          method: 'get',
          headers: {
            "Authorization": `Bearer ${keycloak.token}`
          }  
        });
        const body_json = await result.json();

        if (body_json.kvpolicy) {
          console.log(body_json);
          const settings = decodeSettings(fromHexString(body_json.kvpolicy));

          if (this.props.isKeycloakPolicyEditor) {
            const selected = { ...this.state.selected
            };

            for (const name of Object.keys(selected)) {
              selected[name] = false;
            }

            for (const name of Object.keys(settings)) {
              selected[name] = true;
            }

            this.setSettings({ ...settings,
              selected
            });
          } else if (this.props.isKeycloakAccountEditor) {
            this.setState({
              policy: settings
            });
          }
        }
      } catch (error) {
        alert("Failed loading policy");
      }

      this.setState({
        inProgress: false
      });

      if (this.props.isKeycloakAccountEditor) {
        await this.load();
      }
    } else {
      await this.load();
    }

    this.reactUpdateInterval = setInterval(() => {
      this.forceUpdate();
      this.updateQueryString();
    }, 1000);
  }

  componentWillUnmount() {
    document.removeEventListener("keyup", this.keyPress, false);
    clearInterval(this.reactUpdateInterval);
  }

  render() {
    // console.log(this.state);
    const {
      classes,
      isKeycloakPolicyEditor
    } = this.props;
    const {
      mode
    } = this.state; // console.log(classes)

    return (/*#__PURE__*/React.createElement("div", {
        className: classes.root
      }, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Backdrop, {
        className: classes.backdrop,
        open: this.state.inProgress
      }, /*#__PURE__*/React.createElement(CircularProgress, {
        color: "inherit"
      })), /*#__PURE__*/React.createElement(Grid, {
        item: true,
        xs: 12
      }, /*#__PURE__*/React.createElement(Button, {
        style: {
          margin: 5
        },
        variant: "contained",
        color: "primary",
        startIcon: /*#__PURE__*/React.createElement(FolderOpen, null),
        onClick: this.load_defaults
      }, "LOAD DEFAULTS"), /*#__PURE__*/React.createElement(Button, {
        style: {
          margin: 5
        },
        variant: "contained",
        color: "primary",
        startIcon: /*#__PURE__*/React.createElement(FolderOpen, null),
        onClick: this.load
      }, "get ", mode), /*#__PURE__*/React.createElement(Button, {
        style: {
          margin: 5
        },
        variant: "contained",
        color: "primary",
        startIcon: /*#__PURE__*/React.createElement(Save, null),
        onClick: this.store
      }, "set ", mode), isKeycloakPolicyEditor ? /*#__PURE__*/React.createElement(Button, {
        style: {
          margin: 5
        },
        variant: "contained",
        color: "primary",
        startIcon: /*#__PURE__*/React.createElement(Save, null),
        onClick: async () => {
          if (this.props.onSave) {
            this.props.onSave(this.getSettings());
          } else {
            this.setState({
              inProgress: true
            });

            try {
              await fetch(this.props.apiEndpoint + 'policy/', {
                method: 'post',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                  "Authorization": `Bearer ${keycloak.token}`
                },
                body: new URLSearchParams({
                  realm: this.props.realm,
                  kvpolicy: Uint8Array2hex(encodeSettings(this.getSettings()))
                })
              });
            } catch (error) {
              alert("Failed saving policy");
            }

            this.setState({
              inProgress: false
            });
          }
        }
      }, "Save Policy for realm ", this.props.realm) : null), Object.keys(SETTING_CATEGORIES).filter(c => c != "Developer options" || this.state.showDeveloperOptions).map(category => /*#__PURE__*/React.createElement(Card, {
        className: classes.root,
        style: {
          margin: 10
        }
      }, /*#__PURE__*/React.createElement(CardContent, null, /*#__PURE__*/React.createElement(Typography, {
        className: classes.title,
        color: "textSecondary",
        gutterBottom: true
      }, category), /*#__PURE__*/React.createElement(Grid, {
        container: true,
        spacing: 2
      }, PROPERTIES.filter(property => SETTING_CATEGORIES[category].includes(property.name)).map(property => /*#__PURE__*/React.createElement(Grid, {
        key: property.name,
        item: true,
        xs: 12,
        md: 4,
        lg: 3
      }, true || mode !== 'settings' ? /*#__PURE__*/React.createElement(Checkbox, {
        checked: this.state.selected[property.name],
        onChange: () => {
          this.toggleProperty(property.name);
        },
        color: "primary"
      }) : null, /*#__PURE__*/React.createElement(FormControl, {
        disabled: !this.state.selected[property.name]
      }, /*#__PURE__*/React.createElement(Tooltip, {
        title: localized(property.name) // property.name +
        // " " +
        // property.data_type +
        // " [tlv_type=" +
        // property.tlv_type +
        // "] " +
        // toHexString(
        //   encodeSettings({
        //     [property.name]:
        //       property.name === "time"
        //         ? (new Date().getTime() / 1000).toFixed()
        //         : this.state[property.name],
        //     selected: { [property.name]: true }
        //   }).slice(1)
        // )

      }, /*#__PURE__*/React.createElement(FormHelperText, null, localized(property.name))), (() => {
        const enforced_by_policy = this.state.policy[property.name] !== undefined;

        if (property.name === "time") {
          return (/*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", null, new Date(new Date().getTime()).toLocaleString()), this.state[property.name] ? /*#__PURE__*/React.createElement("div", null, new Date(new Date().getTime() + this.state[property.name] * 1000).toLocaleString(), "[", this.state[property.name], "s]") : null)
          );
        } else if (property.multiselect) {
          // console.log(property, this.state[property.name]);
          return (/*#__PURE__*/React.createElement(ButtonGroup, {
              variant: "text",
              color: "primary",
              "aria-label": "text primary button group"
            }, property.enum_values.map(({
              value,
              label,
              coming_soon
            }) => /*#__PURE__*/React.createElement(Tooltip, {
              key: label,
              title: localized(label)
            }, /*#__PURE__*/React.createElement(Button, {
              disabled: !this.state.selected[property.name] || enforced_by_policy && !this.state.policy[property.name].includes(value),
              size: "small",
              color: this.state[property.name].includes(value) && !coming_soon ? "secondary" : "default",
              onClick: () => {
                if (coming_soon) {
                  return;
                } else if (this.state[property.name].includes(value)) {
                  const values = this.state[property.name].filter(v => v !== value);
                  this.setState({
                    [property.name]: values
                  });
                } else {
                  this.setState({
                    [property.name]: [...this.state[property.name], value]
                  });
                }
              }
            }, icon(label)))))
          );
        } else if (mode === 'policy' && property.data_type === 'uint24minmax') {
          return (/*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Select, {
              value: this.state[property.name].min,
              onChange: e => this.setState({
                [property.name]: { ...this.state[property.name],
                  min: e.target.value
                }
              })
            }, "enum_values" in property ? property.enum_values.map(({
              value,
              label
            }) => /*#__PURE__*/React.createElement(MenuItem, {
              key: label,
              value: value,
              disabled: value > this.state[property.name].max
            }, localized(label))) : null), /*#__PURE__*/React.createElement(Select, {
              value: this.state[property.name].max,
              onChange: e => this.setState({
                [property.name]: { ...this.state[property.name],
                  max: e.target.value
                }
              })
            }, "enum_values" in property ? property.enum_values.map(({
              value,
              label
            }) => /*#__PURE__*/React.createElement(MenuItem, {
              key: label,
              value: value,
              disabled: value < this.state[property.name].min
            }, localized(label))) : null))
          );
        } else {
          return (/*#__PURE__*/React.createElement(Select, {
              value: property.data_type === 'uint24minmax' ? this.state[property.name].min : this.state[property.name],
              onChange: e => {
                if (property.data_type === 'uint24minmax') {
                  this.setState({
                    [property.name]: {
                      min: e.target.value,
                      max: e.target.value
                    }
                  });
                } else {
                  this.setState({
                    [property.name]: e.target.value
                  });
                }
              }
            }, "enum_values" in property ? property.enum_values.map(({
              value,
              label
            }) => /*#__PURE__*/React.createElement(MenuItem, {
              key: label,
              value: value,
              disabled: !(() => {
                if (!enforced_by_policy) {
                  return true;
                } else if (property.data_type === 'uint24minmax') {
                  //console.log(property, value)
                  return value >= this.state.policy[property.name].min && value <= this.state.policy[property.name].max;
                } else {
                  return value === this.state.policy[property.name];
                }
              })()
            }, localized(label))) : null)
          );
        }
      })(), this.state.policy[property.name] !== undefined ? /*#__PURE__*/React.createElement(FormHelperText, null, "Enforced by policy") : null))))))), /*#__PURE__*/React.createElement(Grid, {
        item: true,
        xs: 12
      }, /*#__PURE__*/React.createElement(Button, {
        style: {
          margin: 5
        },
        variant: "contained",
        color: "primary",
        startIcon: /*#__PURE__*/React.createElement(CheckBox, null),
        onClick: this.selectAll
      }, "Select all"), /*#__PURE__*/React.createElement(Button, {
        style: {
          margin: 5
        },
        variant: "contained",
        color: "primary",
        startIcon: /*#__PURE__*/React.createElement(CheckBoxOutlineBlank, null),
        onClick: this.selectNone
      }, "select none"), /*#__PURE__*/React.createElement(Button, {
        style: {
          margin: 5
        },
        variant: "contained",
        color: "primary",
        startIcon: /*#__PURE__*/React.createElement(SyncAlt, null),
        onClick: this.invertSelection
      }, "invert selection"))))
    );
  }

}

const SettingsEditorWith = withStyles(styles)(SettingsEditor);
export { SettingsEditorWith as SettingsEditor };
export default withStyles(styles)(SettingsEditor);
//# sourceMappingURL=SettingsEditor.js.map