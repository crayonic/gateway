import React, { Component } from "../../../../common/keycloak/web_modules/react.js";
import { getRealm } from "./utils.js";
import { SettingsEditor } from "./SettingsEditor.js";

class SettingsEditorKeyvaultAccountWrapper extends Component {
  render() {
    return (/*#__PURE__*/React.createElement(SettingsEditor, {
        updateQueryString: false,
        isKeycloakAccountEditor: true,
        apiEndpoint: "/cgw/" // apiEndpoint='http://localhost:8443/cgw/'
        ,
        realm: getRealm()
      })
    );
  }

}

export { SettingsEditorKeyvaultAccountWrapper };
//# sourceMappingURL=SettingsEditorKeyvaultAccountWrapper.js.map