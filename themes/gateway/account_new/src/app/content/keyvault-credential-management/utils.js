export const fromHexString = (hexString) =>
  new Uint8Array(hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)));

export const toHexString = (bytes) =>
  bytes.reduce((str, byte) => str + byte.toString(16).padStart(2, "0"), "");

export function getRealm() {
  const tmp = window.location.href.substring((window.location.href.search(/\/realms\//g))+8);
  const realm = tmp.substring(0, tmp.search(/\//g));
  return (realm) ? realm : '';
}