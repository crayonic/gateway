/*global keycloak*/

import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  Button,
  ButtonGroup,
  IconButton,
  Box,
  Container,
  Grid,
  GridList,
  Checkbox,
  Tooltip,
  Icon, 
  Card,
  List, ListItem, ListItemAvatar, Avatar, ListItemText, ListItemSecondaryAction, ListSubheader,
  Typography,
  Backdrop, CircularProgress,
  CardContent, MenuItem,
  Select,
  createStyles,
  withStyles
} from "@material-ui/core";

import {
  Fingerprint,
  TouchApp,
  Create,
  RecordVoiceOver,
  Dialpad,
  Block,
  Mic,
  Translate,
  FolderOpen, Save,
  CheckBoxOutlineBlank, CheckBox, SyncAlt,
  AccountCircle, Delete
} from "@material-ui/icons";
import {
  FormHelperText,
  FormControl,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio
} from "@material-ui/core";

import { CrayonicBackup } from './crayonic';
import { fromHexString } from './utils';

const styles = (theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    backdrop: {
      zIndex: 10000,
      color: '#f00',
    },
  }
);


class CredentialManagementEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // credentials: {"timestamp":"Fri, 16 Apr 2021 09:15:21 GMT","sn":"F96E36327FC5","rkCnt":4,"residentKeys":[{"rkIndex":0,"credentialId":"4b56bd87e10b89feb7af65ff5e885f535238f72c277dcd5262e25aa4825a67b1218fe2d82647e6fc646b9ce1488bbe2a879ec8b345a6e1b275628cacf36ddfc946671ee729000000","credentialUserId":"32653337303730632d613337352d346234302d393439642d363761616565623137633562","credentialUserIdCGW":"2e37070c-a375-4b40-949d-67aaeeb17c5b","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"crayonic.io"},{"rkIndex":1,"credentialId":"4b566d2923d63ac739b3d55c67beb357df68e480d4418b9968e1bb6fadd472aec3773a2f49960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d97634b000000","credentialUserId":"33616335616462312d323566352d343738312d396265632d363139303965333563663132","credentialUserIdCGW":"3ac5adb1-25f5-4781-9bec-61909e35cf12","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"},{"rkIndex":2,"credentialId":"4b5675b0db3c333f400ff923c4a65a7f532e4a0a001bdef218e8ad724fba12291d8896f849960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d9763f2000000","credentialUserId":"30626663373635302d303432612d343132642d393862302d626561323566373064623832","credentialUserIdCGW":"0bfc7650-042a-412d-98b0-bea25f70db82","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"},{"rkIndex":3,"credentialId":"4b56ac0084f5a638d1598343c1e599ecec10d6dc70da44f420acffd4debdcf90c5847b5a49960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d97635a020000","credentialUserId":"34313865336637302d323133322d346464662d393566322d366431666530383639616236","credentialUserIdCGW":"418e3f70-2132-4ddf-95f2-6d1fe0869ab6","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"}]}
      credentials: {},
      isEdited: false
    }
  }

  removeResidentKey = (rkIndex) => {
    this.setState({
      credentials: {
        ...this.state.credentials,
        residentKeys: this.state.credentials.residentKeys.filter(rk => rk.rkIndex !== rkIndex)
      },
      isEdited: true
    })
  }

  saveResidentKeys = async () => {
    await fetch(
      this.props.apiEndpoint + 'credentials/rk',
      {
        method: 'POST',
        headers: {
          "Authorization": `Bearer ${keycloak.token}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          realm: this.props.realm,
          username: keycloak.tokenParsed.preferred_username,
          payload: this.state.credentials
        })
      }
    )
  }

  saveAndSyncToKeyvault = async() => {
    this.setState({isLoading: true})

    try {
      await this.saveResidentKeys();
      const credentials = await this.fetchCredentials();
      console.log('credentials to be restored', credentials)
      await CrayonicBackup.restore(fromHexString(credentials.payload));
    } catch (error) {
      console.error(error)
      alert("Failed saving backup")
    }
    this.setState({isLoading: false})
  }

  fetchCredentials = async() => {
    try {
      const result = await fetch(
        this.props.apiEndpoint + 'credentials/rk?realm=' + this.props.realm + '&username=' + keycloak.tokenParsed.preferred_username, 
        {
          method: 'get',
          headers: {
            "Authorization": `Bearer ${keycloak.token}`
          }
        }
      )
      const credentials = await result.json();
      console.log(credentials);
      this.setState({credentials, isEdited: false})
      return credentials;
    } catch (error) {
      console.error(error)
      alert("Failed loading backup")
    }
  }

  async componentDidMount() {
    // return
    this.setState({isLoading: true})
    await this.fetchCredentials();
    this.setState({isLoading: false})
  }

  render() {
    const { classes } = this.props;
    const { credentials } = this.state;
    const residentKeys = credentials.residentKeys || []
    const dense = false;

    console.log('keycloak', keycloak)

    return(<>
      <Backdrop className={classes.backdrop} open={this.state.isLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Typography variant="h6" className={classes.title}>
        Credentials backup for SN={credentials.sn} from {credentials.timestamp}
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6} style={{justifyContent: 'space-between'}}>
          <div className={classes.demo}>
            <List dense={dense} subheader={<ListSubheader>Discoverable credentials (Resident keys)</ListSubheader>}>
              {residentKeys.map(rk => (
                <ListItem key={rk.rkIndex}>
                  <ListItemAvatar>
                    <Avatar>
                      <AccountCircle />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={`${rk.credentialUserDisplayName}` + ((rk.credentialUserName !== rk.credentialUserDisplayName) ? ` (${rk.credentialUserName})` : "")}
                    secondary={`${rk.rpId}`}
                  />
                  <ListItemSecondaryAction onClick={() => this.removeResidentKey(rk.rkIndex)}>
                    <IconButton edge="end" aria-label="delete">
                      <Delete />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </div>
          {(this.state.isEdited)
          ? <Grid item xs={12} md={12} className={classes.root}>
            <Button variant="contained" onClick={this.fetchCredentials}>
              Cancel
            </Button>
            <Button variant="contained" color="primary" onClick={this.saveAndSyncToKeyvault}>
              Save and update keyvault
            </Button> 
          </Grid>
          : null}
        </Grid>
      </Grid>
      {/* <pre>
        {JSON.stringify(this.state.credentials, null, 4)}
      </pre> */}
    </>)
  }
}

const CredentialManagementEditorWith = withStyles(styles)(CredentialManagementEditor);
export {CredentialManagementEditorWith as CredentialManagementEditor};
export default withStyles(styles)(CredentialManagementEditor);
