<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <div class="row">
        <div class="col-md-10">
            <h2>${msg("editAccountHtmlTitle")}&nbsp;${(account.username!'')}</h2>
        </div>
        <div class="col-md-2 subtitle">
            <#--  <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>  -->
            <span class="subtitle"><span class="required"></span></span>            
        </div>
    </div>

    <form action="${url.accountUrl}" class="form-horizontal" method="post">

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

        <#if !realm.registrationEmailAsUsername>
            <div class="form-group ${messagesPerField.printIfExists('username','has-error')}">
                <div class="col-sm-2 col-md-2">
                    <label for="username" class="control-label">${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>
                </div>

                <div class="col-sm-10 col-md-10">
                    <input type="text" class="form-control" id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>
                </div>
            </div>
        </#if>

        <div class="form-group ${messagesPerField.printIfExists('email','has-error')}">
            <div class="col-sm-2 col-md-2">
            <label for="email" class="control-label">${msg("email")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="email" name="email" autofocus value="${(account.email!'')}" disabled="disabled"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="firstName" name="firstName" value="${(account.firstName!'')}" disabled="disabled"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="lastName" name="lastName" value="${(account.lastName!'')}" disabled="disabled"/>
            </div>
        </div>

        <#--  <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <#if url.referrerURI??><a href="${url.referrerURI}">${kcSanitize(msg("backToApplication")?no_esc)}</a></#if>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Cancel">${msg("doCancel")}</button>
                </div>
            </div>
        </div>  -->

    </form>

    <p></p>

    <p><div>
        <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="settingsButton" value="Settings" onclick="callSettings();">Keyvault Settings</button>
        &nbsp;&nbsp;&nbsp;
        <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="backupButton" value="Backup" onclick="callBackup();">Backup KeyVault</button>
        &nbsp;&nbsp;&nbsp;
        <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="restoreButton" value="Restore" onclick="callRestore();">Restore KeyVault</button>
        &nbsp;&nbsp;&nbsp;
        <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="residentkeysButton" value="ResidentKeys" onclick="callShowResidentKeys();">Show Resident Keys</button>
    </div><br></p>

    <p><textarea id="backupResult" placeholder="Backup/Restore output" cols="100" rows="5" style="font-size: small;"></textarea></p>

    <p><div id="residentKeysList" style="font-size: small;"></div></p>
    <p><br></p>

    <#--  jquery.min.js and account.js should point to your host site -->
    <script type="text/javascript" src="/jquery.min.js"></script>
    <script type="text/javascript" src="/account.js"></script>

    <script type="text/javascript">
        //must be in template file! (.ftl)
        window.onload = () => {

//            const username = "${(account.username!'')}";
            gatewayStart();
        };
    </script>

</@layout.mainLayout>
