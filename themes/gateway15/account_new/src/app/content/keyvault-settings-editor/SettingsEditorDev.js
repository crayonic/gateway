import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  Button,
  ButtonGroup,
  IconButton,
  Box,
  Container,
  Grid,
  GridList,
  Checkbox,
  Tooltip,
  Icon
} from "@material-ui/core";
import {
  Fingerprint,
  TouchApp,
  Create,
  RecordVoiceOver,
  Dialpad,
  Block,
  Mic,
  Translate
} from "@material-ui/icons";
import { Input, TextField, Alert } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import SendIcon from "@material-ui/icons/Send";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import {
  FormHelperText,
  FormControl,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio
} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import {
  makeStyles,
  createStyles,
  Theme,
  withStyles
} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

import {
  encodeSettings,
  decodeSettings,
  SettingsPropertiesDefinitions
} from "./SettingsEncoderDecoder";

import { CrayonicSettingsRequest, CrayonicSettings } from "./storage";
import { fromHexString, toHexString } from "./utils";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary
    }
  })
);

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function Uint8Array2hex(arr) {
  return Array.prototype.map
    .call(arr, (x) => ("00" + x.toString(16)).slice(-2))
    .join("");
}

const PROPERTIES = SettingsPropertiesDefinitions;
function localized(resource) {
  return (
    {
      time: "Time",
      uv_timeout_index: "User verification timeout",
      msc_timeout_index: "Mass storage timeout",
      unlock_timeout_index: "Unlock timeout",
      u2f_os: "U2F OS",
      u2f_up: "U2F User presence",
      fido_up: "FIDO User presence",
      fido_uv: "FIDO User verification",
      ble_use_rpas: "BLE Private Address",
      export_fingerprints_always: "Export FPs always",
      export_handwriting_always: "Export Handwr. always",
      nfc_to_st_safe_bridge: "NFC to ST bridge",
      client_pin: "Client PIN",
      logging_enabled: "USB Logging",
      fido_over_usb: "FIDO over USB",
      ccid_over_usb: "CCID over USB",
      ble_enabled: "BLE Enabled",
      UP_UV_OPTIONS_AUTO: "No verification",
      UP_UV_OPTIONS_TOUCH: "Touch",
      UP_UV_OPTIONS_FINGERPRINT: "Fingerprint",
      UP_UV_OPTIONS_FINGERPRINT_OR_PASSCODE: "Fingerprint or passcode",
      UP_UV_OPTIONS_PASSCODE: "Passcode",
      UP_UV_OPTIONS_HANDWRITING: "Handwriting",
      UP_UV_OPTIONS_VOICE: "Voice",
      U2F_OS_OTHER: "Other OS",
      U2F_OS_LINUX_MACOS: "Linux/Mac"
    }[resource] || resource
  );
}

function icon(resource) {
  return (
    {
      UP_UV_OPTIONS_AUTO: <Block />,
      UP_UV_OPTIONS_TOUCH: <TouchApp />,
      UP_UV_OPTIONS_FINGERPRINT: <Fingerprint />,
      UP_UV_OPTIONS_PASSCODE: <Dialpad />,
      UP_UV_OPTIONS_HANDWRITING: <Translate />,
      UP_UV_OPTIONS_VOICE: <Mic />
    }[resource] || resource
  );
}

class SettingsEditor extends Component {
  constructor(props) {
    super(props);
    this.state = Object.assign(
      {
        mode: "settings",
        selected: Object.assign(
          {
            mode: "settings"
          },
          ...PROPERTIES.map((property) => ({
            [property.name]: true
          }))
        )
      },
      ...PROPERTIES.map((property) => ({
        [property.name]: property.default_value
      }))
    );
  }

  componentDidMount() {
    this.reactUpdateInterval = setInterval(() => {
      this.forceUpdate();
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.reactUpdateInterval);
  }

  load = async () => {
    const response = await CrayonicSettings.load(this.state.mode === "policy");
    if (toHexString(response.ret_code) !== "9000") {
      alert("Error occured: " + toHexString(response.ret_code));
    }
    const settings = decodeSettings(response.value);
    this.setSettings(settings);
  };

  load_forever = async () => {
    let load_success_counter = 0;
    let load_fail_counter = 0;
    let load_forever = true;
    this.setState({load_forever: true, load_success_counter, load_fail_counter});

    while(load_forever) {
      try {
        await this.load();
        load_success_counter += 1;
      } catch (error) {
        load_fail_counter += 1;
      }
      this.setState({load_success_counter, load_fail_counter});
      await sleep(3000);
      load_forever = this.state.load_forever
    }
  }

  stop_load_forever = async () => {
    this.setState({load_forever: false});
  }

  store = async () => {
    const response = await CrayonicSettings.store(
      encodeSettings(this.getSettings()),
      this.state.mode === "policy"
    );
    if (toHexString(response.ret_code) !== "9000") {
      alert("Error occured: " + toHexString(response.ret_code));
    }
  };

  toggleProperty = (name) => {
    const selected = { ...this.state.selected };
    selected[name] = !selected[name];
    this.setState({ selected });
  };

  selectAll = () => {
    const selected = { ...this.state.selected };
    for (const name of Object.keys(selected)) {
      selected[name] = true;
    }
    this.setState({ selected });
  };

  selectNone = () => {
    const selected = { ...this.state.selected };
    for (const name of Object.keys(selected)) {
      selected[name] = false;
    }
    this.setState({ selected });
  };

  invertSelection = () => {
    const selected = { ...this.state.selected };
    for (const name of Object.keys(selected)) {
      selected[name] = !selected[name];
    }
    this.setState({ selected });
  };

  getSettings = () => {
    const state_copy = { ...this.state };
    state_copy.time = (new Date().getTime() / 1000).toFixed();
    return state_copy;
  };

  setSettings = (settings) => {
    const settings_copy = { ...settings };
    settings_copy.time =
      settings_copy.time - (new Date().getTime() / 1000).toFixed();
    const selected = { ...this.state.selected };
    for (const name of Object.keys(settings_copy)) {
      selected[name] = false;
    }
    for (const name of Object.keys(settings_copy)) {
      selected[name] = true;
    }
    this.setState({ ...settings_copy, selected });
  };

  render() {
    // console.log(this.state);
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Container>
          {/* <pre>{JSON.stringify(this.state, null, 4)}</pre> */}
          <Button
            variant="contained"
            color="primary"
            // startIcon={<SendIcon />}
            onClick={this.selectAll}
          >
            Select all
          </Button>
          <Button
            variant="contained"
            color="primary"
            // startIcon={<SendIcon />}
            onClick={this.selectNone}
          >
            select none
          </Button>
          <Button
            variant="contained"
            color="primary"
            // startIcon={<SendIcon />}
            onClick={this.invertSelection}
          >
            invert selection
          </Button>
          <Grid container spacing={2}>
            {PROPERTIES.map((property) => (
              <Grid key={property.name} item xs={12} md={4} lg={3}>
                <Paper className={classes.paper}>
                  <FormControl>
                    {/* <InputLabel>{localized(property.name)}</InputLabel> */}
                    <Tooltip
                      title={
                        property.name +
                        " " +
                        property.data_type +
                        " [tlv_type=" +
                        property.tlv_type +
                        "] " +
                        toHexString(
                          encodeSettings({
                            [property.name]:
                              property.name === "time"
                                ? (new Date().getTime() / 1000).toFixed()
                                : this.state[property.name],
                            selected: { [property.name]: true }
                          }).slice(1)
                        )
                      }
                    >
                      <FormHelperText>
                        <Checkbox
                          checked={this.state.selected[property.name]}
                          onChange={() => {
                            this.toggleProperty(property.name);
                          }}
                          color="primary"
                        />
                        {localized(property.name)}
                      </FormHelperText>
                    </Tooltip>
                    {(() => {
                      if (property.name === "time") {
                        return (
                          <div>
                            <div>
                              {new Date(new Date().getTime()).toLocaleString()}
                            </div>
                            {this.state[property.name] ? (
                              <div>
                                {new Date(
                                  new Date().getTime() +
                                    this.state[property.name] * 1000
                                ).toLocaleString()}
                                [{this.state[property.name]}s]
                              </div>
                            ) : null}
                          </div>
                        );
                      } else if (property.multiselect) {
                        // console.log(property, this.state[property.name]);
                        return (
                          <ButtonGroup
                            variant="text"
                            color="primary"
                            aria-label="text primary button group"
                          >
                            {property.enum_values.map(({ value, label }) => (
                              <Tooltip key={label} title={localized(label)}>
                                <Button
                                  size="small"
                                  color={
                                    this.state[property.name].includes(value)
                                      ? "secondary"
                                      : "default"
                                  }
                                  onClick={() => {
                                    if (
                                      this.state[property.name].includes(value)
                                    ) {
                                      const values = this.state[
                                        property.name
                                      ].filter((v) => v !== value);
                                      this.setState({
                                        [property.name]: values
                                      });
                                    } else {
                                      this.setState({
                                        [property.name]: [
                                          ...this.state[property.name],
                                          value
                                        ]
                                      });
                                    }
                                  }}
                                >
                                  {icon(label)}
                                </Button>
                              </Tooltip>
                            ))}
                          </ButtonGroup>
                        );
                      } else {
                        return (
                          <Select
                            value={this.state[property.name]}
                            onChange={(e) =>
                              this.setState({ [property.name]: e.target.value })
                            }
                          >
                            {"enum_values" in property
                              ? property.enum_values.map(({ value, label }) => (
                                  <MenuItem key={label} value={value}>
                                    {localized(label)}
                                  </MenuItem>
                                ))
                              : null}
                          </Select>
                        );
                      }
                    })()}
                  </FormControl>
                </Paper>
              </Grid>
            ))}
            <Grid item xs={12}>
              <FormControl component="fieldset">
                <RadioGroup
                  aria-label="gender"
                  name="gender1"
                  value={this.state.mode}
                  onChange={(e) => this.setState({ mode: e.target.value })}
                >
                  <FormControlLabel
                    value="settings"
                    control={<Radio />}
                    label="Settings"
                  />
                  <FormControlLabel
                    value="policy"
                    control={<Radio />}
                    label="Policy"
                  />
                </RadioGroup>
              </FormControl>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SendIcon />}
                onClick={this.load}
              >
                load
              </Button>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SendIcon />}
                onClick={this.store}
              >
                store
              </Button>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SendIcon />}
                onClick={this.load_forever}
              >
                load forever
              </Button>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SendIcon />}
                onClick={this.stop_load_forever}
              >
                stop load forever
              </Button>
              Success: {this.state.load_success_counter} Fail: {this.state.load_fail_counter}
              {/* {toHexString(
                CrayonicSettingsRequest.store(
                  encodeSettings(this.getSettings()),
                  this.state.mode === "policy"
                )
              )
                .match(/.{1,2}/g)
                .join(" ")} */}
            </Grid>
            <Grid item>
              {Uint8Array2hex(encodeSettings(this.getSettings()))
                .match(/.{1,2}/g)
                .join(" ")}

              <Button
                variant="contained"
                color="primary"
                onClick={() =>
                  this.setState({
                    settings_byte_representation: Uint8Array2hex(
                      encodeSettings(this.getSettings())
                    )
                  })
                }
              >
                Copy for manual edit
              </Button>
              <TextField
                required
                id="standard-required"
                // label="Required"
                helperText="settings byte representation"
                fullWidth={true}
                value={this.state.settings_byte_representation}
                onChange={(e) =>
                  this.setState({
                    settings_byte_representation: e.target.value
                  })
                }
              />
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  const settings = decodeSettings(
                    fromHexString(this.state.settings_byte_representation)
                  );
                  const selected = { ...this.state.selected };
                  for (const name of Object.keys(selected)) {
                    selected[name] = false;
                  }
                  for (const name of Object.keys(settings)) {
                    selected[name] = true;
                  }
                  this.setSettings({ ...settings, selected });
                }}
              >
                Decode and load byte representation
              </Button>
            </Grid>
          </Grid>
        </Container>
      </div>
    );
  }
}

export default withStyles(useStyles)(SettingsEditor);
