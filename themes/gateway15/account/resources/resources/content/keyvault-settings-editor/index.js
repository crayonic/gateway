function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "../../../../common/keycloak/web_modules/react.js";
import ReactDOM from "../../../../common/keycloak/web_modules/react-dom.js";
import { Button, Box, Container, Grid, GridList } from "../../../../common/keycloak/web_modules/@material-ui/core.js";
import { Input, TextField, Alert } from "../../../../common/keycloak/web_modules/@material-ui/core.js";
import Icon from "@material-ui/core/Icon";
import DeleteIcon from "@material-ui/icons/Delete";
import SendIcon from "@material-ui/icons/Send";
import SettingsEditor from "./SettingsEditor.js";
import { CrayonicKeyvalueStorage } from "./storage.js";

class App extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "set", async () => {
      await CrayonicKeyvalueStorage.set(this.state.key, this.state.value);
    });

    _defineProperty(this, "get", async () => {
      this.setState({
        value: ""
      });
      const value = await CrayonicKeyvalueStorage.getAsString(this.state.key);
      this.setState({
        value
      });
    });

    this.state = {
      result: null,
      payloadToSend: "1234567890",
      key: "bitwarden masterPassword user@example.com",
      value: "placeholder value"
    };
  }

  render() {
    return (/*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Grid, {
        container: true,
        spacing: 3
      }, /*#__PURE__*/React.createElement(Grid, {
        item: true,
        xs: 12
      }, /*#__PURE__*/React.createElement(SettingsEditor, null)))))
    );
  }

}

class AppWrapper extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      some_value: 'aaa'
    });
  }

  render() {
    return this.props.children;
  }

}

_defineProperty(AppWrapper, "propTypes", {});

const MOUNT_NODE_PROPERTY_NAME = '_mount_node';
const DEV = process.env.NODE_ENV === 'development';

if (DEV) {
  ReactDOM.render( /*#__PURE__*/React.createElement(AppWrapper, null, /*#__PURE__*/React.createElement(App, null)), document.querySelector("#app"));
} else {
  window.KEYVAULT_SETTINGS_EDITOR = {
    createInstance: node => {
      return new Promise((resolve, reject) => {
        let instance = null;
        ReactDOM.render( /*#__PURE__*/React.createElement(AppWrapper, {
          ref: ref => {
            instance = ref;
          }
        }, /*#__PURE__*/React.createElement(App, null)), node, () => {
          instance[MOUNT_NODE_PROPERTY_NAME] = node;
          resolve(instance);
        });
      });
    },
    destroyInstance: ref => {
      // console.log('destroy', ref);
      return ReactDOM.unmountComponentAtNode(ref[MOUNT_NODE_PROPERTY_NAME]);
    }
  };
} //usage
// let instance = null;
// window.KEYVAULT_SETTINGS_EDITOR.createInstance(document.querySelector("#app")).then(
//     (ref) => {
//       instance = ref;
//       console.log('ready', ref.state);
//       setTimeout(() => {
//           ref.setState({
//               some_value: 'bbb'
//           }, () => {
//               console.log('value set', ref.state);
//           });
//       }, 500);
//     }
// );
//
// setTimeout(() => {
//     console.log('destroy result', window.KEYVAULT_SETTINGS_EDITOR.destroyInstance(instance));
// }, 5000);
//# sourceMappingURL=index.js.map