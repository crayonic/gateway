//
// Managing Keyvault device
// CHM/Crayonic, 4/2021
//
import * as React from "../../../../common/keycloak/web_modules/react.js";
import { DataList, DataListItem, DataListItemRow, DataListCell, DataListToggle, DataListContent, DataListItemCells, Grid, GridItem, Button } from "../../../../common/keycloak/web_modules/@patternfly/react-core.js";
import { TextField } from "../../../../common/keycloak/web_modules/@material-ui/core.js";
import IframeComponent from "./IframeComponent.js";
export class KeyvaultPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'React',
      backupResult: null,
      residentKeysList: null
    };
  }

  updateContent() {
    this.setState({
      backupResult: localStorage.getItem('backupResult'),
      residentKeysList: localStorage.getItem('residentKeysList')
    });
  } // onClick: () => { callBackup(this.setState) }


  render() {
    const divStyle = {
      display: 'inline-block',
      margin: '0 auto'
    };
    const keyvaultFrame = React.createElement(IframeComponent, {
      src: "/keyvault.html",
      style: {
        divStyle
      },
      height: "1000px !important",
      width: "100%"
    }, 'Manage your Keyvault device');
    const container = React.createElement('div', {}, [keyvaultFrame]);
    return container;
  }

} 
