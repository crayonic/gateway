import React, { Component } from "../../../../common/keycloak/web_modules/react.js";
import { getRealm } from "./utils.js";
import { Certificates } from "./Certificates.js";
import { KeycloakContext } from "../../keycloak-service/KeycloakContext.js";
import { KeycloakService } from "../../keycloak-service/keycloak.service.js";

class CertificatesAccountWrapper extends Component {
  render() {
    return (/*#__PURE__*/React.createElement(KeycloakContext.Consumer, null, keycloak => /*#__PURE__*/React.createElement(Certificates, {
        keycloak: keycloak,
        apiEndpoint: "/cgw/" // apiEndpoint='http://localhost:8443/cgw/'
        // apiEndpoint='https://crayonic.io/cgw/'
        ,
        realm: getRealm()
      }))
    );
  }

}

export { CertificatesAccountWrapper };
//# sourceMappingURL=CertificatesAccountWrapper.js.map