import React, { Component } from "react";
import { getRealm } from './utils';
import { Certificates } from './Certificates';

import { KeycloakContext } from '../../keycloak-service/KeycloakContext';
import { KeycloakService } from '../../keycloak-service/keycloak.service';

class CertificatesAccountWrapper extends Component {
  
render() {
    return (
      <KeycloakContext.Consumer>
          { (keycloak) => (
            <Certificates 
              keycloak={keycloak}
              apiEndpoint='/cgw/'
              // apiEndpoint='http://localhost:8443/cgw/'
              // apiEndpoint='https://crayonic.io/cgw/'
              realm={getRealm()}
            />
          )}
      </KeycloakContext.Consumer>
    )
  }
}

export { CertificatesAccountWrapper };
