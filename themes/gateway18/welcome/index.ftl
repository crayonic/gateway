<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Crayonic Gateway Administration Console</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.stylesCommon?has_content>
        <#list properties.stylesCommon?split(' ') as style>
            <link href="${resourcesCommonPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>

</head>
<body>
<p align="center">
<img src="welcome-content/gateway-logo.png" alt="Crayonic Gateway" width="400" align="center">
<br>
<div class="d1">
  <h3><a href="${adminUrl}"><b>Administration Console</b></a> - Manage Crayonic Gateway (admin access)</h3>
</div>
</p>

</body>
</html>
