/*global keycloak*/

import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  Button,
  ButtonGroup,
  IconButton,
  Box,
  Container,
  Grid,
  GridList,
  Checkbox,
  Tooltip,
  Icon, 
  Card,
  List, ListItem, ListItemAvatar, Avatar, ListItemText, ListItemSecondaryAction, ListSubheader,
  Typography,
  Backdrop, CircularProgress,
  CardContent, MenuItem,
  Select,
  createStyles,
  withStyles
} from "@material-ui/core";

import {
  Fingerprint,
  TouchApp,
  Create,
  RecordVoiceOver,
  Dialpad,
  Block,
  Mic,
  Translate,
  FolderOpen, Save,
  CheckBoxOutlineBlank, CheckBox, SyncAlt,
  AccountCircle, Delete, GetApp, Sync, VpnKey, Refresh, AlternateEmail, CloudDownload
} from "@material-ui/icons";
import {
  FormHelperText,
  FormControl,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio
} from "@material-ui/core";

// import { Certificate } from '@fidm/x509';

import {
  CrayonicKeyvalueStorage,
  CrayonicPivManager,
  makeCredentialsOptions
} from "./storage.ts";


import crypto from "crypto-browserify";
import forge from "node-forge";


const styles = (theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      margin: 10
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary
    },
    cardTitle: {
      fontSize: 14,
    },
    backdrop: {
      zIndex: 10000,
      color: 'white',
    },
}
);


// const MOCK_ACCOUNT_DATA = true;
const fromHexString = hexString =>
  new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
const toHexString = bytes =>
  bytes.reduce((str, byte) => str + byte.toString(16).padStart(2, '0'), '');
function str2ab(str) {
  // var s = encode_utf8(str);
  var s = str;
  var buf = new ArrayBuffer(s.length);
  var bufView = new Uint8Array(buf);
  for (var i = 0, strLen = s.length; i < strLen; i++) {
    bufView[i] = s.charCodeAt(i);
  }
  return bufView;
}

function b64toUint8Array(b64str) {
  return Uint8Array.from(atob(b64str), c => c.charCodeAt(0))
}

function encrypt3DES(data, key) {
  const cipher = crypto.createCipheriv("des-ede3", key, "");

  let encrypted = cipher.update(data, "binary", "binary");
  // console.log(encrypted);
  encrypted += cipher.final("binary");
  // console.log(encrypted);
  return encrypted.slice(0, 8);
}

function decrypt3DES(data, key) {
  const decipher = crypto.createDecipheriv("des-ede3", key, "");

  let decrypted = decipher.update(data, "binary", "binary");
  // console.log(decrypted.toString(), typeof decrypted, decrypted.length);
  decrypted += decipher.update(data, "binary", "binary");
  // console.log(decrypted.toString(), typeof decrypted, decrypted.length);
  return decrypted.slice(0, 8); // 3DES block size
}

// export const keyvaultCert = fromHexString("5c035fc105538202b3708202aa308202a63082018ea00302010202084561ce82dfd67a34300d06092a864886f70d01010b050030133111300f060355040313084b65797661756c74301e170d3232303131373039333930305a170d3233303131373039333930305a30133111300f060355040313084b65797661756c7430820122300d06092a864886f70d01010105000382010f003082010a0282010100c0a7fb3c3db56767915311a57c174e6bc1a9b043953ac6aed88ae940b41e10fc8d7fee03255459264444d84f23436e77d90f5cacb8d1b9d3e2f7347d6fee00ae2cc8c5aa27d6b95199052132dee16adef011a9adbb6109cc97cfc01f35ab073736bebf98512aecaff43562c8bb91c1344ffc7c9627d967faa79c47092988321eb7e7cd2edfcb274506f89480478b54e531a8dae25c4bff9be5a971b37250993bf6cbbe05e256fbda7689e16555875c6d71909168f1a82590b2a08fcc7083d1a2ed7f70823df866a2dfa62d17160b76b851043bd90c5d7b653e18f2c0a9e5c47d0c99c8de1570aff2d644c2d3585e456af380ea3568113227c2dcfcf0bad197cb0203010001300d06092a864886f70d01010b0500038201010042d205c5e03c8e492ad3290cfc04d9b3e42f68b67f973d5bfd6b6745f8d97e602e00d02aed4d2fe84c2e3a4286cd0b05be57b19acb259f9c95317cdad7f0327148670c2a834174a949a5de7ebfd9dbd9834d43c86aa5dafe310998cc2dc23dea388cb800dfc5fc059c691ccce33ecf4109b464bf7191655e1564306bcfc0e419ee874594f7cbd08393f33c328565338a1cd14eb8c8fe722e39d69d85aa80d11d6e8d36ff06a1f2d791f8157cb88b382eb20a93a9879301fb8ba6c01ff680f08a5e77b668ecdd7413e801ea7a7c4baa282e7a057166abe3a6a8ec6237fa1b3281b51624858826a98a963f4d55e6db1bb0ada11902207ebfdb99399c6b8fffac09710100fe00");
// export const keyvaultPrivate = fromHexString("018180fc15f0b015d7fd993b0fbc6aa95aa032b145bebe5a4819e426c632150d8eff6d4a51050ece2b6baee9e381ab89dbb3b1c4a3e6de4d3cecada3efd48616aadf2f0c1ec2f0b5c55c10ade377403023e7262552a9b3b380d42f0f70819cd4d02a317326db8974bd02c404b63b1e3fc68bc5e5f056f55c7dda5a07ca908ca2a54857028180c3a5cde7f5ccbeb9f7ac33f7e451e8e741ac18ad67671eab03ffebc1c86c68ebad510d63ff823b4cc4400251b385c7e321e30493d4dd33b8dae9e3aa4c836369928c285bb03438a02736adb7acfb58d3817450e22349811939450acb5f36fda4a0b62714a67435fe19566eed862f8c87812e465fcfd40360062302b80d80d3ad038180d2e4d813b87296ab170a372b54bb88c26024654b82e44724e47b7372c1811c98265059d6e50cd66de3267e593060299953557bd406f22d5fa783015d9ca11f4e90e93b557dfcbae50954ff7327d321927c2f07e25b0e30759e8915264bb44b584b69fac8a86d685eed42377b3b07b3efc04aadb55fc7ba610f9b9c0acc1752c904818055eabe1d9b47aa8a2845ab1cb55b7317fb33043342317aba472b5ff0dd7f1e24411c52f11307b1dae863e3a0d5616e82442359a341db52175fab4e9b3eb684359c7188579b6d9360615815b98f716174a068e00c2ecadcb2a2ffbad9cc9a6d909a32e3bea1ca6edb836cfd91b957b032e17bdcd3be84b066becb292a82638c91058180b9149556675041ba68ba8fbbeea2b940817e23fa829411641f65157789cd3585e5bf37d1db74bafe0ed351d274ed5f6ef6085710317f3ddc87dda70782c135f8eb10e18bc584c4d1b1709d8523d94893fa04c243037b5cac3359d8a556af3011b5073eece2e1bfb93e816bee82af76c6c9b28a9ab776885f13432cc31e9db768ab0101");

function buf2hex(buffer) {
  // buffer is an ArrayBuffer
  return Array.prototype.map
    .call(new Uint8Array(buffer), (x) => ("00" + x.toString(16)).slice(-2))
    .join("");
}

const PIV_DEFAULT_MANAGEMENT_KEY = Uint8Array.from([
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
]);

const PIV_SLOTS = {
  '9a': 'Authentication',
  '9c': 'Digital Signature',
  '9d': 'Key Management',
}

function fileDownload(data, filename, mime, bom) {
  var blobData = (typeof bom !== 'undefined') ? [bom, data] : [data]
  var blob = new Blob(blobData, {type: mime || 'application/octet-stream'});
  if (typeof window.navigator.msSaveBlob !== 'undefined') {
      // IE workaround for "HTML7007: One or more blob URLs were
      // revoked by closing the blob for which they were created.
      // These URLs will no longer resolve as the data backing
      // the URL has been freed."
      window.navigator.msSaveBlob(blob, filename);
  }
  else {
      var blobURL = (window.URL && window.URL.createObjectURL) ? window.URL.createObjectURL(blob) : window.webkitURL.createObjectURL(blob);
      var tempLink = document.createElement('a');
      tempLink.style.display = 'none';
      tempLink.href = blobURL;
      tempLink.setAttribute('download', filename);

      // Safari thinks _blank anchor are pop ups. We only want to set _blank
      // target if the browser does not support the HTML5 download attribute.
      // This allows you to download files in desktop safari if pop up blocking
      // is enabled.
      if (typeof tempLink.download === 'undefined') {
          tempLink.setAttribute('target', '_blank');
      }

      document.body.appendChild(tempLink);
      tempLink.click();

      // Fixes "webkit blob resource error 1"
      setTimeout(function() {
          document.body.removeChild(tempLink);
          window.URL.revokeObjectURL(blobURL);
      }, 200)
  }
}

class Certificates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // credentials: {"timestamp":"Fri, 16 Apr 2021 09:15:21 GMT","sn":"F96E36327FC5","rkCnt":4,"residentKeys":[{"rkIndex":0,"credentialId":"4b56bd87e10b89feb7af65ff5e885f535238f72c277dcd5262e25aa4825a67b1218fe2d82647e6fc646b9ce1488bbe2a879ec8b345a6e1b275628cacf36ddfc946671ee729000000","credentialUserId":"32653337303730632d613337352d346234302d393439642d363761616565623137633562","credentialUserIdCGW":"2e37070c-a375-4b40-949d-67aaeeb17c5b","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"crayonic.io"},{"rkIndex":1,"credentialId":"4b566d2923d63ac739b3d55c67beb357df68e480d4418b9968e1bb6fadd472aec3773a2f49960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d97634b000000","credentialUserId":"33616335616462312d323566352d343738312d396265632d363139303965333563663132","credentialUserIdCGW":"3ac5adb1-25f5-4781-9bec-61909e35cf12","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"},{"rkIndex":2,"credentialId":"4b5675b0db3c333f400ff923c4a65a7f532e4a0a001bdef218e8ad724fba12291d8896f849960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d9763f2000000","credentialUserId":"30626663373635302d303432612d343132642d393862302d626561323566373064623832","credentialUserIdCGW":"0bfc7650-042a-412d-98b0-bea25f70db82","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"},{"rkIndex":3,"credentialId":"4b56ac0084f5a638d1598343c1e599ecec10d6dc70da44f420acffd4debdcf90c5847b5a49960de5880e8c687434170f6476605b8fe4aeb9a28632c7995cf3ba831d97635a020000","credentialUserId":"34313865336637302d323133322d346464662d393566322d366431666530383639616236","credentialUserIdCGW":"418e3f70-2132-4ddf-95f2-6d1fe0869ab6","credentialUserName":"timo","credentialUserDisplayName":"timo","rpId":"localhost"}]}
      account: {},
      isEdited: false,
      isImporting: false,
      importingStateMessage: null
    }
  }

  async componentDidMount() {
    this.setState({isLoading: true})
    await this.fetchAccount()
    this.setState({isLoading: false})
  }

  fetchAccount = async() => {
    // if (MOCK_ACCOUNT_DATA) {
    //   this.setState({
    //     account: {
    //       attributes: {
    //         cert_pivslot_9a_crt: "-----BEGIN CERTIFICATE-----MIIGMjCCBRqgAwIBAgITEQAAAVua335u5yZdtwAAAAABWzANBgkqhkiG9w0BAQsFADBAMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNQ3JheW9uaWMgSW5jLjEZMBcGA1UEAxMQQ3JheW9uaWMgVGVzdCBDQTAeFw0yMjAyMTgwOTU1MzZaFw0yMzAyMTgwOTU1MzZaMFwxEjAQBgoJkiaJk/IsZAEZFgJpbzEYMBYGCgmSJomT8ixkARkWCGp1c3RvcGVuMRMwEQYKCZImiZPyLGQBGRYDYXBwMRcwFQYDVQQDEw5UaW1vdGVqIFN0YW5lazCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAN/2UCmjouhZv0wtfsinl6Y+NEA5wg+lDqgMey10N1gTeWx56BY7Kfy6U2xPKHvjFL7kGh6oN2EHcJp9ZALO5TbFgTfKduCojMA4Qfu7kaLGKlokH9M5xXMji42ak5NVWQuWQ6uRxcGC+sUG+V2Q63WZigOdOvffmfubCTaW2TFyfULiIiIbXrJPuBPR6cYL/MHa9lB0U3OdMmRAsPFvzkxOXI89Kuwvd46iG79J6/k3KAeIXXKAYVYMX8BNi7lGWLkbnV0eKy5OE9Y7jVUE+ODltvoL1ruEqce6CE9yxV2Y9zqTqBvZp9YnZbrkFtkLSvEFkw//RL8/XSK/vzz+1YkCAwEAAaOCAwcwggMDMA4GA1UdDwEB/wQEAwIHgDAtBgNVHREEJjAkoCIGCisGAQQBgjcUAgOgFAwSc3RhbmVrQGNyYXlvbmljLmlvMB0GA1UdDgQWBBQzePQJJ8sWNANShKShj28KQdtUujAfBgNVHSMEGDAWgBR1OiTQ/89Q1rSDa0miTJPahDUG6DCB1gYDVR0fBIHOMIHLMIHIoIHFoIHChoG/bGRhcDovLy9DTj1DcmF5b25pYyUyMFRlc3QlMjBDQSxDTj1jcmF5b25pYy1kYyxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1jcmF5b25pYyxEQz1pbz9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0P2Jhc2U/b2JqZWN0Q2xhc3M9Y1JMRGlzdHJpYnV0aW9uUG9pbnQwgcYGCCsGAQUFBwEBBIG5MIG2MIGzBggrBgEFBQcwAoaBpmxkYXA6Ly8vQ049Q3JheW9uaWMlMjBUZXN0JTIwQ0EsQ049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9Y3JheW9uaWMsREM9aW8/Y0FDZXJ0aWZpY2F0ZT9iYXNlP29iamVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRob3JpdHkwDAYDVR0TAQH/BAIwADA8BgkrBgEEAYI3FQcELzAtBiUrBgEEAYI3FQiEsIoFgfudJYaxlTaF1ZY0hcjBC3L8sAqHlLlzAgFkAgEbMCsGA1UdJQQkMCIGCisGAQQBgjcKAwwGCisGAQQBgjcUAgIGCCsGAQUFBwMCMDcGCSsGAQQBgjcVCgQqMCgwDAYKKwYBBAGCNwoDDDAMBgorBgEEAYI3FAICMAoGCCsGAQUFBwMCMC4GA1UdIAQnMCUwIwYhKwYBBAGCNxUIhLCKBYH7nSWGsZU2hdWWNIXIwQtyAYMRMA0GCSqGSIb3DQEBCwUAA4IBAQAXsgDeEsom00AIbNEQCK1NQaahyC2X92dzt/bEztIEKj+MMzNh4CbdZZ3fXOAavPRNHQc8jd4WPgnMq/n4isrx7Y+7ShqLJ3N29sG67ZXwtAzJxKvTSxYiBjRilcq9+jorFXdBp1WWCq0715YYD3yrWcsGMvLGO3vrnvEsHSDh0Ry6nhFO6bRcA20rJE2xQq9YKVfs3Hu+xt7lPVE1WsXybUHOkLaHO2dbzyj7HlM2i5fx5IKZS+Mdt2fDDfwloy2sL1CWSz+awdUpny8G9OsrSn35K6FlfKHwn/3/hXHLo1w2yIIurKRJxl0zOy9mS5wVymrT9Aizdh6TV+02TJdF-----END CERTIFICATE-----",
    //         cert_pivslot_9a_key: "-----BEGIN RSA PRIVATE KEY-----MIIEpAIBAAKCAQEA3/ZQKaOi6Fm/TC1+yKeXpj40QDnCD6UOqAx7LXQ3WBN5bHnoFjsp/LpTbE8oe+MUvuQaHqg3YQdwmn1kAs7lNsWBN8p24KiMwDhB+7uRosYqWiQf0znFcyOLjZqTk1VZC5ZDq5HFwYL6xQb5XZDrdZmKA50699+Z+5sJNpbZMXJ9QuIiIhtesk+4E9Hpxgv8wdr2UHRTc50yZECw8W/OTE5cjz0q7C93jqIbv0nr+TcoB4hdcoBhVgxfwE2LuUZYuRudXR4rLk4T1juNVQT44OW2+gvWu4Spx7oIT3LFXZj3OpOoG9mn1idluuQW2QtK8QWTD/9Evz9dIr+/PP7ViQIDAQABAoIBAAL9e+aCa8tkX1x6AwZUzn6GuBXfb4rb7k0qFX7u3UpMwENFgWbClXXtWu3IRMcaqg0+aGbeYhbmjYdSg0JSVtcbXfVLu7iWJ4LtfVwKDhC5Spl3gAFG9S7rW5KhbLT737t63x5sTNKfmA+HX07zf2FLkn8n20Cx+JFWaZIUA4RYd5LT5b03byyzJ7oghSWH8k2CzRmjo4WASLZnJXoX9x8qr0XUOUsVVJmuxK7Pl2E+lu7GcWRygzS7bfkoycmekFi+ZVxOOsgm5yFD/k5GcbkfvqMFScxQnyTA6ByIrm4yiLQYyYsLHhpTfH00drxrWstN053dGgVb5nxjb4jwmPECgYEA+5oYaZlcW1tjsrpU8G5cl30EVYffNAzpPXR43Cg85gdXffjYZh9bRCwoCsTI2PymEdsIwXLrovKHXoehBzOaV+8hpOk6CGs+oLT1aHCEpeQBLpAmFrPs/PxQGUnVQQ7HXGFiv1pno7/qLgr20zeEdVm/fVSB/oacxGGmLQh/dHUCgYEA4+CIBNyp1V3Y4Pz2mojPVaD3zWsJOfv4pgiwwNIjoW2wOy6tlVBNtd4gbrl5HnHlloGG3+g0AQYA2UJmBgMVC9ESzv6wnh2OosUT8n3hwD5KtdS1PH0VWLeBFhj8UgVkzBvA9a2mV+zQb126X8CDQtHmJQllUTRFBOvWmD08akUCgYEA5xX8MBk0c5A3vLNZSk8C6fxgG7KktejZ3KD6VLL8ZAYAPOpuzzUjnTS1PlbJZkp+uU2k+BB2Dn6VCKSnqLajnvo6u32e2JavuekasWcO3lLRJxQsd5OgKigP5XojzRIm11l22evN3t9o1gsp8os+NI5Oy2zvzHeUyuYpO5lffvUCgYAwp5tDNn5IoV4SQ9C/lCB4zwK56HWy3LrtcIOU2AlliU8Mg93qOJgvEbtpCM70kisMJKHQovqfkVGTEdtIw1W2DTDv42+Agx0WYe/tYbVW37YB9uWvgaGSLLQyLKmLNb2c2vwrOKWAtn6WmLOXiDo+hSSXmdb5LKjj8RsDrGd+5QKBgQDHPgGADYGvrkGa493j8eO4BqxFe6rQ3p8gUIoojw7wZAZC+AwAjKhPHfB9wGpKExE7ybh4yrL8Zc9g4zRntcdxZ+w2LerSRMEH8KggzNp8mtgXIoqb+qR9GkSltkLyqr0XS674d29sKA1EJGnqOGE8ADKhAqbgdjCCG2WxjWXY5w==-----END RSA PRIVATE KEY-----",
    //         cert_pivslot_9c_crt: "-----BEGIN CERTIFICATE-----MIIFvzCCBKegAwIBAgITEQAAAVwdY8V/JfLzkgAAAAABXDANBgkqhkiG9w0BAQsFADBAMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNQ3JheW9uaWMgSW5jLjEZMBcGA1UEAxMQQ3JheW9uaWMgVGVzdCBDQTAgFw0yMjAyMTgwOTU1MzhaGA8yMDUwMTIyMjExMjIxMVowXDESMBAGCgmSJomT8ixkARkWAmlvMRgwFgYKCZImiZPyLGQBGRYIanVzdG9wZW4xEzARBgoJkiaJk/IsZAEZFgNhcHAxFzAVBgNVBAMTDlRpbW90ZWogU3RhbmVrMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA01/0olKoqU4UiStaOW5PpFwr0uxkXSTxYqwCdJVBQEvO93TX32WHHsz/2ViFmqTZbMteMTuFSs3dvPlz+Ha/LAVAVV9IKLgs66uga4bfkyhtavyCSThjHKS0wnI4zJiN9h4g/CJYuRYMH/XhrF59nArBprx+gzvJgMZddeVshX3WjLIaCWkt1L/1Ih0/IiiVUIOJkpI/fg/MA1kde1nNADptBH2rsQ+tlS4XT7uX4rY4+y7v1GCmJnnpYN2ga43p3idXUXHJqy/M//fsjwEgyb9qoZzWxOzJynM1/9Idghdf24yagy3Xy1nPaKAZQ8iaWY/QmMb6aof9e/G7vkIvmwIDAQABo4ICkjCCAo4wDgYDVR0PAQH/BAQDAgQwMB0GA1UdDgQWBBRfzWn7S0m+hi3fgJcoawZ/Qq3I0jAfBgNVHSMEGDAWgBR1OiTQ/89Q1rSDa0miTJPahDUG6DCB1gYDVR0fBIHOMIHLMIHIoIHFoIHChoG/bGRhcDovLy9DTj1DcmF5b25pYyUyMFRlc3QlMjBDQSxDTj1jcmF5b25pYy1kYyxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1jcmF5b25pYyxEQz1pbz9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0P2Jhc2U/b2JqZWN0Q2xhc3M9Y1JMRGlzdHJpYnV0aW9uUG9pbnQwgcYGCCsGAQUFBwEBBIG5MIG2MIGzBggrBgEFBQcwAoaBpmxkYXA6Ly8vQ049Q3JheW9uaWMlMjBUZXN0JTIwQ0EsQ049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9Y3JheW9uaWMsREM9aW8/Y0FDZXJ0aWZpY2F0ZT9iYXNlP29iamVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRob3JpdHkwDAYDVR0TAQH/BAIwADA8BgkrBgEEAYI3FQcELzAtBiUrBgEEAYI3FQiEsIoFgfudJYaxlTaF1ZY0hcjBC3KBzmeGg/ZZAgFkAgEqMCEGA1UdJQQaMBgGCisGAQQBgjdDAQEGCisGAQQBgjcKAwQwKwYJKwYBBAGCNxUKBB4wHDAMBgorBgEEAYI3QwEBMAwGCisGAQQBgjcKAwQwDQYJKoZIhvcNAQELBQADggEBAIT+9TZMrwGa9HqJ2zK2YxmvutBTOwKDOhVEXyWVnwZgwc6MC1JbP1rc432VmBcqyo6jmIcyvYkvhjtZWSx3LHNHduxBC123ttlrKP+hBIm9829c2Js7YUAs0TNrDTTewPotbPviEpSb+h5q47SkvIM0Lgrvr+Kjq1ADqrd52F/gsK5EF4xbJsaW7PuuRJPSpP04cyAhYjkNNgLB+CEU0FQFzQB8gIVs2pWXoIL/pCasjRGSmdjEwCe/gfIII3zrHDG3n8Cd/nMwrqMOiLl39y3gck7o+HPoe2sXzyskzh2jx/qPWuTtYgKw4wIMoetOgPV3YM/wSSLIeGBKxoJBdEM=-----END CERTIFICATE-----",
    //         cert_pivslot_9c_key: "-----BEGIN RSA PRIVATE KEY-----MIIEpAIBAAKCAQEA01/0olKoqU4UiStaOW5PpFwr0uxkXSTxYqwCdJVBQEvO93TX32WHHsz/2ViFmqTZbMteMTuFSs3dvPlz+Ha/LAVAVV9IKLgs66uga4bfkyhtavyCSThjHKS0wnI4zJiN9h4g/CJYuRYMH/XhrF59nArBprx+gzvJgMZddeVshX3WjLIaCWkt1L/1Ih0/IiiVUIOJkpI/fg/MA1kde1nNADptBH2rsQ+tlS4XT7uX4rY4+y7v1GCmJnnpYN2ga43p3idXUXHJqy/M//fsjwEgyb9qoZzWxOzJynM1/9Idghdf24yagy3Xy1nPaKAZQ8iaWY/QmMb6aof9e/G7vkIvmwIDAQABAoIBAA+9RWoQ3Z49wim0oLNGE4gZlOnP0GlfJtpCFKy5sHIpMvAaMzjfRoB0o1h7zvcE72u6ePkRhYtcaPZ5XLlTnkiY24F/5s+hZzblQLzA/kTm7b7ZrpnlKPwpB4yzFKendGqvM6WSqSifC6gVlwhOwOutoV3ZC1KaURbVrn/o5XvLEFdCxhGiFjspx2rB6FbJWZvZaTIQ43Ozf6dyzqLFfQMkAdkXA9SPuLOxzJBgOaJcYKpfCyW/sPYHcdilwmFJh05HtDv3NUZa9pANo0YxLuk2MDJgOWVw3GmftJy8YkaD4Fd/mKI/JKgdQ8VXLD424YBbfrbncsI2vxnqDYbCUYECgYEA7qS39qhEr1o/rHlr4AAoxKdcLbq+CqMrZMlD7Lv04DCazb62tFEViPr1Nb9BdFUSxYgpEpw2dHkxvGKF4XtuDC5o00AIQOTK/oiGbfyHHfz838okVcc9JHC1Ha5gbv2N98BOLIQ18ymoBlzTtQIE/Z6h8A9FtfodK8L7xvePd+8CgYEA4r+GcuLQJab8XxVRhiA7Y0Xz5OS6vJaKatlt13wHxOjAGEpZLHxUGKwjC2rgA4XE3bEmTx5qeLiXgBVyBsB/GbWr6x1m83/gWg8k8IWnEVzcEYbfICtP3QlegJVg8lo/L0YNvaMSY9TLLnhWzORu2nZCsxYxBdIfNZHe503XNxUCgYBB/gTtTazovNE81bryD7k2YH2KG8jv0h1u2oVREsztriMw4BtQE9gbsCJdAY2NKPVu8G9DOTWJ0fXh/XC+YM6WycKtIFjn8Rz9dC2QfrOypEIddJ3FproOuKgtFAqQxscaivBTOSBeJNfVIphcxrkwlUx8wQxq/UZR0rKjW9QtuwKBgQCMBEKsrLxlk8DhohNuZHbnrWmW3VsmWztsEYmPd6qAgE0mcrqwCrIPbjRRt5RO+IqBbwO99YU/uJTs5dE8tB+IK7PWgphxInTSh+mCkLWCtjm6pPMhNT2iu0dc1WFBJlNACN0xTgJsHRm+P1cyPJjrU/2KAtkQxBi7hrEDWgDcpQKBgQCB5xrlXLqur71FzPBdCj5aePi1EhE8iBEF4ibJm0tKqWv0KBJQAaeCghS3OWg2/2cfo/XNj6IZTaJUyL6QtHuwSEE9/yndwd5tCVsjmbcdEfGwZpvm97J/baWyBXmvyDaAzWULthhMA6GQ5PV5YNWIMzDtEuPpSVjN0kdCl6FIOw==-----END RSA PRIVATE KEY-----",
    //         cert_pivslot_9d_crt: "-----BEGIN CERTIFICATE-----MIIFvzCCBKegAwIBAgITEQAAAVwdY8V/JfLzkgAAAAABXDANBgkqhkiG9w0BAQsFADBAMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNQ3JheW9uaWMgSW5jLjEZMBcGA1UEAxMQQ3JheW9uaWMgVGVzdCBDQTAgFw0yMjAyMTgwOTU1MzhaGA8yMDUwMTIyMjExMjIxMVowXDESMBAGCgmSJomT8ixkARkWAmlvMRgwFgYKCZImiZPyLGQBGRYIanVzdG9wZW4xEzARBgoJkiaJk/IsZAEZFgNhcHAxFzAVBgNVBAMTDlRpbW90ZWogU3RhbmVrMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA01/0olKoqU4UiStaOW5PpFwr0uxkXSTxYqwCdJVBQEvO93TX32WHHsz/2ViFmqTZbMteMTuFSs3dvPlz+Ha/LAVAVV9IKLgs66uga4bfkyhtavyCSThjHKS0wnI4zJiN9h4g/CJYuRYMH/XhrF59nArBprx+gzvJgMZddeVshX3WjLIaCWkt1L/1Ih0/IiiVUIOJkpI/fg/MA1kde1nNADptBH2rsQ+tlS4XT7uX4rY4+y7v1GCmJnnpYN2ga43p3idXUXHJqy/M//fsjwEgyb9qoZzWxOzJynM1/9Idghdf24yagy3Xy1nPaKAZQ8iaWY/QmMb6aof9e/G7vkIvmwIDAQABo4ICkjCCAo4wDgYDVR0PAQH/BAQDAgQwMB0GA1UdDgQWBBRfzWn7S0m+hi3fgJcoawZ/Qq3I0jAfBgNVHSMEGDAWgBR1OiTQ/89Q1rSDa0miTJPahDUG6DCB1gYDVR0fBIHOMIHLMIHIoIHFoIHChoG/bGRhcDovLy9DTj1DcmF5b25pYyUyMFRlc3QlMjBDQSxDTj1jcmF5b25pYy1kYyxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1jcmF5b25pYyxEQz1pbz9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0P2Jhc2U/b2JqZWN0Q2xhc3M9Y1JMRGlzdHJpYnV0aW9uUG9pbnQwgcYGCCsGAQUFBwEBBIG5MIG2MIGzBggrBgEFBQcwAoaBpmxkYXA6Ly8vQ049Q3JheW9uaWMlMjBUZXN0JTIwQ0EsQ049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9Y3JheW9uaWMsREM9aW8/Y0FDZXJ0aWZpY2F0ZT9iYXNlP29iamVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRob3JpdHkwDAYDVR0TAQH/BAIwADA8BgkrBgEEAYI3FQcELzAtBiUrBgEEAYI3FQiEsIoFgfudJYaxlTaF1ZY0hcjBC3KBzmeGg/ZZAgFkAgEqMCEGA1UdJQQaMBgGCisGAQQBgjdDAQEGCisGAQQBgjcKAwQwKwYJKwYBBAGCNxUKBB4wHDAMBgorBgEEAYI3QwEBMAwGCisGAQQBgjcKAwQwDQYJKoZIhvcNAQELBQADggEBAIT+9TZMrwGa9HqJ2zK2YxmvutBTOwKDOhVEXyWVnwZgwc6MC1JbP1rc432VmBcqyo6jmIcyvYkvhjtZWSx3LHNHduxBC123ttlrKP+hBIm9829c2Js7YUAs0TNrDTTewPotbPviEpSb+h5q47SkvIM0Lgrvr+Kjq1ADqrd52F/gsK5EF4xbJsaW7PuuRJPSpP04cyAhYjkNNgLB+CEU0FQFzQB8gIVs2pWXoIL/pCasjRGSmdjEwCe/gfIII3zrHDG3n8Cd/nMwrqMOiLl39y3gck7o+HPoe2sXzyskzh2jx/qPWuTtYgKw4wIMoetOgPV3YM/wSSLIeGBKxoJBdEM=-----END CERTIFICATE-----",
    //         cert_pivslot_9d_key: "-----BEGIN RSA PRIVATE KEY-----MIIEpAIBAAKCAQEA01/0olKoqU4UiStaOW5PpFwr0uxkXSTxYqwCdJVBQEvO93TX32WHHsz/2ViFmqTZbMteMTuFSs3dvPlz+Ha/LAVAVV9IKLgs66uga4bfkyhtavyCSThjHKS0wnI4zJiN9h4g/CJYuRYMH/XhrF59nArBprx+gzvJgMZddeVshX3WjLIaCWkt1L/1Ih0/IiiVUIOJkpI/fg/MA1kde1nNADptBH2rsQ+tlS4XT7uX4rY4+y7v1GCmJnnpYN2ga43p3idXUXHJqy/M//fsjwEgyb9qoZzWxOzJynM1/9Idghdf24yagy3Xy1nPaKAZQ8iaWY/QmMb6aof9e/G7vkIvmwIDAQABAoIBAA+9RWoQ3Z49wim0oLNGE4gZlOnP0GlfJtpCFKy5sHIpMvAaMzjfRoB0o1h7zvcE72u6ePkRhYtcaPZ5XLlTnkiY24F/5s+hZzblQLzA/kTm7b7ZrpnlKPwpB4yzFKendGqvM6WSqSifC6gVlwhOwOutoV3ZC1KaURbVrn/o5XvLEFdCxhGiFjspx2rB6FbJWZvZaTIQ43Ozf6dyzqLFfQMkAdkXA9SPuLOxzJBgOaJcYKpfCyW/sPYHcdilwmFJh05HtDv3NUZa9pANo0YxLuk2MDJgOWVw3GmftJy8YkaD4Fd/mKI/JKgdQ8VXLD424YBbfrbncsI2vxnqDYbCUYECgYEA7qS39qhEr1o/rHlr4AAoxKdcLbq+CqMrZMlD7Lv04DCazb62tFEViPr1Nb9BdFUSxYgpEpw2dHkxvGKF4XtuDC5o00AIQOTK/oiGbfyHHfz838okVcc9JHC1Ha5gbv2N98BOLIQ18ymoBlzTtQIE/Z6h8A9FtfodK8L7xvePd+8CgYEA4r+GcuLQJab8XxVRhiA7Y0Xz5OS6vJaKatlt13wHxOjAGEpZLHxUGKwjC2rgA4XE3bEmTx5qeLiXgBVyBsB/GbWr6x1m83/gWg8k8IWnEVzcEYbfICtP3QlegJVg8lo/L0YNvaMSY9TLLnhWzORu2nZCsxYxBdIfNZHe503XNxUCgYBB/gTtTazovNE81bryD7k2YH2KG8jv0h1u2oVREsztriMw4BtQE9gbsCJdAY2NKPVu8G9DOTWJ0fXh/XC+YM6WycKtIFjn8Rz9dC2QfrOypEIddJ3FproOuKgtFAqQxscaivBTOSBeJNfVIphcxrkwlUx8wQxq/UZR0rKjW9QtuwKBgQCMBEKsrLxlk8DhohNuZHbnrWmW3VsmWztsEYmPd6qAgE0mcrqwCrIPbjRRt5RO+IqBbwO99YU/uJTs5dE8tB+IK7PWgphxInTSh+mCkLWCtjm6pPMhNT2iu0dc1WFBJlNACN0xTgJsHRm+P1cyPJjrU/2KAtkQxBi7hrEDWgDcpQKBgQCB5xrlXLqur71FzPBdCj5aePi1EhE8iBEF4ibJm0tKqWv0KBJQAaeCghS3OWg2/2cfo/XNj6IZTaJUyL6QtHuwSEE9/yndwd5tCVsjmbcdEfGwZpvm97J/baWyBXmvyDaAzWULthhMA6GQ5PV5YNWIMzDtEuPpSVjN0kdCl6FIOw==-----END RSA PRIVATE KEY-----",
    //       }
    //     },
    //     isEdited: false
    //   })
    //   return
    // }
    try {
      const result = await fetch(
        keycloak.authServerUrl + '/realms/' + keycloak.realm + '/account/', 
        {
          method: 'get',
          headers: {
            "Authorization": `Bearer ${keycloak.token}`,
            "Content-Type": "application/json"
          }
        }
      )
      const account = await result.json();
      console.log(account);
      this.setState({account, isEdited: false})
      return account;
    } catch (error) {
      console.error(error)
      alert("Failed loading account")
    }
  }

  importCertificate = async (slot, private_key, public_cert) => {
    const slot_int = parseInt(slot, 16);

    const private_key_bytes = b64toUint8Array(private_key.replace(/(-----(BEGIN|END) RSA PRIVATE KEY[^-]*-----|\s)/g,''));
    const public_cert_bytes = b64toUint8Array(public_cert.replace(/(-----(BEGIN|END) CERTIFICATE[^-]*-----|\s)/g,''));

    console.log('private_key_bytes', buf2hex(private_key_bytes))
    console.log('public_cert_bytes', buf2hex(public_cert_bytes))

    

    this.setState({isImporting: true});
    try {
      // console.log("keyvaultCert, kyevaultPrivate", keyvaultCert, keyvaultPrivate);
      this.setState({importingStateMessage: "Getting card challenge..."})
      const witness_response = await CrayonicPivManager.getWitness();
      console.log("witness_response", witness_response)
      if (witness_response.ret_code !== 0x9000) {
        throw ("Failed getting card challenge with error " + witness_response.ret_code)
      }
      const witness_challenge = witness_response.tlv.w.slice(4); // TODO parse internal tlv?
      console.log("witness_challenge", buf2hex(witness_challenge));
      const witness_challenge_decrypted = str2ab(
        decrypt3DES(witness_challenge, PIV_DEFAULT_MANAGEMENT_KEY)
      );
      // http://tripledes.online-domain-tools.com/
      console.log(
        "witness_challenge_decrypted",
        buf2hex(witness_challenge_decrypted)
      );

      // secure random challenge
      const myChallenge = crypto.randomBytes(8);

      this.setState({importingStateMessage: "Answering card challenge..."})
      const mutual_response = await CrayonicPivManager.mutualResponse(
        witness_challenge_decrypted,
        myChallenge
      );

      console.log("mutual_response", mutual_response);

      const myChallenge_decrypted = str2ab(
        encrypt3DES(myChallenge, PIV_DEFAULT_MANAGEMENT_KEY)
      );

      // check if same
      // console.log(
      //   "myChallenge_decrypted",
      //   buf2hex(myChallenge_decrypted),
      //   buf2hex(mutual_response.tlv.r)
      // );
      if (buf2hex(myChallenge_decrypted) !== buf2hex(mutual_response.tlv.r)) {
        throw "Card challenge invalid response";
      }

      // TODO this on KV
      let piv_slot_tag = null;
      switch(slot_int) {
        case 0x9a: piv_slot_tag = 0x05; break;
        case 0x9c: piv_slot_tag = 0x0A; break;
        case 0x9d: piv_slot_tag = 0x0B; break;
        case 0x9e: piv_slot_tag = 0x01; break;
      }
      const keyvaultCert = Uint8Array.from([
        0x5c, 0x03, 0x5f, 0xc1, piv_slot_tag, 0x53, 0x82, 
        (public_cert_bytes.length + 9) / 256, (public_cert_bytes.length + 9) % 256,
        0x70, 0x82, 
        (public_cert_bytes.length) / 256, (public_cert_bytes.length) % 256,
        ...public_cert_bytes,
        0x71, 0x01, 0x00, 0xfe, 0x00,
      ])
      // END TODO

      var privateKey = forge.pki.privateKeyFromPem(
        private_key
      );
      console.log(privateKey);
      // console.log("n", buf2hex(Uint8Array.from(privateKey.n.toByteArray())));
      // console.log("d", buf2hex(Uint8Array.from(privateKey.d.toByteArray())));
      // console.log("p", buf2hex(Uint8Array.from(privateKey.p.toByteArray())));
      // console.log("q", buf2hex(Uint8Array.from(privateKey.q.toByteArray())));
      // console.log("dP", buf2hex(Uint8Array.from(privateKey.dP.toByteArray())));
      // console.log("dQ", buf2hex(Uint8Array.from(privateKey.dQ.toByteArray())));
      // console.log("qInv", buf2hex(Uint8Array.from(privateKey.qInv.toByteArray())));
    
      const p = Uint8Array.from(privateKey.p.toByteArray().slice(-128));
      const q = Uint8Array.from(privateKey.q.toByteArray().slice(-128));
      const dP = Uint8Array.from(privateKey.dP.toByteArray().slice(-128));
      const dQ = Uint8Array.from(privateKey.dQ.toByteArray().slice(-128));
      const qInv = Uint8Array.from(privateKey.qInv.toByteArray().slice(-128));

      // console.log("p", buf2hex(p));
      // console.log("q", buf2hex(q));
      // console.log("dP", buf2hex(dP));
      // console.log("dQ", buf2hex(dQ));
      // console.log("qInv", buf2hex(qInv));
    
      const private_key_message = Uint8Array.from([
        0x01, 0x81, p.length % 256, ...p,
        0x02, 0x81, q.length % 256, ...q,
        0x03, 0x81, dP.length % 256, ...dP,
        0x04, 0x81, dQ.length % 256, ...dQ,
        0x05, 0x81, qInv.length % 256, ...qInv,
        ...[0xab, 0x01, 0x01]
      ]);
    
      console.log(buf2hex(private_key_message));

      this.setState({importingStateMessage: "Importing certificate..."})
      const result = await CrayonicPivManager.importCertificate(
        slot_int,
        private_key_message,
        keyvaultCert
      );
      console.log(result);
      if (buf2hex(result.ret_code) == "9000") {
        alert("Import sucessful")
      } else (
        alert("Import failed with error code " + buf2hex(result.ret_code))
      )
    } catch (e) {
      alert("Import failed: " + e)
    }
    this.setState({isImporting: false});
    
  };

  render() {
    const { classes } = this.props;
    const { credentials } = this.state;
    // const residentKeys = credentials.residentKeys || []
    const dense = false;

    console.log('keycloak', keycloak)

    

    return(
    <div className={classes.root}>
      <Backdrop className={classes.backdrop} open={this.state.isImporting}>
        <CircularProgress color="inherit" />
        <span style={{padding: "1em"}}>{this.state.importingStateMessage}</span>
      </Backdrop>
      <Typography variant="h6" className={classes.title}>
        PIV Certificates
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6} style={{justifyContent: 'space-between'}}>
          <div className={classes.demo}>
            <List dense={dense} subheader={<ListSubheader></ListSubheader>}>
              {Object.keys(PIV_SLOTS).map((piv_slot) => {
                const enrolled = this.state.account.attributes && this.state.account.attributes[`cert_pivslot_${piv_slot}_crt`] !== undefined;
                return (
                  <ListItem key={piv_slot}>
                    {/* <pre>{Certificate.fromPEM(this.state.account.attributes.cert_SIGNATURE_key)}</pre> */}
                    <ListItemAvatar>
                      <Avatar>
                        <VpnKey />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={`${PIV_SLOTS[piv_slot]} (${piv_slot})`}
                      secondary={enrolled ? `Enrolled` : 'N/A'}
                      // secondary={`Enrolled, SCEP ${scep_type}`}
                    />
                    {enrolled ? <ListItemSecondaryAction>
                      <IconButton label="dadsa" edge="end" aria-label="download" onClick={() => {
                        const blobkey = new Blob([this.state.account.attributes[`cert_pivslot_${piv_slot}_key`]], {type : 'application/pkcs8'});
                        fileDownload(blobkey, `${PIV_SLOTS[piv_slot]} (${piv_slot}).key`)
                        const blobcrt = new Blob([this.state.account.attributes[`cert_pivslot_${piv_slot}_crt`]], {type : 'application/x-x509-user-cert'});
                        fileDownload(blobcrt, `${PIV_SLOTS[piv_slot]} (${piv_slot}).crt`)
                      }}>
                        <GetApp />
                      </IconButton>
                      <IconButton edge="end" aria-label="download" onClick={() => {
                        this.importCertificate(piv_slot, this.state.account.attributes[`cert_pivslot_${piv_slot}_key`], this.state.account.attributes[`cert_pivslot_${piv_slot}_crt`]);
                      }}>
                        <CloudDownload />
                      </IconButton>
                      {/* <IconButton edge="end" aria-label="delete">
                        <Refresh />
                      </IconButton>
                        <GetApp />
                      </IconButton>
                      <IconButton edge="end" aria-label="delete">
                        <Sync />
                      </IconButton> */}
                    </ListItemSecondaryAction> : null}
                  </ListItem>
                )
              })}
            </List>
          </div>
        </Grid>
      </Grid>
      {/* <pre>
        {JSON.stringify(this.state.credentials, null, 4)}
      </pre> */}
    </div>)
  }
}

const CertificatesWith = withStyles(styles)(Certificates);
export {CertificatesWith as Certificates};
export default withStyles(styles)(Certificates);
