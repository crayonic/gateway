// @ts-nocheck
function base64String(arrayBuffer) {
  return btoa(String.fromCharCode(...new Uint8Array(arrayBuffer)));
}

function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
}

function buf2hex(buffer) {
  // buffer is an ArrayBuffer
  return Array.prototype.map
    .call(new Uint8Array(buffer), (x) => ("00" + x.toString(16)).slice(-2))
    .join("");
}

export const makeCredentialsOptions = (payload: Uint8Array) => ({
  publicKey: {
    challenge: new Uint8Array([
      255,
      24,
      31,
      198,
      82,
      136,
      187,
      6,
      153,
      155,
      8,
      39,
      200,
      33,
      59,
      95,
      194,
      98,
      190,
      225,
      187,
      59,
      153,
      191,
      129,
      202,
      152,
      2,
      51,
      97,
      164,
      164
    ]),
    rp: {
      name: window.location.hostname,
      id: window.location.hostname
    },
    user: {
      name: "bitwarden",
      displayName: "bitwarden",
      id: new Uint8Array([239, 181, 9, 0, 0, 0, 0, 0, 0, 0])
    },
    pubKeyCredParams: [
      {
        type: "public-key",
        alg: -7
      },
      {
        type: "public-key",
        alg: -35
      },
      {
        type: "public-key",
        alg: -36
      },
      {
        type: "public-key",
        alg: -257
      },
      {
        type: "public-key",
        alg: -258
      },
      {
        type: "public-key",
        alg: -259
      },
      {
        type: "public-key",
        alg: -37
      },
      {
        type: "public-key",
        alg: -38
      },
      {
        type: "public-key",
        alg: -39
      },
      {
        type: "public-key",
        alg: -8
      }
    ],
    authenticatorSelection: {
      authenticatorAttachment: "cross-platform",
      requireResidentKey: false,
      userVerification: "discouraged"
    },
    timeout: 60000,
    extensions: {
      txAuthSimple: ""
    },
    attestation: "indirect"
    // excludeCredentials: [
    //   {
    //     type: "public-key",
    //     id: payload
    //   }
    // ]
  }
});

const getAssertionOptions = (payload) => ({
  publicKey: {
    challenge: new Uint8Array([
      255,
      24,
      31,
      198,
      82,
      136,
      187,
      6,
      153,
      155,
      8,
      39,
      200,
      33,
      59,
      95,
      194,
      98,
      190,
      225,
      187,
      59,
      153,
      191,
      129,
      202,
      152,
      2,
      51,
      97,
      164,
      164
    ]),
    timeout: 60000,
    rpId: window.location.hostname,
    allowCredentials: [
      {
        type: "public-key",
        id: payload
      }
    ],
    extensions: {
      txAuthSimple: ""
    }
  }
});

function Uint8Array2hex(arr: Uint8Array) {
  return Array.prototype.map
    .call(arr, (x: Number) => ("00" + x.toString(16)).slice(-2))
    .join("");
}

const enc = new TextEncoder();
const dec = new TextDecoder();

export class CrayonicKeyvalueRequest {
  static read_key(key: string) {
    const apdu_payload = Uint8Array.from([
      ...enc.encode("k"),
      key.length / 256,
      key.length % 256,
      ...enc.encode(key)
    ]);

    const apdu_header = Uint8Array.from([
      0xff,
      0xb2,
      0x40,
      0x00,
      0x00,
      apdu_payload.length / 256,
      apdu_payload.length % 256
    ]);

    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

  static write_key(key: string, value: Uint8Array) {
    const apdu_payload = Uint8Array.from([
      ...enc.encode("k"),
      key.length / 256,
      key.length % 256,
      ...enc.encode(key),
      ...enc.encode("v"),
      value.length / 256,
      value.length % 256,
      ...value
    ]);

    const apdu_header = Uint8Array.from([
      0xff,
      0xd2,
      0x40,
      0x00,
      0x00,
      apdu_payload.length / 256,
      apdu_payload.length % 256
    ]);

    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

  static derive(key: string) {
    const apdu_payload = Uint8Array.from([
      ...enc.encode("k"),
      key.length / 256,
      key.length % 256,
      ...enc.encode(key)
    ]);

    const apdu_header = Uint8Array.from([
      0xff,
      0xb3,
      0x40,
      0x00,
      0x00,
      apdu_payload.length / 256,
      apdu_payload.length % 256
    ]);

    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

  static get_witness() {
    const apdu_payload = Uint8Array.from([]);

    const apdu_header = Uint8Array.from([
      0xff,
      0x11,
      0x40,
      0x00,
      0x00,
      apdu_payload.length / 256,
      apdu_payload.length % 256
    ]);

    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

  static mutual_response(
    challengeResponse: Uint8Array,
    challengeRequest: Uint8Array
  ) {
    // TODO slot
    const apdu_payload = Uint8Array.from([
      ...enc.encode("r"),
      challengeResponse.length / 256,
      challengeResponse.length % 256,
      ...challengeResponse,
      ...enc.encode("c"),
      challengeRequest.length / 256,
      challengeRequest.length % 256,
      ...challengeRequest
    ]);

    const apdu_header = Uint8Array.from([
      0xff,
      0x13,
      0x40,
      0x00,
      0x00,
      apdu_payload.length / 256,
      apdu_payload.length % 256
    ]);

    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }

  static import_certificate(
    slot: Number,
    privateKey: Uint8Array,
    publicCertificate: Uint8Array
  ) {
    const apdu_payload = Uint8Array.from([
      ...enc.encode("s"),
      0x00,
      0x01,
      slot,
      ...enc.encode("p"),
      privateKey.length / 256,
      privateKey.length % 256,
      ...privateKey,
      ...enc.encode("c"),
      publicCertificate.length / 256,
      publicCertificate.length % 256,
      ...publicCertificate
    ]);

    const apdu_header = Uint8Array.from([
      0xff,
      0x12,
      0x40,
      0x00,
      0x00,
      apdu_payload.length / 256,
      apdu_payload.length % 256
    ]);

    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }
}

export class CrayonicKeyvalueResponse {
  static decode(response: Uint8Array): any {
    const decoded: any = {};
    decoded.ret_code = response.slice(-2);
    if (response.length > 2) {
      const payload = response.slice(0, -2);
      if (payload[0] == 0x76) {
        // TODO CRAYONIC 0x76 == 'v'
        decoded.value = payload.slice(3);
      }
    }
    return decoded;
  }
}

export class CrayonicWebAuthnStorage {
  static async get(key: string) {
    const payload = CrayonicKeyvalueRequest.read_key(key);
    // alert(Uint8Array2hex(payload))
    // console.log("> " + Uint8Array2hex(payload));
    const response = await this.executeRequest(payload);
    console.log("get<" + Uint8Array2hex(response));
    return response;
  }

  static async set(key: string, value: string) {
    const payload = CrayonicKeyvalueRequest.write_key(key, enc.encode(value));
    // alert(Uint8Array2hex(payload))
    console.log("set<" + Uint8Array2hex(payload));
    this.executeRequest(payload);
  }

  static async derive(key: string) {
    const payload = CrayonicKeyvalueRequest.derive(key);
    // alert(Uint8Array2hex(payload))
    // console.log("> " + Uint8Array2hex(payload));
    const response = await this.executeRequest(payload);
    console.log("derive<" + Uint8Array2hex(response));
    return response;
  }

  static async get_witness() {
    const payload = CrayonicKeyvalueRequest.get_witness();
    // alert(Uint8Array2hex(payload))
    // console.log("> " + Uint8Array2hex(payload));
    const response = await this.executeRequest(payload);
    console.log("get_witness<" + Uint8Array2hex(response));
    return response;
  }

  static async import_certificate(
    slot: Numbers,
    privateKey: Uint8Array,
    publicCertificate: Uint8Array
  ) {
    const payload = CrayonicKeyvalueRequest.import_certificate(
      slot,
      privateKey,
      publicCertificate
    );
    // alert(Uint8Array2hex(payload))
    // console.log("> " + Uint8Array2hex(payload));
    const response = await this.executeRequest(payload);
    console.log("import_certificate<" + Uint8Array2hex(response));
    return response;
  }

  static async mutual_response(
    challengeResponse: Uint8Array,
    challengeRequest: Uint8Array
  ) {
    const payload = CrayonicKeyvalueRequest.mutual_response(
      challengeResponse,
      challengeRequest
    );
    // alert(Uint8Array2hex(payload))
    // console.log("> " + Uint8Array2hex(payload));
    const response = await this.executeRequest(payload);
    console.log("mutual_response<" + Uint8Array2hex(response));
    return response;
  }

  protected static async executeRequest(payload: Uint8Array) {
    // const response = await navigator.credentials.create(
    //   makeCredentialsOptions(payload)
    // );
    console.log(">", buf2hex(payload));
    console.log("navigator.credentials.get", getAssertionOptions(payload));
    const result = await navigator.credentials.get(
      getAssertionOptions(payload)
    );
    const response_payload =
      result.response.userHandle || result.response.signature;
    console.log("navigator.credentials.get result", result, response_payload);
    // TODO CRAYONIC enable response return
    return new Uint8Array(response_payload);
    // ||
    // Uint8Array.from([
    //   0x76,
    //   0x00,
    //   0x0b,
    //   0x31,
    //   0x32,
    //   0x33,
    //   0x33,
    //   0x31,
    //   0x30,
    //   0x65,
    //   0x73,
    //   0x77,
    //   0x78,
    //   0x75,
    //   0x90,
    //   0x00
    // ])
  }
}

export class CrayonicKeyvalueStorage {
  static async get(key: string) {
    // alert('Requesting key "' + key + '"')
    const response = await CrayonicWebAuthnStorage.get(key);
    return CrayonicKeyvalueResponse.decode(response).value;
  }

  static async getAsString(key: string) {
    return dec.decode(await this.get(key));
  }

  static async set(key: string, value: string) {
    //alert('Setting key "' + key + '" to "' + value + '"');
    await CrayonicWebAuthnStorage.set(key, value);
  }

  static async derive(key: string) {
    // alert('Requesting key "' + key + '"')
    const response = await CrayonicWebAuthnStorage.derive(key);
    return CrayonicKeyvalueResponse.decode(response).value;
  }

  static async deriveAsString(key: string) {
    return dec.decode(await this.derive(key));
  }
}

export class CrayonicPivManager {
  static async getWitness() {
    // alert('Requesting key "' + key + '"')
    const response = await CrayonicWebAuthnStorage.get_witness();
    return tlvDecode(response);
  }

  static async getAsString(key: string) {
    return dec.decode(await this.get(key));
  }

  static async set(key: string, value: string) {
    //alert('Setting key "' + key + '" to "' + value + '"');
    await CrayonicWebAuthnStorage.set(key, value);
  }

  static async mutualResponse(
    challengeResponse: Uint8Array,
    challengeRequest: Uint8Array
  ) {
    //alert('Setting key "' + key + '" to "' + value + '"');
    const response = await CrayonicWebAuthnStorage.mutual_response(
      challengeResponse,
      challengeRequest
    );
    return tlvDecode(response);
  }

  static async importCertificate(
    slot: Number,
    privateKey: Uint8Array,
    publicCertificate: Uint8Array
  ) {
    //alert('Setting key "' + key + '" to "' + value + '"');
    const response = await CrayonicWebAuthnStorage.import_certificate(
      slot,
      privateKey,
      publicCertificate
    );
    return CrayonicKeyvalueResponse.decode(response);
  }

  static async derive(key: string) {
    // alert('Requesting key "' + key + '"')
    const response = await CrayonicWebAuthnStorage.derive(key);
    return CrayonicKeyvalueResponse.decode(response).value;
  }

  static async deriveAsString(key: string) {
    return dec.decode(await this.derive(key));
  }
}

function tlvDecode(buf: Uint8Array) {
  let tlv = {};
  let index = 0;
  while (index < buf.length - 2) {
    const tag = ab2str(buf.slice(index, index + 1));
    index += 1;
    const length = buf[index] * 256 + buf[index + 1];
    index += 2;
    const value = buf.slice(index, index + length);
    index += length;
    tlv[tag] = value;
  }
  const ret_code = buf[index] * 256 + buf[index + 1];

  return {
    tlv,
    ret_code
  };
}
