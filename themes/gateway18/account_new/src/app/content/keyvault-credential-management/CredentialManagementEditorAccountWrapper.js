import React, { Component } from "react";
import { getRealm } from './utils';
import { CredentialManagementEditor } from './CredentialManagementEditor';

import { KeycloakContext } from '../../keycloak-service/KeycloakContext';
import { KeycloakService } from '../../keycloak-service/keycloak.service';

class CredentialManagementEditorAccountWrapper extends Component {
  
render() {
    return (
      <KeycloakContext.Consumer>
          { (keycloak) => (
            <CredentialManagementEditor 
              keycloak={keycloak}
              apiEndpoint='/cgw/'
              // apiEndpoint='http://localhost:8443/cgw/'
              // apiEndpoint='https://crayonic.io/cgw/'
              realm={getRealm()}
            />
          )}
      </KeycloakContext.Consumer>
    )
  }
}

export { CredentialManagementEditorAccountWrapper };
