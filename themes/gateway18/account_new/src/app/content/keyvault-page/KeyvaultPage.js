//
// Managing Keyvault device
// CHM/Crayonic, 4/2021
//

import * as React from 'react';

import {
  DataList,
  DataListItem,
  DataListItemRow,
  DataListCell,
  DataListToggle,
  DataListContent,
  DataListItemCells,
  Grid,
  GridItem,
  Button,
} from '@patternfly/react-core';

import { TextField } from "@material-ui/core";
import IframeComponent from './IframeComponent';

export class KeyvaultPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: 'React',
      backupResult: null,
      residentKeysList: null 
    }
  }
 
  updateContent() {
    this.setState({
      backupResult: localStorage.getItem('backupResult'),
      residentKeysList: localStorage.getItem('residentKeysList')
    });
  }

  // onClick: () => { callBackup(this.setState) }

  render() {
  

    const divStyle = {
      display: 'inline-block',
      margin: '0 auto'
    };

    const keyvaultFrame = React.createElement(IframeComponent, {
      src: "https://crayonic.io/keyvault.html",
      style: {divStyle},
      height: "1000px !important",
      width: "100%"
      }, 'Manage your Keyvault device');


    const container = React.createElement('div', {}, [
      keyvaultFrame
    ]);

    return container;

  }
}

      // title,
      // p, 
      // desc, 
      // p, p, 
      // backupButton, 
      // nbsp, 
      // restoreButton,
      // nbsp,
      // residentkeysButton,
      // nbsp,
      // refreshButton,      
      // p,
      // operationResult,
      // p,
      // operationResultRK,
      // p, p,

      // const title = React.createElement('h1', {
      //   className: "pf-c-title pf-m-3xl"      
      //   }, 'Keyvault management');
  
      // const desc = React.createElement('p', {}, 'Here you can backup your KeyVault as well as restore its content in case of lost device.');
      // const p = React.createElement('p', {}, '');
        
  

      // const nbsp = React.createElement('strong', {}, '');

    // const backupButton = React.createElement(Button, {
    //   id: 'cgw-button-backup',
    //   type: "button",
    //   onClick: () => { callBackup(); }
    // }, "Backup Keyvault");

    // const restoreButton = React.createElement(Button, {
    //   id: 'cgw-button-restore',
    //   type: "button",
    //   onClick: () => { callRestore(); }
    // }, "Restore Keyvault");
  
    // const residentkeysButton = React.createElement(Button, {
    //   id: 'cgw-button-residentkeys',
    //   type: "button",
    //   onClick: () => { callShowResidentKeys(); }
    // }, "Show Resident Keys");

    // const refreshButton = React.createElement(Button, {
    //   id: 'cgw-button-refresh',
    //   type: "button",
    //   onClick: () => { this.updateContent() }
    // }, "Refresh");
    

    // const keyvaultManagement = React.createElement('iframe', {
    //   src: 'https://crayonic.io/keyvault.html',
    //   title: 'Crayonic Keyvault Management',
    //   width: '100%',
    //   height: '100%'
    // }, 'Keyvault Management');

    // const operationResult = React.createElement(TextField, {
    //   id: 'backupRestoreResult',
    //   // className: "col-sm-10 col-md-10",
    //   multiline: true,
    //   fullWidth: true,
    //   variant: "outlined",
    //   placeholder: "Backup/Restore log",
    //   rows: '5'
    // }, this.state.backupResult);

    // const operationResultRK = React.createElement('div', {
    //   id: 'residentKeysList'
    // }, this.state.residentKeysList);
    
