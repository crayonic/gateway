//
// index.js
//
// Crayonic Gateway (CGW) API Server - document acronym: [CGW-API]
// API endpoint: /cgw/
//
// IMPORTANT!!! remove debug api endpoints in PRODUCTION environment + make sure your server hosting DB is NOT accepting connections from outside
//

// Marek Chorvat
// v0.1.x: 2020-10,11,12 - mongodb
// v0.2.0: 2021/3 - postgresSQL + policies
// v0.2.1: 2021/3 - usernameless login - userhandle/username sync
// v0.2.2: 2021/4 - credentials management API for CGW, security improvements
// v0.2.5: 2021/7 - KV can now be restored from selected backup instead from the latest one (GET /cgw/backup/meta)
// v0.2.6 - 0.2.8: 2021-10 - Docker and Kubernetes improvements, moving to gateway-localhost gitlab repository

// TODO: evaluate option to include https://github.com/github/webauthn-json library? No client-side processing needed

// const VERSION = '0.2.4';  // 2021-04-06
// const VERSION = '0.2.5';  // 2021-07-13
// const VERSION = '0.2.6';  // 2021-10-11
// const VERSION = '0.2.7';  // 2021-10-18
// const VERSION = '0.2.8';  // 2021-10-26, 27
// const VERSION = '0.2.9';  // 2021-11-02 new rpid (incompatible with previous releases), multiple http/https configs
// const VERSION = '0.2.10';  // 2021-11-04 call authentication (ENV CGWAPIKEY needs to be set first), '/info' endpoint
// const VERSION = '0.2.11';  // 2021-11-22 k8s psql fixes (0.0.0.0 domain name resolver in k8s)
// const VERSION = '0.2.13';  // 2021-11-24,25,26,28 keycloak protection
const VERSION = '0.2.14';  // 2022-07-22 k8s app config update, new env params: KEYCLOAK_HOST_ADDRESS, CGW_API_ROOT


// pass via ENV variable
const NODE_ENV = process.env.NODE_ENV;
const USE_DYNAMIC_RPID = process.env.NODE_USE_DYNAMIC_RPID;
const KEYCLOAK_HOST = process.env.KEYCLOAK_HOST || 'crayonic.io';
const KEYCLOAK_HOST_ADDRESS = process.env.KEYCLOAK_HOST_ADDRESS || 'https://crayonic.io/auth';

const HOST = '0.0.0.0'; // localhost
// const callPrefix = '/cgw';  // API endpoint (e.g. crayonic.io/cgw/)
// const CGW_API_ROOT = process.env.CGW_API_ROOT || '/cgw';
const CGW_API_ROOT = process.env.CGW_API_ROOT;

let APP_PATH = '';  // used and changed by internal logger

// localhost: port = 443; docker/k8/prod: port = 3000
// const PORT = (NODE_ENV === 'prod') ? 3000 : 443;
let PORT;

const http = require('http');
const https = require('https');
const fs = require('fs');
// const util = require('util');
// const { v4: uuidv4 } = require('uuid');
const express = require('express');
const session = require('express-session');
const helmet = require("helmet");
const os = require('os');
const Keycloak = require('keycloak-connect');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const axios = require('axios').default;

// using v2 (v3 is an ESM-only module)
//npm install node-fetch@2 --save
// import fetch from 'node-fetch';
const fetch = require('node-fetch');

const core = require('./core.js');  // internal core functions (logging, etc.)
// const { Buffer } = require('buffer');

//
// Express setup
//
const app = express();
app.set( 'trust proxy', true );

app.use(helmet());

// app.use(bodyParser.json());
app.use(cors());

// for POST forms on the web
app.use(express.urlencoded({
  extended: true
}))

app.use(express.json());

// app.use(session({
//   secret: 'crayonicFTW',
//   cookie: { maxAge: 1200000 },
//   resave: true,
//   saveUninitialized: true
// }));

//
// Keycloak middleware
// securing all traffic to /cgw/
// https://www.keycloak.org/docs/latest/securing_apps/#_nodejs_adapter

// app.use( keycloak.middleware() );
// app.all(CGW_API_ROOT, function(req, res, next) {
//   keycloak.protect();
//   next();
// }); 

// var memoryStore = new session.MemoryStore();
// var keycloak = new Keycloak({ store: memoryStore });

// Create a session-store to be used by both the express-session
// middleware and the keycloak middleware.

var memoryStore = new session.MemoryStore();

app.use(session({
  secret: 'crayonicFTW',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));

// Provide the session store to the Keycloak so that sessions
// can be invalidated from the Keycloak console callback.
//
// Additional configuration is read from keycloak.json file
// installed from the Keycloak web console.

// "realm": "yourRealm",
// "auth-server-url": "http://localhost:8080/auth",
// "ssl-required": "external",
// "resource": "yourRealm/keep it default",
// "public-client": true,
// "confidential-port": 0,
// "url": 'http://localhost:8080/auth',
// "clientId": 'yourClientId',
// "enable-cors": true

// const kcConfig = {
//   clientId: 'account',
//   bearerOnly: true,
//   serverUrl: `${process.env.KEYCLOAK_HOST_ADDRESS}`,
//   realm: 'gateway'
//   // realmPublicKey: 'MIIBIjANB...'
// };


// If the client Access Type is bearer-only instead of credentials you need to provide realmPublicKey. Realm Public Key can be copied from Realm Settings > Keys > Public Key.
// or here: https://test.crayonic.io/auth/realms/gateway/ (public key)
// realmPublicKey: 'MIIBIjANBgkqhkiG9w0BAQEFAAO...'

// const kcConfig = {
//   clientId: 'gateway-api',
//   bearerOnly: true,
//   serverUrl: `${process.env.KEYCLOAK_HOST_ADDRESS}`,
//   realm: 'gateway',
//   credentials: {
//       secret: 'd9d71ab9-283f-4be7-bd8a-3fda8f0583c8'
//   }
// };


// const kcConfig = {  
//   "realm" : "gateway",
//   "realm-public-key" : "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTbkO5rSSW/iWrKboNSJj4uXtTFRtCr/ts2twu4wOpVChY/e6qwZ5K72IsGzuU5WD/g7Pcsr+eQINf9NgVQeV2r5fGuqnHN7ousije6iminBmuujbpeD+29fuczTUMiyxAH+RGEVA3qbC2LJ6Erl4LtKGY/Sn48vQDSxozIo8yG65MWn7GudPmGCXzVvJPfv/DGfCwfw4ySKcNWkMgAKDUees8YnpljDd+q0ZBaWBqjctk8mWk/FV0Pl8ExuSqG+3jM/Zr7hYNSmcjwqBfoSHGExs41i5ARUQSW9REfAx428VfdYL/Q0dPJr8lT8hjV523ffcptiaNX24bZabssu/wIDAQAB",
//   "auth-server-url" : "https://test.crayonic.io/auth",
//   "ssl-required" : "external",
//   "resource" : "gateway-api",
//   "public-client" : true
// }

let timeoutObj;

let keycloak;
let kcConfig;
let realmToProtect = 'gateway';  // default realm to protect
let realm ='gateway';
let username;

axios.get(`${KEYCLOAK_HOST_ADDRESS}/realms/${realm}/`)
  .then(function (response) {
    // handle success
    // console.log(response);
    // const data = response.json();     
    const data = response.data;

    kcConfig = {  
      // "realm" : "gateway",
      // "realm-public-key" : "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTbkO5rSSW/iWrKboNSJj4uXtTFRtCr/ts2twu4wOpVChY/e6qwZ5K72IsGzuU5WD/g7Pcsr+eQINf9NgVQeV2r5fGuqnHN7ousije6iminBmuujbpeD+29fuczTUMiyxAH+RGEVA3qbC2LJ6Erl4LtKGY/Sn48vQDSxozIo8yG65MWn7GudPmGCXzVvJPfv/DGfCwfw4ySKcNWkMgAKDUees8YnpljDd+q0ZBaWBqjctk8mWk/FV0Pl8ExuSqG+3jM/Zr7hYNSmcjwqBfoSHGExs41i5ARUQSW9REfAx428VfdYL/Q0dPJr8lT8hjV523ffcptiaNX24bZabssu/wIDAQAB",
      "realm" : data['realm'],
      "realm-public-key" : data['public_key'],
      "auth-server-url" : `${KEYCLOAK_HOST_ADDRESS}`,
      "ssl-required" : "external",
      "resource" : "gateway-api",
      "public-client" : true
    }
    
    core.log(`kcConfig: ${JSON.stringify(kcConfig)}`);
    
    keycloak = new Keycloak({
      store: memoryStore,
      // scope: 'phone'
    }, kcConfig);
    
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });





// initKeycloak(realmToProtect).catch((err) => {

//   // keycloak auth server is possibly down or not started yet (e.g. due to docker-compose startup procedure) - wait a sec and retry
//   core.log(`${err} : cannot get realm config info. Retrying in 30 seconds.`);

//   timeoutObj = setTimeout(initKeycloakRetry(realmToProtect), 30*1000);

// });

// async function initKeycloakRetry(realmToProtect) {

//   core.log(`initKeycloakRetry: getting realm config info`);

//   const pkey = await initKeycloak(realmToProtect).catch((err) => {
//     core.log(`initKeycloakRetry: ${err} : cannot get realm config info.`);
//   });

//   return pkey;
// }




//
// logging for API calls
// all API calls start with '::' + PROTOCOL + METHOD + APICALL in the logs
//
app.use(function (req, res, next) {
  APP_PATH = req.path;

  // init realm to be protected
  if (req.query.realm || req.body.realm) {
    realmToProtect = req.query.realm || req.body.realm;
  } else {
    realmToProtect = realm;
  }

  // const tokenId = (req.kauth) ? req.kauth.grant.content.sub : '';
 
    // source: https://test.crayonic.io/auth/realms/gateway/, "public_key"
    let publicKey = `-----BEGIN PUBLIC KEY-----\n${kcConfig['realm-public-key']}\n-----END PUBLIC KEY-----`;  

    if (req.headers.authorization) {

      if (realmToProtect != kcConfig['realm']) {
        core.log(`changing realm to: ${realmToProtect}`);
        try {

          axios.get(`${KEYCLOAK_HOST_ADDRESS}/realms/${realm}/`)
          .then(function (response) {
            // handle success
            // console.log(response);
            // const data = response.json();     
            const data = response.data;
        
            kcConfig = {  
              // "realm" : "gateway",
              // "realm-public-key" : "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTbkO5rSSW/iWrKboNSJj4uXtTFRtCr/ts2twu4wOpVChY/e6qwZ5K72IsGzuU5WD/g7Pcsr+eQINf9NgVQeV2r5fGuqnHN7ousije6iminBmuujbpeD+29fuczTUMiyxAH+RGEVA3qbC2LJ6Erl4LtKGY/Sn48vQDSxozIo8yG65MWn7GudPmGCXzVvJPfv/DGfCwfw4ySKcNWkMgAKDUees8YnpljDd+q0ZBaWBqjctk8mWk/FV0Pl8ExuSqG+3jM/Zr7hYNSmcjwqBfoSHGExs41i5ARUQSW9REfAx428VfdYL/Q0dPJr8lT8hjV523ffcptiaNX24bZabssu/wIDAQAB",
              "realm" : data['realm'],
              "realm-public-key" : data['public_key'],
              "auth-server-url" : `${KEYCLOAK_HOST_ADDRESS}`,
              "ssl-required" : "external",
              "resource" : "gateway-api",
              "public-client" : true
            }
            
            core.log(`kcConfig: ${JSON.stringify(kcConfig)}`);
            
            keycloak = new Keycloak({
              store: memoryStore,
              // scope: 'phone'
            }, kcConfig);
            
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(function () {
            // always executed
          });

          // publicKey = initKeycloakRetry(realmToProtect);      


        } catch (error) {
          core.log(`${error} : unable to change realm.`);
        }
      }
    

      // (req.headers['authorization']).contains('Bearer: ')
      try {

        const token = getToken(req);


        jwt.verify(token, publicKey, function (err, decoded) {
          if (err) {
            core.log(` <err> :: ${req.protocol} ${req.method} ${APP_PATH} - (${kcConfig['realm']}) - auth - ${err}`); 
            core.log(token);
            core.log(publicKey);
          } else {
            realm = (decoded.iss).split('/')[5];
            username = decoded.preferred_username;

            core.log(` ${username}/${realm} :: ${req.protocol} ${req.method} ${APP_PATH} - JWT verified (${kcConfig['realm']})`);
            // core.log(`JWT token verified: ${JSON.stringify(decoded)}`);
          }
          
        });


      } catch(err) {
        core.log(` <unauthorized> :: ${req.protocol} ${req.method} ${APP_PATH} - (${kcConfig['realm']}) - verify - ${err}`);    
      }
  
    } else {
      core.log(` <unauthorized> :: ${req.protocol} ${req.method} ${APP_PATH} - (${kcConfig['realm']}) - no authorization header`);      

    }
  

  next();

});


// apikey authentication
// check whether CGWAPIKEY is set in environment
if (process.env.CGWAPIKEYCHECK==='on' && !process.env.CGWAPIKEY) {
  core.log(`Exiting... environment variable CGWAPIKEY must be set`);
  process.exit(1);
}

//
// DB storage for users (PostgresSQL)
//
// let APP_DB = null;
const {sql} = require('@databases/pg');
const db = require('./db');

//
// 1st time db init - conditional tables creation
//
dbInit().catch((err) => {
  core.log(err);
  process.exit(1);
});

async function dbInit() {
  await db.query(sql`
    CREATE TABLE IF NOT EXISTS credentials (
      ID SERIAL NOT NULL PRIMARY KEY,
      rpid TEXT NOT NULL,
      realm TEXT NOT NULL,
      username TEXT NOT NULL,
      userid TEXT,
      credentialid TEXT NOT NULL,
      userhandle TEXT NOT NULL,
      CONSTRAINT userhandle UNIQUE (userhandle)
    );
    CREATE TABLE IF NOT EXISTS backups (
      ID SERIAL NOT NULL PRIMARY KEY,
      rpid TEXT NOT NULL,
      realm TEXT NOT NULL,
      username TEXT NOT NULL,
      userid TEXT,
      device JSONB,
      backup JSONB
    );
    CREATE TABLE IF NOT EXISTS policies (
      ID SERIAL NOT NULL PRIMARY KEY,
      rpid TEXT NOT NULL,
      realm TEXT NOT NULL,
      kvpolicy JSONB,
      CONSTRAINT con_realm UNIQUE (realm)      
    );    
  `);

  // kvpolicy DB fix (user_atrribute/value too small)
  // TODO: move this fix to separate location

  // await db.query(sql`
  //   ALTER TABLE "user_attribute"
  //     ALTER COLUMN "value" TYPE VARCHAR(8192),
  //     ALTER COLUMN "value" DROP NOT NULL,
  //     ALTER COLUMN "value" SET DEFAULT 'NULL::character varying';
  //   `);

  // APP_DB = true;
  core.log(`DB connected and running.`);
  core.log(``);

  // await db.dispose();
}


//
// credential management API
//

async function getCredentials (req, res) {
  await db.query(sql`SELECT * FROM credentials ORDER BY realm, id ASC;`)
    .then(results => res.status(200).json(results))
    .catch(error => res.status(400).json(error));

}

// input params (GET form): realm, userhandle
// if success: returns status 200 and json response (username)
// if error: returns status 400
async function getUserhandle (req, res) {

  // new rpid:
  // const rpid = req.headers.host
  const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

  await db.query(sql`SELECT userhandle, username, realm FROM credentials WHERE rpid = ${rpid} AND realm = ${req.query.realm} AND userhandle = ${req.query.userhandle};`)
  .then(results => {
      core.log(`getUserhandle: userhandle: ${results[0].userhandle}, username: ${results[0].username}, realm: ${results[0].realm}, rpid: ${rpid}`);
      res.status(200).json({username: results[0].username});
  })
  .catch(error => {
      core.log(`getUserhandle: ${error}`);      
      res.status(400).json(error);
  });

}

// input params (POST form): realm, userhandle, username
// if success: returns status 200 and json response
// if error: returns status 400
async function setUserhandle (req, res) {

  // new rpid:
  // const rpid = req.headers.host
  // const rpid = HOST;

  const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

  const credentialid = 0;
  const userid = 0;

  // core.log(`setUserhandle(): realm: ${req.body.realm}, userhandle: ${req.body.userhandle}, username: ${req.body.username} `);

  // psql insert
  await db.query(sql`
    INSERT INTO credentials (rpid, credentialid, userid, realm, userhandle, username) 
    VALUES (${rpid}, ${credentialid}, ${userid}, ${req.body.realm}, ${req.body.userhandle}, ${req.body.username})
    ON CONFLICT (userhandle) DO NOTHING;`)
  .then(results => {
    core.log(`setUserhandle: userhandle/username (${req.body.userhandle}/${req.body.username}) processed for realm '${req.body.realm}' (sql response: ${results})`);
    res.status(200).json(results);
  })
  .catch(error => {
      core.log('setUserhandle: ' + error);      
      res.status(400).json(error);
  });

}

// Test function only: removes userhandle/username pair for given userhandle
async function removeUserhandle (req, res) {

  if (req.body.userhandle) {

    await db.query(sql`DELETE FROM credentials WHERE userhandle = ${req.body.userhandle};`)
      .then(results => res.status(200).json(results))
      .catch(error => res.status(400).json(error));

  } else {
    core.log('removeUserhandle: Error - Missing input parameters');
    res.status(400).json({error: 'Missing input parameters'});
  }
}


//
// policies API
//

async function getPolicies (req, res) {
  await db.query(sql`SELECT * FROM policies ORDER BY realm, id ASC;`)
    .then(results => res.status(200).json(results))
    .catch(error => res.status(400).json(error));

}

// input params (GET form): realm
// if success: returns status 200 and json response
// if error: returns status 400

async function getPolicy (req, res) {

  // new rpid:
  // const rpid = req.headers.host
  // const rpid = HOST;
  const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

  const realm = `${rpid}/${req.query.realm}`;

  // core.log(`getPolicy: realm = ${realm}`);
  await db.query(sql`SELECT kvpolicy FROM policies WHERE realm = ${realm};`)
  .then(results => {
    if (results.length) {
      core.log(`getPolicy: Policy for [${realm}] retrieved: ${JSON.stringify(results[0].kvpolicy)}`);
      res.status(200).json(results[0].kvpolicy);
    }
    else {
      res.status(400).json({'error': 'Policy not found'}); 
    }
  })
  .catch(error => {
      core.error(`getPolicy: error`, error);
      res.status(400).json(error);
  });

}

// input params (POST form): realm, kvpolicy
// if success: returns status 200 and empty json response
// if error: returns status 400

// realm
// test marek:
// 010100000a00000a020000000000000300012c00012c04010501060107020703080109000a000b000c000d010e000f011001
async function setPolicy (req, res) {

  // new rpid:
  // const rpid = req.headers.host
  // const rpid = HOST;
  
  const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';
  const realm = `${rpid}/${req.body.realm}`;

  // psql upsert
  await db.query(sql`
    INSERT INTO policies (rpid, realm, kvpolicy) VALUES (${rpid}, ${realm}, ${{kvpolicy: req.body.kvpolicy}})
    ON CONFLICT (realm) DO UPDATE SET kvpolicy = EXCLUDED.kvpolicy;`)
  .then(results => {
    core.log(`setPolicy: Policy saved for: ${realm} (sql response: ${results})`);
    res.status(200).json(results);
  })
  .catch(error => {
      core.log('setPolicy: ' + error);      
      res.status(400).json(error);
  });

}

//
// authentication
//
// check validity of keycloak token (keycloak lib)
// "Authorization": `Bearer ${keycloak.token}`,
//
function apiKeyCheck (resource) {

  // core.log(`Authorization: ${req.headers.Authorization}, cgwapikey: ${req.headers.cgwapikey}`);

  if (process.env.CGWAPIKEYCHECK==='on') {

  //   if (req.headers.cgwapikey === process.env.CGWAPIKEY) {
  //     next();
  
    // if (keycloak.protect(resource)) {      
    if (resource) {      

      return true;

    } else {
      core.error(`Error: Unauthorized access. Host: ${req.headers.host}`, '');
      res.status(401).json({error: 'Unauthorized access'});
    }
  
  } else {
    return true;

  }

}


//
// info page
//
function getInfo (req, res) {

  let tmp = req.headers;
  tmp.app = 'Crayonic Gateway API';
  tmp.version = VERSION;

  // let result = JSON.stringify(tmp);
  res.status(200).json(tmp);
}


//
// backup/restore API
//

async function getUsers (req, res) {

  await db.query(sql`SELECT * FROM backups ORDER BY rpid, realm, username ASC;`)
    // .then(results => res.status(200).json(results.rows))
    .then(results => res.status(200).json(results))
    .catch(error => res.status(400).json(error));
}


//
// /:realm/:username
//
// input params (GET form): realm, username
// if success: returns status 200 and json response
// if error: returns status 400

async function getBackup (req, res) {
  // const username = parseInt(request.params.id)
  // const username = req.params.username;

  const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

  const rows = await db.query(sql`
    SELECT backup -> 'payload' AS payload FROM backups WHERE rpid = ${rpid} AND realm = ${req.query.realm} AND username = ${req.query.username} ORDER BY id DESC;`)
  .then(results => {
    core.log('getBackup: Payload retrieved: ' + results[0].payload);
    res.status(200).json(results[0]);
  })
  .catch(error => {
    core.log('getBackup error: ' + error);      
    res.status(400).json(error);
  });

}

//
// /:realm/:username
//
// input params (GET form): realm, username
// if success: returns status 200 and json response
// if error: returns status 400

async function getBackupMeta (req, res) {
  
  const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

  await db.query(sql`SELECT backup FROM backups WHERE rpid = ${rpid} AND realm = ${req.query.realm} AND username = ${req.query.username} ORDER BY id DESC;`)
    .then(results => res.status(200).json(results))
    .catch(error => res.status(400).json(error));

}



//
// /:realm/:username
//
// input params (POST form): realm, username, credentialId (optional), userHandle (optional), payload, userid (optional)
// if success: returns status 200 and json response
// if error: returns status 400

async function setBackup (req, res) {

  // const rpid = HOST;
  // new rpid:
  // const rpid = req.headers.host
  const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

  let backupData;

  const device = {
    credentialId: req.body.credentialId,
    userHandle: req.body.userHandle
  }

  if (!req.body.userid)
    req.body.userid = 0;

  // POST request (use req.query.username for GET requests)
  if (req.body.realm && req.body.username && req.body.payload) {

    try {
      const payload = req.body.payload;
      const length = (payload.length)/2;  // hex byte = 2 chars
      // sn is fixed 12 chars length
      const snPos = payload.indexOf('73000c') + 6;  // char 's' = serial number, including 2 bytes(4 chars) SN length (e.g. 73000C)
      const serialNumber = core.hex2ascii(payload.substring(snPos, snPos+24)); // sn length = 12 chars 

      // 4639454536364239454131327
      core.log('setBackup processing for sn: ' + serialNumber);

      backupData = {
        timestamp: new Date().toUTCString(),
        sn: serialNumber,
        payload: payload,
        length: length
      };

    } catch (err) {
      core.log("Backup Payload processing error: " + err);
      res.status(400).json({error: err});
    }

    await db.query(sql`
      INSERT INTO backups (rpid, realm, username, userid, device, backup)
      VALUES (${rpid}, ${req.body.realm}, ${req.body.username}, ${req.body.userid}, ${device}, ${backupData});
    `)
    .then(results => res.status(200).json(results))
    .catch(error => res.status(400).json(error));

  } else {
    core.log('setBackup: Error - Missing input parameters');    
    res.status(400).json({error: 'Missing input parameters'});
  }
}

// Test function only: removes all backups for a given user
async function removeBackup (req, res) {

  if (req.body.realm && req.body.username) {

    // const rpid = HOST;
    // new rpid:
    // const rpid = req.headers.host
    const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

    await db.query(sql`DELETE FROM backups WHERE rpid = ${rpid} AND realm = ${req.body.realm} AND username = ${req.body.username};`)
      .then(results => res.status(200).json(results))
      .catch(error => res.status(400).json(error));

  } else {
    core.log('removeBackup: Error - Missing input parameters');
    res.status(400).json({error: 'Missing input parameters'});
  }
}

//
// returns list of resident keys for given user. The data is parsed from the latest backup payload
// input params (GET form): realm, username
//
async function parseResidentKeys (req, res) {

  if (req.query.realm && req.query.username) {

    // const rpid = HOST;
    const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

    let credentialData;
    let payload;
    let sn;
    let timestamp;
    let rks = [];

    // first retrieve latest backup
    await db.query(sql`SELECT backup FROM backups WHERE rpid = ${rpid} AND realm = ${req.query.realm} AND username = ${req.query.username} ORDER BY id DESC;`)
      .then(results => {

        // core.log(JSON.stringify(results));

        if (!results[0]) {
          credentialData = [];
        } else {

          payload = results[0].backup.payload;
          sn = results[0].backup.sn;
          timestamp = results[0].backup.timestamp;

          core.log('parseResidentKeys starting.');
          
          try {
            // char 'r' = number of resident keys included in payload - including 2 bytes(4 chars) rkCnt length (e.g. 720001)
            // RKcnt is fixed 1 char length

            // 'r' = 720001 = RK count (e.g. 720001xx) -> xx = number of RKs in payload
            // 'i' = 690001xx = RK index. xx = index num (e.g. 69000100 = 0, 69000101 = 1, etc.)
            // 'C' = 4300xx (72/0x48 bytes default) = credential.id (e.g. 4b56188b100be7fb6fa6a8d1ec7f295764bea2e3742b2cfe0955616c4c62c63487c821a52647e6fc646b9ce1488bbe2a879ec8b345a6e1b275628cacf36ddfc946671ee724000000) -> "KV...."
            // 'I' = 4900xx (xx bytes) = credential.user.id (e.g. 63393432623466372d653039652d343561302d396430382d653262346232643733343735) -> c942b4f7-e09e-45a0-9d08-e2b4b2d73475
            // 'N' = 4e00xx = credential.user.name - xx = username length
            // 'D' = 4400xx = credential.user.displayName - xx = displayname length
            // 'O' = 4f00xx = credential.user.icon - should be skipped - not saved in current payload
            // 'R' = 5200xx = rpId (e.g.: 637261796f6e69632e696f = crayonic.io)

            if (payload.indexOf('720001') < 0) {
              core.log('No RKs found');
              throw ('No resident keys found in payload');
            }
          
            // rk cnt length = 1 char (0-255)
            // hexToDec: parseInt(hexString, 16)
            const rkCnt = parseInt(payload.substring(payload.indexOf('720001')+6, payload.indexOf('720001')+8), 16);
            core.log(`rkCnt: ${rkCnt}`);

            // validity check
            if (payload.substring(payload.indexOf('69000100')) < 0) {
              core.log('RK payload mismatch');
              throw ('RK payload mismatch');
            }

            // iterate through all resident keys
            const residentKeysPayload = payload.substring(payload.indexOf('69000100'));
            let i;
            let rkPos;
            let residentKeyData;
            let rk = new Object; // rk.credentialId, credentialUserID, etc.
            let pos;
            let cmd;
            let length;
            let data;

            for (i=0; i<rkCnt; i++) {          
                rkPos = residentKeysPayload.indexOf(`690001${core.dec2smallhex(i)}`) + 8;

                if ((i+1) == rkCnt)
                    residentKeyData = residentKeysPayload.substring(rkPos);
                else 
                    residentKeyData = residentKeysPayload.substring(rkPos, residentKeysPayload.indexOf(`690001${core.dec2smallhex(i+1)}`) );        

                // core.log(`residentKeyData: ${residentKeyData}, length: ${residentKeyData.length}`);
                rk.icon = ''; // not always present

                pos = 0;
                while (pos < residentKeyData.length) {
                    cmd = residentKeyData.substring(pos, pos+4);
                    length = parseInt(residentKeyData.substring(pos+4, pos+6), 16);
                    data = residentKeyData.substring(pos+6, pos+6+(length*2));

                    // core.log(`pos: ${pos}, cmd: ${cmd}, (${length}): ${data}`);

                    switch (cmd) {
                        default:
                        case '4300':
                            rk.credentialId = data;
                            pos += (6+data.length);
                            break;
                        case '4900':
                            rk.credentialUserId = data;
                            pos += (6+data.length);
                            break;
                        case '4e00':
                            rk.credentialUserName = data;
                            pos += (6+data.length);
                            break;
                        case '4400':
                            rk.credentialUserDisplayName = data;
                            pos += (6+data.length);
                            break;
                        case '5200':
                            rk.rpId = data;
                            pos += (6+data.length);
                            break;
                        case '4f00':
                            rk.icon = data;
                            pos += (6+data.length);
                            break;
                    }

                }
                
                // core.log(`done. pos: ${pos}`);


                rk.credentialUserIdCGW = core.isValidV4UUID(core.hex2ascii(rk.credentialUserId)) ? (core.hex2ascii(rk.credentialUserId)) : '';
                
                // core.log(`rkIndex: ${i}`);
                // core.log(`rkData: ${residentKeyData}`);
                // core.log(`credentialId: ${rk.credentialId}`);
                // core.log(`credentialUserId: ${rk.credentialUserId}`);
                // core.log(`credentialUserIdCGW: ${rk.credentialUserIdCGW}`);
                // core.log(`credentialUserName: [${rk.credentialUserNameLength}] ${core.hex2ascii(rk.credentialUserName)}`);
                // core.log(`credentialUserDisplayName: [${rk.credentialUserDisplayNameLength}] ${core.hex2ascii(rk.credentialUserDisplayName)}`);
                // core.log(`credentialIcon: [${rk.iconLength}] ${core.hex2ascii(rk.icon)}`);
                // core.log(`rpId: [${rk.rpIdLength}] ${core.hex2ascii(rk.rpId)}`);
                // core.log('');
            
                rks.push({
                    rkIndex: i, 
                    // residentKey: residentKeyData,              
                    credentialId: rk.credentialId,
                    credentialUserId: rk.credentialUserId,
                    credentialUserIdCGW: rk.credentialUserIdCGW,
                    credentialUserName: core.hex2ascii(rk.credentialUserName),
                    credentialUserDisplayName: core.hex2ascii(rk.credentialUserDisplayName),
                    credentialIcon: core.hex2ascii(rk.icon),
                    rpId: core.hex2ascii(rk.rpId)              
                });
        
            }

            credentialData = {
                timestamp: timestamp,
                sn: sn,
                payload: payload,
                rkCnt: rkCnt,
                residentKeys: rks
            };
              
      
          } catch (err) {
            core.log("parseResidentKeys() processing error: " + err);
            res.status(400).json({error: err});
          }
        }

      })
      .then(results => res.status(200).json(credentialData))
      .catch(error => res.status(400).json(error));

  } else {
    core.log('parseResidentKeys: Error - Missing input parameters');
    res.status(400).json({error: 'Missing input parameters'});
  }
  
}

// input (POST): realm, username, payload (JSON)
// saves provided list of resident keys for given user to a new backup and restores KV with modified payload
// this function is a workaround until new KV command setRK is implemented in KV firmware
// we are not mofifiying SN. backup timestamp is updated in setBackup()
async function setResidentKeys (req, res) {

  if (req.body.realm && req.body.username && req.body.payload) {

    // const rpid = req.headers.host;
    // const rpid = HOST;
    const rpid = (USE_DYNAMIC_RPID) ? req.headers.host : '0.0.0.0';

    const realm = req.body.realm;
    const username = req.body.username;
    const sn = req.body.payload.sn;
    const payload = req.body.payload.payload; // backup payload in hex
    const residentKeys = req.body.payload.residentKeys; // RK array
    const rkCnt = req.body.payload.rkCnt;  // number of RKs (=residenKeys.length)

    let backupData; // this will be saved in setBackup()
    let corePayload;  // clean KV backup - no RKs present
    let newPayload;
    let i;

      // first check incoming payload - some resident keys might have been removed:
      // 1) check number of RKs in incoming payload, if 0 -> just save corePayload
      // 2) update RK count ('r' = 0x720001xx) in new payload
      // 3) save all provided RKs in new payload
      // 4) create and save new backup to DB - todo: reuse setBackup()
      // 5) call restoreKeyvault
  
      try {
        core.log('setResidentKeys starting.');
  
        // position for end of clean payload without RKs (end of sn)
        const snPosEnd = payload.indexOf('73000c') + 6 + 24;
        corePayload = payload.substring(0, snPosEnd);
        
        if ((payload.indexOf('720001') < 0) || (residentKeys.length == 0) || (rkCnt == 0)) {
          core.log('No resident keys found in payload');
          // throw ('No resident keys found in payload');
          newPayload = corePayload;
        } else {
            newPayload = `${corePayload}720001${core.dec2smallhex(residentKeys.length)}`;
  
            for (i=0; i<residentKeys.length; i++) {      
              newPayload += `690001${core.dec2smallhex(i)}`;
              newPayload += `4300${core.dec2smallhex(residentKeys[i].credentialId.length /2)}${residentKeys[i].credentialId}`;
              newPayload += `4900${core.dec2smallhex(residentKeys[i].credentialUserId.length /2)}${residentKeys[i].credentialUserId}`;
              newPayload += `4e00${core.dec2smallhex(residentKeys[i].credentialUserName.length)}${core.ascii2hex(residentKeys[i].credentialUserName)}`;
              newPayload += `4400${core.dec2smallhex(residentKeys[i].credentialUserDisplayName.length)}${core.ascii2hex(residentKeys[i].credentialUserDisplayName)}`;
              
              // icon is optional
              if (residentKeys[i].credentialIcon.length > 0) {
                newPayload += `4f00${core.dec2smallhex(residentKeys[i].credentialIcon.length)}${core.ascii2hex(residentKeys[i].credentialIcon)}`;
              }
  
              newPayload += `5200${core.dec2smallhex(residentKeys[i].rpId.length)}${core.ascii2hex(residentKeys[i].rpId)}`;            
            }
        }
  
        backupData = {
          timestamp: new Date().toUTCString(),
          sn: sn,
          payload: newPayload,
          length: (newPayload.length)/2
        };
  
        // core.log(`realm: ${realm}, username: ${username}`);
        // core.log(JSON.stringify(backupData, null, 4));

      // save new backup to DB
      await db.query(sql`
      INSERT INTO backups (rpid, realm, username, backup)
      VALUES (${rpid}, ${realm}, ${username}, ${backupData});
      `)
      .then(results => res.status(200).json(results))
      .catch(error => res.status(400).json(error));
  
    } catch (err) {
      core.log("setResidentKeys processing error: " + err);
      res.status(400).json({error: err});
    }


  } else {
    core.log('setResidentKeys: Error - Missing input parameters');
    res.status(400).json({error: 'Missing input parameters'});
  }
  
}


//////////////////////
// REST API routing //
//////////////////////
//
// new set of API calls from v0.2.0 (postgres + policies + KV fw 0.5.0 and later)
//
// GET  /cgw/

// root
app.get(CGW_API_ROOT, (req, res) => {
  res.status(200).send('OK');
})

// input: n/a
app.get(`${CGW_API_ROOT}/info`, getInfo);






// intro text
function startupMessage(protocol) {
  core.log('');
  core.log(`Crayonic Gateway API Server [CGW-API] version ${VERSION} started`);
  core.log(`Running on Node.js ${process.version} on a ${os.platform()}-type OS, release ${os.release()}, architecture: ${os.arch()}`);
  core.log(`Environment: ${NODE_ENV}, Hostname: ${os.hostname()}, Protocol: ${process.env.NODE_PROTOCOL}, Running at ${protocol}://${HOST}:${PORT}`);

  core.log(`Total mem: ${os.totalmem()/1048576} MB, Free mem: ${os.freemem()/1048576} MB`);
  core.log(`CGWAPIKEYCHECK: ${process.env.CGWAPIKEYCHECK}`);
  // core.log(`Keycloak Auth config: ${JSON.stringify(keycloak.getConfig())}`);
}

//
// startup
//

if (NODE_ENV === 'prod') {
  
  // does kubernetes TLS config exist? (env values NODE_TLS_CRT and NODE_TLS_KEY)
  // if yes: run HTTPS at port 3010
  // if not: run HTTP, port 3000
  if (process.env.NODE_PROTOCOL==='https') {
    PORT = 3010;
    let tls_key;
    let tls_crt;
    let tls_txt;

    // if NODE_TLS_KEY, NODE_TLS_KEY are not set in env variables, try to read values from tls.crt and tls.key files
    if (process.env.NODE_TLS_CRT && process.env.NODE_TLS_KEY) {
      tls_key = process.env.NODE_TLS_KEY;
      tls_crt = process.env.NODE_TLS_CRT;
      tls_txt = `TLS cert loaded from env`;
    }
    else {
      tls_key = fs.readFileSync(`./tls.key`);
      tls_crt = fs.readFileSync(`./tls.crt`);
      tls_txt = `TLS cert loaded from tls.crt`;
    }

    https.createServer({key: tls_key, cert: tls_crt}, app ).listen(PORT, HOST, () => {
      startupMessage('https');
      // core.log(`${tls_txt}: ${tls_crt}`);
      core.log(`${tls_txt}`);
    });    

  } else {
    PORT = 3000;
    http.createServer(app).listen(PORT, HOST, () => {
      startupMessage('http');
    });  
  }

  
} else {

  // https://localhost:443
  // production certificate file (tls) or localhost cert
  // const certFile = (NODE_ENV === "prod") ? 'tls' : 'localhost';
  PORT = 443;

  https.createServer({key: fs.readFileSync(`./localhost.key`), cert: fs.readFileSync(`./localhost.crt`)}, app ).listen(PORT, HOST, () => {
    startupMessage('https');
  });
}

//
// keycloak connect init + secured routing
//
async function initKeycloak(realm) {

  try {
    const r = await fetch(`${KEYCLOAK_HOST_ADDRESS}/realms/${realm}/`);
    const d = await r.json();

    kcConfig = {  
      // "realm" : "gateway",
      // "realm-public-key" : "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTbkO5rSSW/iWrKboNSJj4uXtTFRtCr/ts2twu4wOpVChY/e6qwZ5K72IsGzuU5WD/g7Pcsr+eQINf9NgVQeV2r5fGuqnHN7ousije6iminBmuujbpeD+29fuczTUMiyxAH+RGEVA3qbC2LJ6Erl4LtKGY/Sn48vQDSxozIo8yG65MWn7GudPmGCXzVvJPfv/DGfCwfw4ySKcNWkMgAKDUees8YnpljDd+q0ZBaWBqjctk8mWk/FV0Pl8ExuSqG+3jM/Zr7hYNSmcjwqBfoSHGExs41i5ARUQSW9REfAx428VfdYL/Q0dPJr8lT8hjV523ffcptiaNX24bZabssu/wIDAQAB",
      "realm" : d['realm'],
      "realm-public-key" : d['public_key'],
      "auth-server-url" : `${KEYCLOAK_HOST_ADDRESS}`,
      "ssl-required" : "external",
      "resource" : "gateway-api",
      "public-client" : true
    }
    
    core.log(`kcConfig: ${JSON.stringify(kcConfig)}`);

    keycloak = new Keycloak({
      store: memoryStore,
      // scope: 'phone'
    }, kcConfig);
    
    app.use(keycloak.middleware({
      logout: '/logout',
      admin: '/'
    }));
    

    // input: n/a
    // app.get(`${CGW_API_ROOT}/users`, apiKeyCheck(), keycloak.protect('realm:api-admin'), getUsers); // admin function only
    app.get(`${CGW_API_ROOT}/users`, apiKeyCheck('realm:api-admin'), getUsers); // admin function only

    // input: n/a
    app.get(`${CGW_API_ROOT}/policies`, apiKeyCheck('realm:api-admin'), getPolicies); // admin function only

    // input: n/a
    app.get(`${CGW_API_ROOT}/credentials`, apiKeyCheck('realm:api-admin'), getCredentials); // admin function only

    // input: realm, username
    app.post(`${CGW_API_ROOT}/backup/remove`, apiKeyCheck('realm:api-admin'), removeBackup);  // admin function only

    // input: userhandle
    app.post(`${CGW_API_ROOT}/credentials/userhandle/remove`, apiKeyCheck('realm:api-admin'), removeUserhandle);  // admin function only


    //
    // CREDENTIALS/UserHandle sync
    // for usernameless login: syncs userhandle/username pairs

    // input: realm, userhandle
    // returns username for given userhandle + realm
    app.get(`${CGW_API_ROOT}/credentials/userhandle`, apiKeyCheck('realm:api-user'), getUserhandle);

    // input: realm, userhandle, username
    // inserts/updates username/userhandle pair for given userhandle + realm
    app.post(`${CGW_API_ROOT}/credentials/userhandle`, apiKeyCheck('realm:api-user'), setUserhandle);


    //
    // CREDENTIALS - Resident Keys management
    // for credential management in Gateway

    // input: realm, username
    // returns list of resident keys for given user. The data is parsed from the latest backup payload
    app.get(`${CGW_API_ROOT}/credentials/show`, apiKeyCheck('realm:api-user'), parseResidentKeys);

    // input: realm, username
    // returns list of resident keys for given user. The data is parsed from the latest backup payload
    // duplicate of '/credentials/show'
    app.get(`${CGW_API_ROOT}/credentials/rk`, apiKeyCheck('realm:api-user'), parseResidentKeys);

    // input: realm, username, payload (same json structure as returned by GET /credentials/rk call)
    // saves provided list of resident keys for given user to a new backup and restores KV with modified payload
    // this function is a workaround until new KV command setRK is implemented in KV firmware
    app.post(`${CGW_API_ROOT}/credentials/rk`, apiKeyCheck('realm:api-user'), setResidentKeys);



    //
    // POLICIES
    //

    // input: realm
    // app.get(`${CGW_API_ROOT}/policy`, apiKeyCheck, keycloak.protect('realm:api-user'), getPolicy);
    app.get(`${CGW_API_ROOT}/policy`, apiKeyCheck('realm:api-user'), getPolicy);

    // input: realm, kvpolicy
    app.post(`${CGW_API_ROOT}/policy`, apiKeyCheck('realm:api-admin'), setPolicy);


    //
    // BACKUP/RESTORE
    //

    // input: realm, username
    app.get(`${CGW_API_ROOT}/backup`, apiKeyCheck('realm:api-user'), getBackup); // returns only backup payload

    // input: realm, username
    app.get(`${CGW_API_ROOT}/backup/meta`, apiKeyCheck('realm:api-user'), getBackupMeta);  // returns full backup metadata

    // input: realm, username, payload, credentialId (optional), userHandle(optional), userid (optional)
    app.post(`${CGW_API_ROOT}/backup`, apiKeyCheck('realm:api-user'), setBackup);

    clearTimeout(timeoutObj);

    return d['public_key']; // realm's public key

  } catch (error) {

    core.log(`initKeycloak: can't get config for realm '${realm}: ${error}'`);
    return false;
  }

}

function getToken(req) {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    return req.headers.authorization.split(" ")[1];
  } 
  return null;
}


// TODO: test auth and update


// app.use(keycloak.middleware({
//   logout: '/logout',
//   admin: '/'
// }));

// debug functions, remove in prod!
if (KEYCLOAK_HOST != 'crayonic.io') {

  // input: n/a
  // app.get(`${CGW_API_ROOT}/users`, apiKeyCheck(), keycloak.protect('realm:api-admin'), getUsers); // admin function only
  app.get(`${CGW_API_ROOT}/users`, getUsers); // admin function only

  // input: n/a
  app.get(`${CGW_API_ROOT}/policies`, getPolicies); // admin function only

  // input: n/a
  app.get(`${CGW_API_ROOT}/credentials`, getCredentials); // admin function only

  // input: realm, username
  app.post(`${CGW_API_ROOT}/backup/remove`, removeBackup);  // admin function only

  // input: userhandle
  app.post(`${CGW_API_ROOT}/credentials/userhandle/remove`, removeUserhandle);  // admin function only


}



//
// CREDENTIALS/UserHandle sync
// for usernameless login: syncs userhandle/username pairs

// input: realm, userhandle
// returns username for given userhandle + realm
app.get(`${CGW_API_ROOT}/credentials/userhandle`, getUserhandle);

// input: realm, userhandle, username
// inserts/updates username/userhandle pair for given userhandle + realm
app.post(`${CGW_API_ROOT}/credentials/userhandle`, setUserhandle);


//
// CREDENTIALS - Resident Keys management
// for credential management in Gateway

// input: realm, username
// returns list of resident keys for given user. The data is parsed from the latest backup payload
app.get(`${CGW_API_ROOT}/credentials/show`, parseResidentKeys);

// input: realm, username
// returns list of resident keys for given user. The data is parsed from the latest backup payload
// duplicate of '/credentials/show'
app.get(`${CGW_API_ROOT}/credentials/rk`, parseResidentKeys);

// input: realm, username, payload (same json structure as returned by GET /credentials/rk call)
// saves provided list of resident keys for given user to a new backup and restores KV with modified payload
// this function is a workaround until new KV command setRK is implemented in KV firmware
app.post(`${CGW_API_ROOT}/credentials/rk`, setResidentKeys);



//
// POLICIES
//

// input: realm
// app.get(`${CGW_API_ROOT}/policy`, apiKeyCheck, keycloak.protect('realm:api-user'), getPolicy);
app.get(`${CGW_API_ROOT}/policy`, getPolicy);

// input: realm, kvpolicy
app.post(`${CGW_API_ROOT}/policy`, setPolicy);


//
// BACKUP/RESTORE
//

// input: realm, username
app.get(`${CGW_API_ROOT}/backup`, getBackup); // returns only backup payload

// input: realm, username
app.get(`${CGW_API_ROOT}/backup/meta`, getBackupMeta);  // returns full backup metadata

// input: realm, username, payload, credentialId (optional), userHandle(optional), userid (optional)
app.post(`${CGW_API_ROOT}/backup`, setBackup);


