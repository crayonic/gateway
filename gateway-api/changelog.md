## Crayonic Gateway API Server changelog

This document lists changes and improvements in the code.

See **package.json** for actual Crayonic Gateway API Server version and **About** menu item on KeyVault device for Firmware version

### v0.2.8
- updated: docker-compose and kubernetes compatibility improvements, moved to gateway-local repo

### v0.2.0
- replaced: mongoDB with PostgresSQL as main DB for storing user backups/KV policies
- removed: built-in Webauthn/FIDO2 web test site for Crayonic KeyVault (KV) - www: '/app' - if you need one, please build from repo: https://gitlab.com/mchorvat/gatewayapp/-/commits/v1-mongodb
- moved API root to: /cgw/, e.g. https://crayonic.io/cgw/
- new API structure - see README.md for more information

### v0.1.3
- backup now stores KV serial number in payload metadata
- /backup/exists?username=[username] returns also sn metadata = 12 characters (hex) long serial number of KV that has been backed up
- WebAuthn authenticatorUserVerification setting set to: required
- added custom username support. Users can register with their own username and backups are stored accordingly
- backup operation no longer requires user presence verification

### v0.1.2
- KV fw 2.4.9 is now required - KV is now tested and fully working on MacOS Bigsur in FIDO2 mode (fingerprints, pin code)
- fixed multiple commands being sent in allowCredentials in backup/restore operations
- improved restore flow documenation in README.md

### v0.1.1
- added logging of API calls
- logs are now captured also in server.log file
- removed unused libraries, added 'os' lib for os values detection

### v0.1.0
- first fully working version with all the main flows (webauthn register, webauthn login, keyvault backup, keyvault restore) implemented. See README.md for more details.
- KeyVault fw requirement: **2.4.6** - due to major changes in restore flow
- replaced FIDO2 NodeJs library 'simplewebauthn' with 'fido2-library'
- added js frontend libraries: webauthn-simple-app, ux-events, bootstrap
- new API calls GET /backup/exists and POST /backup/user
- express-sessions implemented
- **Crayonic Gateway App Server** frontend demo available at: https://crayonic.io/app/


