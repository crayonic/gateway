//
// core functions/helper functions
//
// const fs = require('fs');

module.exports = {

    log : function(txt) {
      console.log(module.exports.getDate() + txt);
    //   fs.writeFileSync("server.log", txt);
    },
  
    error : function(txt, optTxt = null) {
      console.error(module.exports.getDate() + '!! ' + global.APP_PATH + ' , ' + txt, optTxt);
    },
  
    getDate : function() {
      let now = new Date();
      
      let dd = now.getDate();
      if (dd<10) dd = '0' + dd
      let mm = now.getMonth() + 1
      if (mm<10) mm = '0' + mm;
      let hh = now.getHours();
      if (hh<10) hh = '0' + hh;
      let nn = now.getMinutes();
      if (nn<10) nn = '0' + nn;
      let ss = now.getSeconds();
      if (ss<10) ss = '0' + ss;
    
      return (`[${now.getFullYear()}-${mm}-${dd} ${hh}:${nn}:${ss}] `);
    },

    //
    // function returns 6chars long string representation of payload length in hex
    //
    // input: payload length (int)
    // 
    dec2hex : function (decimal) {
      const chars = 6;  // 000000
      return (decimal + Math.pow(16, chars)).toString(16).slice(-chars); // .toUpperCase()
    },

    dec2smallhex : function (decimal) {
      const chars = 2;  // 00
      return (decimal + Math.pow(16, chars)).toString(16).slice(-chars); // .toUpperCase()
    },

    //
    // function converts hex string to dec
    //
    // input: hex number (int)
    // 
    hex2dec : function (hexString) {
      return parseInt(hexString, 16);
    },


    // https://tomeko.net/online_tools/hex_to_base64.php
    //
    // var base64String = Buffer.from(hexString, 'hex').toString('base64')
    // or
    // var hexString = Buffer.from(base64String, 'base64').toString('hex')
    //
    hex2bin : function (hex) {
      return Buffer.from(hex, 'hex');
    },

// function hexToBase64(hexstring) {
//   return btoa(hexstring.match(/\w{2}/g).map(function(a) {
//       return String.fromCharCode(parseInt(a, 16));
//   }).join(""));
// }

    hex2ascii : function(str1) {
      let hex  = str1.toString();
      let str = '';
      for (let n = 0; n < hex.length; n += 2) {
        str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
      }
      return str;
    },
   
    ascii2hex : function (str) {
      let arr1 = [];
      for (let n = 0, l = str.length; n < l; n ++) {
        let hex = Number(str.charCodeAt(n)).toString(16);
        arr1.push(hex);
      }
      return arr1.join('');
    },
    
    isValidV4UUID : function (uuid) {
      const uuidV4Regex = /^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i;
      return uuidV4Regex.test(uuid);
    }

}
  