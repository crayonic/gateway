//
// db libs
//

const createConnectionPool = require('@databases/pg');

//
// make sure to put this value as ENV in PROD - no config when creating new pool is then necessary
//

// debug:
// const CGWDB_ENV_VAR = 'postgres://gateway@localhost:5432/dbCGW';
// DEV config:
// localhost (0.0.0.0)
// prod:
// const CGWDB_ENV_VAR  = process.env.CGWDB_ENV_VAR;

// host: 'localhost' in PROD, 'crayonic.io' in DEV, 'gateway-db' in docker-compose
const CGWDB_ENV_VAR = {
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST,  
  database: process.env.DB_DB,
  port: process.env.DB_PORT,
  bigIntMode: 'bigint'
};



const db = createConnectionPool(CGWDB_ENV_VAR);
// console.log('db connected');

module.exports = db;

// const { Client } = require('pg');
// const client = new Client();
// client
//   .connect()
//   .then(() => console.log('db connected'))
//   .catch(err => console.error('db connection error', err.stack));

