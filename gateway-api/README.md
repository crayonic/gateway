# Crayonic Gateway Backup/Restore process 

This document describes Crayonic Gateway architecture, entities and one of the main features of the Gateway: **Backup/Restore** flows for FIDO2 HW authenticator **Crayonic KeyVault**.

Installation and build instructions for Crayonic Gateway API server (CGW-API) is also provided.

## Entities

This section describes current demo/test setup of Crayonic Gateway located at crayonic.io. It's possible to change/tweak the configuration for other locations as well.

- **(GW-AS) Gateway IAM server:**
    - Crayonic Gateway instance (based on RedHat Keycloak): docker image "gateway", running in AWS at crayonic.io
    - URL: admin console: https://crayonic.io/auth/admin/master/console/
    - URL: end-user entrypoint: https://crayonic.io/auth/realms/gateway/account/
    - configuration for user federeration, LDAP, SAML2, OpenID
- **(DB) User DB:** 
    - (GW-DB1) GW-AS internal user storage. Can be primary user DB in case AD/LDAP is not configured.
    - (AD-DB2) Active Directory - Optional. Configured in Gateway (user federation) and synced to GW-DB1 (one way). Acts as a primary user DB when used.
    - (AS-DB3) PostgreSQL configured in Dockerfile - used for KeyVault backups
- **(CGW-API) Crayonic Gateway API Server:**
    - This NodeJS application running at port 3000, mapped to 443 (SSL), can run as a docker image or in Kubernetes cluster
    - provides API for Crayonic KeyVault device functionalities: backup/restore/policies/credentials/resident key management
- **(KV) Keyvault:**
    - Crayonic FIDO2 next generation multifactor biometric HW authenticator. **Firmware 2.4.9** or higher is required for backup/restore operations.

## Flow description

### Registration

Note: process starts automatically during first login to GW-AS. Links provided below are for installation at crayonic.io with realm 'gateway'.

1. user logs in at https://crayonic.io/auth/realms/gateway/account/, provides username (user must already exist in GW-AS)
2. GW-AS calls CGW-API: GET /backup/exists, checks whether user already has KV backup stored, if yes, **Restore** flow starts (see below)
3. GW-AS starts webauthn attestation process
4. user confirms registration on KV display with fingerprint/user presence
5. KV authenticator credentials are stored in GW-DB1 and user is logged in the system
6. GW-AS calls Node-AS API: POST /backup/user, which adds new user to AS-DB3. KV backup is initialised (empty).

### Login

Note: KV metadata is backed up to AS-DB3 during every login.

1. user logs in GW-AS at https://crayonic.io/auth/realms/gateway/account/ via passwordless/usernameless login.
2. GW-AS starts webauthn assertion process
3. user confirms login on KV display with fingerprint/user presence
4. KV authenticator credentials are verified with GW-DB1 and user is logged in the system
5. Process continues in **Backup** flow

### Backup

Note: this process is started automatically during **Login** flow. For this process to work, user must be authenticated and must log in the system at least once. 

1. GW-AS calls CGW-API: POST /backup/options, which executes  webauthn assertion operation with backup command (APDU formatted A1 command for all resident keys) and userVerification='discouraged'. 

```js
navigator.credentials.get({
	allowCredentials: { id: <binary> FFA10000000000 },
	userVerification: 'discouraged'
})
```

2. KV returns backup payload in signature field. If the operation is successful, the signature ends with **0x9000**. To get final backup payload, we need to remove 0x9000 from the end of the returned string. Example:
```js

{
    "rawId": 
    "id": 
    "response": {
        "authenticatorData":
        "signature": "6d00607223d5f87802c060577f6eb3bef0ab1366a4f0315e4b92b33ec41153d32b63dcb04fa19a4f389db2536deae8dbda61ae4a7614f285adff133f3895f3c48d2c447aa64fbd867c8e7044002139b54a3f88b444fc6973da82e0f765a21b8dc43553630004000001357300043f020000720001009000"
        "userHandle":
        "clientDataJSON": 
    },
    "getClientExtensionResults": {},
    "type": "public-key"
}
```

3. Backup payload is stored in AS-DB3 with some additional information like timestamp of the backup operation and KV serial number

**Backup payload definition:**
**KV:** backup serialization for stored resident keys (TLV format, ascii characters= tags ('C','I','N','D','O','R'):  C : credential (binary blob), I : user id, N : user name, D : display name, O : icon (not used), R = relying party
**Node-AS:** backup representation:  userId: [ { payload: string (sanitized data from signature field), timestamp: string (contains GMT timestamp), serialNumber: string (KV serial number)} ]


### Restore

**Note:** this process can be started automatically during **Registration** flow in production. For this process to work, user must have existing backup in AS-DB3, which means also that regular **Registration** and **Login** flows have been successfuly executed. 

**WARNING:** After succesfully completing restore operation, the information (e.g. resident keys) on your KV will be lost and replaced by the backup content.

1. GW-AS calls Node-AS API: POST /restore/options, which executes webauthn assertion operation with restore command (APDU formatted A2 command). 
2. Node-AS checks whether backup exists for a given user to prevent lock out situation. Restore fails (no changes on KV device) when there is no backup to work with.
3. KV asks user to confirm the restore operation by providing fingerprint/PIN code
4. Node-AS builds APDU restore header, which consists of 1) APDU restore command ('FFA20000'), 2) payload length = 3 bytes in hex format, 3) payload itself. Example:

```js
navigator.credentials.get({
    allowCredentials: { 
        id: <binary> "ffa2000000006a6d006035c067065d723203b4b8afcc094c3edcb618c4f0457e4103e22248f8ad10e175b376de386947e6afc91277ab18411502872125df732e8fd6ff98356433b3f9fc9703cd7f51e46c02b11c5daa16ea7a1173a793bdf7888c942fb9b859fade4a0563000400000e24",
        type: 'public-key'
    },
	userVerification: 'discouraged'
})

Payload length is 0x00006a = 106 bytes.
```

5. KV diplays on its display "RESTORE" and asks user to make final confirmation by pressing KV button. 
6. Once the restore is completed, KV provides operation status in “response”: { response.signature }. If everything went well, KV is restored with previously backed-up metadata and success code **0x9000** is returned. Example:

```js
response: {
    response.signature: <binary> 9000 (success)
}
```


# Crayonic Gateway Application Server (Node-AS)

- Web frontend (js) + Node.js backend to test FIDO2/Webauthn with Crayonic Keyvault (KV)
- KV firmware v2.4.9 or later required
- Logs are displayed in console
- HTTPS is required (even on localhost)
- two configurations are available: DEV/PROD

Following processes/flows are implemented:
- backup
- restore
- policies
- credentials (resident keys management)


**Note:** KeyVault device (firmware version 2.4.9) now fully supports MacOS BigSur via FIDO2 authentication flow (fingerprints/pin code).

## Backend calls:

To be added



## Installation

### localhost (DEV)
- make sure localhost.crt + localhost.key are in the app folder. See intstructions below on how to create local crt+key files.
- clone or copy project files to desired app location = <app_dir> (e.g. /gatewayApp)
- cd <app_dir>
- **npm install**
- **npm run local**
- visit https://localhost/cgw/

### online (PROD)
#### prerequisites (bare metal)
- make sure nginx is redirecting 0.0.0.0:3000 to node app (e.g. in /etc/nginx/conf.d/default.conf)
- make sure nginx is redirecting all URLs starting with this prefix to node app: /cgw/
```
location ~ ^/(cgw) {
    proxy_pass https://0.0.0.0:3000;
    proxy_set_header X-Forwarded-For $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection ‘upgrade’;
    proxy_set_header Host $host;
}
```
- reload nginx and confirm it's active and running
```
sudo systemctl reload nginx
sudo systemctl status nginx
```
- copy certificate files (tls.crt + tls.key) to the app root folder (e.g. /home/ubuntu/gatewayApp)

#### Bare metal installation/update
- ssh -i "your-pem-key.pem" ubuntu@ec2-54-224-207-171.compute-1.amazonaws.com (or your site)
- check whether app is already running: **netstat -nptl**. If yes, kill the process: **kill -9 PID** or do **pkill node** which kills existing node process
- git clone or copy project files to desired app location = <app_dir> (in our case /home/ubuntu/gatewayApp)
- cd <app_dir>
- copy content of /app/ (web frontend) folder to nginx server html location (in our case: /usr/share/nginx/html/app/)
- **npm install**
- **npm start**

#### Docker installation

localhost build and run (don't forget to set correct -e parameters)
```
docker build --no-cache --pull -t gateway-api:0.2.8 .

docker run --rm -p 3000:3000 -e DB_USER=gateway -e DB_HOST=localhost -e DB_PASS=pass -e DB_DB=dbcgw -e DB_PORT=5432 --name gateway-api gateway-api:0.2.8
```

Prod build/run:
```
docker buildx ls
docker buildx create --name mybuilder --use
docker buildx build -t crayonic/gateway-api:0.2.8 --platform linux/amd64,linux/arm64,linux/arm/v7 --push .

docker run --rm -p 3000:3000 -e DB_USER=gateway -e DB_HOST=localhost -e DB_PASS=pass -e DB_DB=dbcgw -e DB_PORT=5432 --name gateway-api gateway-api:0.2.8
```

localhost shell access (if needed):
```
docker exec -it gateway-api sh
```

check logs:
```
#get container id: docker ps
docker logs container_id > server.log
```



### Setting up HTTPS support locally (localhost)

Websites that want to use WebAuthn must be served over HTTPS, including during development! Fortunately it's simple to generate SSL certificates to host the site locally over HTTPS:
1. Install mkcert as per its instructions - https://github.com/FiloSottile/mkcert#installation
2. Run mkcert -install to initialize mkcert
3. Run the following command from within the example/ directory to generate SSL certificates for localhost:

```
mkcert -key-file localhost.key -cert-file localhost.crt localhost
```

This command should generate the following two files:
* example/localhost.key
* example/localhost.crt
The SSL certificate has been successfully generated if you see these two files. If these files don't exist, make sure you're in the example/ directory before retrying the last step.
