# Crayonic Gateway (CGW)

Identity and Access Management server based on Redhat keycloak.org specifically tailored to take advantage of features built in Crayonic KeyVault FIDO2 device - like Keyvault backup+restore, Keyvault settings, Policy management and X509 Certificate management.

This repo contains `docker-compose.yml`, config files and themes that are required to build and run Crayonic Gateway (CGW) docker image on your localhost or cloud machine.

Currently supported Redhat Keycloak version = **15.0.2**
Currently supported **minimum** Crayonic KeyVault firmware version: **0.7.0** or higher.

This readme describes how to install and run Crayonic Gateway on a your machine.

## Overview

For localhost, Gateway runs on HTTP, port 8080. To set up https, it is recommended to configure reverse proxy, e.g. via Nginx.

Files in repo folder **/js/** (jquery.min.js, account.js, register.js, login.js, etc.) are hosted at crayonic.io by default, but in case they are required to be downloaded from local/dedicated server, links can be updated in these files:
- `/themes/gateway/account/index.ftl`
- `/themes/gateway11/account/account.ftl`
- `/themes/gateway11/login/webauthn-register.ftl`
- `/themes/gateway11/login/login-username.ftl`

Two new CGW themes are provided in themes folder (/themes/)
- `gateway11` - keycloak v11.x user account console (/themes/gateway11/account) and passwordless login flow (/themes/gateway11/login)
- `gateway` - new keycloak v12.x and later user account console written in react.js enhanced with Crayonic KeyVault features - (/themes/gateway/account)


### Setup gateway

Change/update following sections in **docker-compose.yml** with your settings (access credentials, logging):
- services/postgres
- services/gateway-local
- services/persephone
- services/gateway-api

You can also experiment with **realm-export.json**. Included realm-export.json is configured to create a realm called 'gateway' and which enables passwordless webauthn experience and contains several demo apps/clients like AWS, Okta, Google Apps.

Gateway API image can be downloaded from docker hub:
```
docker pull crayonic/gateway-api:0.2.7
```

or in case building the image, should be downloaded from gitlab repository and located in a separate folder "../gatewayApp/"

This can be configured in **docker-compose.yml**.


### Build and Run gateway

```
docker-compose up --force-recreate --build
```

This will start following services:
- `gateway-db` - PostgreSQL server
- `gateway-local` - Gateway server (enhanced keycloak)
- `gateway-api` - API backend (backup/restore, policies, credential management, cert management)
- `gateway-persephone` - X509 Certification sync backend


### Updating themes

By default, docker-compose.yml is configured to enable making changes in templates (themes) on the fly (while Gateway is running) using volume "./themes/:/opt/jboss/keycloak/themes/". Note that any changes in /base/ (login/authentication, etc.) template folder are not refreshed on the fly. Server builds and caches this content during start-up sequence, which means it needs to be restarted (**ctrl + c** in the console to stop, arrow up and enter to run docker-compose command again). 

### Modifying default user account console

If you are modifying user account console theme (/themes/gateway/account), it's required to rebuild react.js code before running docker-compose. The following steps will build the account console app and copy js to `themes/gateway/account/resources` and `themes/keycloak/common/resources/web_modules`. Use yarn, npm is not supported


```
cd [your gateway folder]
cd ./themes/gateway/account_new
yarn
yarn build
cd ../../..
docker-compose up --force-recreate --build
```

### Accessing Gateway Admin Console

After runing docker-compose, wait around 1 minute for the server to spin up and then open
```
localhost:8080/auth/admin/
```
in your browser to login to admin console. See logs in your console for more detailed information. Ctrl+c in the console gracefully stops the server.

### Access Gateway image shell (jboss shell)
```
docker exec -it gateway-local sh
```

### Export gateway logs from container
```
docker logs gateway-local > gateway-local.log
docker logs gateway-api > gateway-api.log
```

### Build/update gateway docker image (CGW versions prior to 0.2.0):
```
cd [your gateway-local folder]
docker stop gateway-local
docker rmi gateway-local (when rebuilding new image)
docker build -t gateway-local:latest .
```

### What to do after AWS EC2 ‘ubuntu-gateway’ instance fails and is restarted?

Note: This memo is not applicable for general audience.

all Gateway services start automatically after server reboot. To doublecheck, feel free to navigate to AWS Cloudwatch/Log Groups/docker-logs/

Optionally, check services in terminal:

1) log in to instance using ssh:
open terminal, log in to AWS server:
```
ssh -i “/path-to-your-private-key-file/crayonic_io.pem" ubuntu@ec2-54-224-207-171.compute-1.amazonaws.com
```
2) to see whether all services are running correctly:
```
docker ps
```
You should see 4 containers active (gateway-db, gateway-local, gateway-persephone, gateway-api).

3) Next check 
```
netstat -tulpn
```
Apart from usual ports 80/443/22, you should see Gateway-lite (not part of Crayonic Gateway installation) NodeJS process running at port 3001 (CGW Lite). Also Redis server listening on port 6379 (session cache for CGW Lite).
```
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:35511         0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:3001            0.0.0.0:*               LISTEN      32164/node          
tcp        0      0 0.0.0.0:443             0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:6379          0.0.0.0:*               LISTEN      - 
```