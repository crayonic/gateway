FROM jboss/keycloak:15.0.2
LABEL maintainer="Marek Chorvat <mchorvat@crayonic.com>" version="20220708-01"

# change in PROD:
#ARG HOST=crayonic.io
ENV KEYCLOAK_USER="admin"
ENV KEYCLOAK_PASSWORD="admin"

#ENV KEYCLOAK_FRONTEND_URL="https://crayonic.io"

ENV KEYCLOAK_WELCOME_THEME="gateway"
ENV KEYCLOAK_DEFAULT_THEME="gateway"

ENV PROXY_ADDRESS_FORWARDING="true"

ENV KEYCLOAK_HOME="/opt/jboss/keycloak"
ENV KEYCLOAK_IMPORT="/tmp/realm-export-gateway.json"

# default mem config was 512kb - this leads to crashes
ENV JAVA_OPTS="-Xms2048m -Xmx2048m -Xss16m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
ENV JAVA_OPTS_APPEND="-Dkeycloak.profile=preview -Dkeycloak.profile.feature.upload_script=enabled -Dkeycloak.profile.feature.token_exchange=enabled"

USER root

# CVE-2022-21449 issue fix
RUN microdnf upgrade java-11-openjdk-headless

#WORKDIR /opt/jboss/keycloak/standalone/configuration/
COPY --chown=jboss standalone.xml /opt/jboss/keycloak/standalone/configuration/
COPY --chown=jboss standalone-ha.xml /opt/jboss/keycloak/standalone/configuration/

# initial config for 'gateway' realm
COPY --chown=jboss realm-export-gateway.json /tmp/

#WORKDIR /opt/jboss/keycloak/themes/
COPY --chown=jboss /themes/ /opt/jboss/keycloak/themes/

# update for admin console react component
COPY --chown=jboss ./react_component/ /opt/jboss/keycloak/themes/base/admin/resources/lib/react_component/

# health check plugin installation. Module available at: /auth/realms/master/health/check
# https://github.com/thomasdarimont/keycloak-health-checks/tree/15.0.2.0
COPY --chown=jboss ./health-checks/ /opt/jboss/keycloak/modules/com/github/thomasdarimont/keycloak/extensions/keycloak-health-checks/main/


WORKDIR /opt/jboss/keycloak/
USER jboss


### localhost build:
# (optional) docker stop gateway-local
# docker rmi gateway-local
# docker build --no-cache --pull -t gateway-local:15.0.2 .
# docker build --no-cache --pull -t gateway-local:latest .

### prod build:
# amd64 image: docker buildx build -t crayonic/crayonic-gateway:20220708-01 --platform linux/amd64 --push .
# GCR: docker buildx build -t eu.gcr.io/crayonic-gateway/gateway:20220708-01 --platform linux/amd64 --push .
# all linux platforms: docker buildx build -t crayonic/crayonic-gateway:20220708-01 --platform linux/amd64,linux/arm64,linux/arm/v7 --push .


### localhost run (using ENV parameters defined earlier)
# docker run --rm -p 8080:8080 --name gateway-local gateway-local
# or for full debug output
# docker run --rm -p 8080:8080 --name gateway-local -e KEYCLOAK_LOGLEVEL=DEBUG gateway-local

###
### optional run with extra params:
###

### change path in /v param to map your local folder to a docker image
# docker run --rm -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT=/tmp/realm-export-gateway.json --name gateway-local -v /Users/marek/Documents/Code/gateway-local/themes/:/opt/jboss/keycloak/themes/ gateway-local
### or:
# docker run --rm -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT=/tmp/realm-export-gateway.json --name gateway-local gateway-local

### no config import+no -v option:
# docker run --rm -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin --name gateway-local -v /Users/marek/Documents/Code/gateway-local/themes/:/opt/jboss/keycloak/themes/ gateway-local
# docker run --rm -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin --name gateway-local gateway-local

### or (in case server starts fails due to restricted script upload):
# docker run --rm -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT="/tmp/realm-export-gateway.json -Dkeycloak.profile.feature.upload_scripts=enabled" --name gateway-local gateway-local

### localhost shell access (if needed):
# docker exec -it gateway-local sh

### adding users:
# edit /opt/jboss/keycloak/standalone/configuration/keycloak-add-user.json

### localhost realm export:
### use export realm feature in admin gui or this script:
# bin/standalone.sh -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=/tmp/realm-export-gateway.json -Dkeycloak.migration.realmName=gateway
