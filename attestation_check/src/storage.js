  const makeCredentialsOptions = (payload) => ({
    publicKey: {
      challenge: new Uint8Array([
        255,
        24,
        31,
        198,
        82,
        136,
        187,
        6,
        153,
        155,
        8,
        39,
        200,
        33,
        59,
        95,
        194,
        98,
        190,
        225,
        187,
        59,
        153,
        191,
        129,
        202,
        152,
        2,
        51,
        97,
        164,
        164
      ]),
      rp: {
        name: window.location.hostname,
        id: window.location.hostname
      },
      user: {
        name: "bitwarden",
        displayName: "bitwarden",
        id: new Uint8Array([239, 181, 9, 0, 0, 0, 0, 0, 0, 0])
      },
      pubKeyCredParams: [
        {
          type: "public-key",
          alg: -7
        },
        {
          type: "public-key",
          alg: -35
        },
        {
          type: "public-key",
          alg: -36
        },
        {
          type: "public-key",
          alg: -257
        },
        {
          type: "public-key",
          alg: -258
        },
        {
          type: "public-key",
          alg: -259
        },
        {
          type: "public-key",
          alg: -37
        },
        {
          type: "public-key",
          alg: -38
        },
        {
          type: "public-key",
          alg: -39
        },
        {
          type: "public-key",
          alg: -8
        }
      ],
      authenticatorSelection: {
        authenticatorAttachment: "cross-platform",
        requireResidentKey: false,
        userVerification: "discouraged"
      },
      timeout: 60000,
      extensions: {
        txAuthSimple: ""
      },
      attestation: "indirect"
      // excludeCredentials: [
      //   {
      //     type: "public-key",
      //     id: payload
      //   }
      // ]
    }
  });
  
  const getAssertionOptions = (payload) => ({
    publicKey: {
      challenge: new Uint8Array([
        255,
        24,
        31,
        198,
        82,
        136,
        187,
        6,
        153,
        155,
        8,
        39,
        200,
        33,
        59,
        95,
        194,
        98,
        190,
        225,
        187,
        59,
        153,
        191,
        129,
        202,
        152,
        2,
        51,
        97,
        164,
        164
      ]),
      timeout: 60000,
      rpId: window.location.hostname,
      allowCredentials: [
        {
          type: "public-key",
          id: payload
        }
      ],
      extensions: {
        txAuthSimple: ""
      }
    }
  });
  

  module.exports = {
    getAssertionOptions,
    makeCredentialsOptions
  }