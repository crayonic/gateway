const cbor = require("cbor");
const x509 = require("@peculiar/x509");

const storage = require("./storage");
const attestation = require("./attestation");

function der2pem(der) {
  var prefix = "-----BEGIN CERTIFICATE-----\n";
  var postfix = "-----END CERTIFICATE-----";
  var derBuffer = new ArrayBuffer(der.length);
  der.map(function (value, i) {
    derBuffer[i] = value;
  });
  console.log("derBuffer", derBuffer);
  var b64encoded = btoa(
    String.fromCharCode.apply(null, new Uint8Array(derBuffer[0]))
  );
  // console.log("b64encoded", b64encoded);
  var pemText = prefix + b64encoded.match(/.{0,64}/g).join("\n") + postfix;
  // console.log("pemText", pemText);
  return pemText;
}

function uint8array2hex(buffer) {
  // buffer is an ArrayBuffer
  return Array.prototype.map
    .call(buffer, (x) => ("00" + x.toString(16)).slice(-2))
    .join("");
}

function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
}

function str2ab(str) {
  // var s = encode_utf8(str);
  var s = str;
  var buf = new ArrayBuffer(s.length);
  var bufView = new Uint8Array(buf);
  for (var i = 0, strLen = s.length; i < strLen; i++) {
    bufView[i] = s.charCodeAt(i);
  }
  return bufView;
}

function b64decode(str) {
  return str2ab(atob(str.replace(/-/g, "/").replace(/_/g, "+")));
}

function buf2hex(buffer) {
  // buffer is an ArrayBuffer
  return Array.prototype.map
    .call(new Uint8Array(buffer), (x) => ("00" + x.toString(16)).slice(-2))
    .join("");
}

async function testIndirectAttestation(result) {
    try {
        // this.setState({ result: null });
        const options = storage.makeCredentialsOptions("1234567890");
        console.log("navigator.credentials.create() with options:", options);
        if (!result) {
            const result = await navigator.credentials.create(options);
        }
        console.log("result", result);
        console.log("verifyPackedAttestation", attestation.verifyPackedAttestation(result));
        // console.log(
        //   "attestationObject",
        //   buf2hex(result.response.attestationObject)
        // );
        const attestationObject = cbor.decode(result.response.attestationObject);
        console.log("attestationObject", attestationObject);
        const clientDataJSON = JSON.parse(ab2str(result.response.clientDataJSON));
        console.log("clientDataJSON", clientDataJSON);
        const attestationCertificate = new x509.X509Certificate(
        der2pem(attestationObject.attStmt.x5c)
        );
        console.log("attestationCertificate", attestationCertificate);
        // this.setState({
        // result: JSON.stringify(attestationCertificate, null, 4)
        // });
        // console.log("signature", buf2hex(attestationCertificate.signature));

        const attestationChallenge = b64decode(clientDataJSON.challenge);
        console.log("attestationChallenge", buf2hex(attestationChallenge));
        const attestationPublicKey = attestationCertificate.publicKey.rawData.slice(
        -65
        );
        console.log("attestationPublicKey", buf2hex(attestationPublicKey));
        const attestationSignature = attestationObject.attStmt.sig;
        console.log("attestationSignature", buf2hex(attestationSignature));

        const valid = attestation.verifyPackedAttestation(result);
            
        const attestationValidMessage =
        "Attestation signature " +
        (valid ? "valid" : "invalid ") +
        ", ";

        if (
        buf2hex(attestationPublicKey) ===
        "040c9f670900aace92cc6d046259ad86ce44d96f135ae3da07a332976e4bb2d41d289ac1ca7f3bbe618435cbe8658a045bebda6b09077483b61b7ef2611811603c"
        ) {
            console.log("MDS 2 certificate");
            // alert(attestationValidMessage + "MDS 2 certificate");
            return {
                valid,
                mds_certificate: "MDS 2 certificate",
                message: attestationValidMessage + "MDS 2 certificate"
            }
        } else if (
        buf2hex(attestationPublicKey) ===
        "04113620f438c7d8543c46e824fe4888f73b18f1f4310ea96c1f89fdf1f2d3db7184a32ab2534a314858cf8f500292a2b48ea6c0e8340077630f46356d0fac648f"
        ) {
            console.log("MDS 3 certificate");
            // alert(attestationValidMessage + "MDS 3 certificate");
            return {
                valid,
                mds_certificate: "MDS 3 certificate",
                message: attestationValidMessage + "MDS 3 certificate"
            }
        } else {
            console.log("Unknown certificate");
            // alert(attestationValidMessage + "Unknown certificate");
            return {
                valid,
                mds_certificate: "Unknown certificate",
                message: attestationValidMessage + "Unknown certificate"
            }
        }
    } catch (error) {
        console.log("error", error);
        throw error
        // this.setState({ result: error.toString() });
    }
};

module.exports = {
    testIndirectAttestation
}