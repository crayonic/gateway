#!/bin/sh

echo "Replacing https://crayonic.io/cg/ with $CG_ROOT"
find /usr/src/app -type f -exec sed -i "s;https://crayonic.io/cg/;$CG_ROOT;g" {} +

# exec CMD
exec "$@"
