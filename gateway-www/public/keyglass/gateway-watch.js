const registerButton = document.querySelector('#register-button');
const authenticateButton = document.querySelector('#authenticate-button');
const deleteButton = document.querySelector('#delete-button');
const loader = document.querySelector('#loader');
const authDialog = document.querySelector('#auth-dialog');
const dialogBody = authDialog.querySelector('[slot="body"]');
const closeButton = document.querySelector('#close-dialog');

closeButton.addEventListener('click', () => {
  authDialog.close();
});

const bufferToBase64 = buffer => btoa(String.fromCharCode(...new Uint8Array(buffer)));
const base64ToBuffer = base64 => Uint8Array.from(atob(base64), c => c.charCodeAt(0));

const removeCredential = () => {
  localStorage.removeItem('credentialWatch');
  deleteButton.style.display = 'none';
  authenticateButton.style.display = 'none';
  registerButton.style.display = 'block';
};

// const apiUrl = 'https://localhost:3005/wa';
const apiUrl = 'https://crayonic.io/wa';
const cgwApiUrl = 'https://crayonic.io/cgw'; // CGW api (userhandle endpoints used)
const realm = 'w'; // watch realm

const register = async () => {
  registerButton.disabled = true;
  loader.style.display = 'block';

  try {
    const credentialCreationOptions = await (await fetch(`${apiUrl}/register-options`, {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })).json();

    credentialCreationOptions.challenge = new Uint8Array(credentialCreationOptions.challenge.data);
    credentialCreationOptions.user.id = new Uint8Array(credentialCreationOptions.user.id.data);
    credentialCreationOptions.user.name = 'WebAuthn Watch Demo';
    credentialCreationOptions.user.displayName = 'WebAuthn Watch Demo';
    credentialCreationOptions.rp.id = window.location.hostname;

    const credential = await navigator.credentials.create({
      publicKey: credentialCreationOptions
    });

    attestation_check_result = await attestation_check.testIndirectAttestation(credential);
    console.log("verifyPackedAttestation", attestation_check_result);

    const credentialId = bufferToBase64(credential.rawId);

    const data = {
      rawId: credentialId,
      response: {
        attestationObject: bufferToBase64(credential.response.attestationObject),
        clientDataJSON: bufferToBase64(credential.response.clientDataJSON),
        id: credential.id,
        type: credential.type
      }
    };

    await (await fetch(`${apiUrl}/register`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({credential: data}),
      credentials: 'include'
    })).json();

    registerButton.style.display = 'none';
    authenticateButton.style.display = 'block';
    deleteButton.style.display = 'block';

    const uuid = uuidv4();

    // console.log(JSON.stringify(data));

    await (await fetch(`${cgwApiUrl}/credentials/userhandle`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({realm: realm, username: uuid, userhandle: credentialId}),
      credentials: 'include'
    })).json();

    localStorage.setItem('credentialWatch', JSON.stringify({credentialId}));

    // dialogBody.innerHTML = 'Registration successful!';
    dialogBody.innerHTML = `Registration successful - your userid is: ${uuid}, your userhandle is: ${credentialId}<br /><br />${attestation_check_result.message}`;

    authDialog.open();
  }
  catch(e) {
    console.error('registration failed', e);

    dialogBody.innerHTML = 'Registration failed';
    authDialog.open();
  }
  finally {
    registerButton.disabled = false;
    loader.style.display = 'none';
  }
};

const authenticate = async () => {
  authenticateButton.disabled = true;
  deleteButton.disabled = true;
  loader.style.display = 'block';

  try {
    const credentialRequestOptions = await (await fetch(`${apiUrl}/authenticate-options`, {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })).json();

    const {credentialId} = JSON.parse(localStorage.getItem('credentialWatch'));

    credentialRequestOptions.challenge = new Uint8Array(credentialRequestOptions.challenge.data);
    credentialRequestOptions.allowCredentials = [
      {
        id: base64ToBuffer(credentialId),
        type: 'public-key',
        // transports: ['internal']
        transports: ["usb", "nfc", "ble"]        
      }
    ];

    // console.log(`Auth credentialRequestOptions: ${JSON.stringify(credentialRequestOptions)}`);

    const credential = await navigator.credentials.get({
      publicKey: credentialRequestOptions
    });

    const data = {
      rawId: bufferToBase64(credential.rawId),
      response: {
        authenticatorData: bufferToBase64(credential.response.authenticatorData),
        signature: bufferToBase64(credential.response.signature),
        userHandle: bufferToBase64(credential.response.userHandle),
        clientDataJSON: bufferToBase64(credential.response.clientDataJSON),
        id: credential.id,
        type: credential.type
      }
    };

    console.log(`Authenticating with credential: ${data.rawId}`);
    console.log(`Auth data: ${JSON.stringify(data)}`);

    const response = (await fetch(`${apiUrl}/authenticate`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({credential: data}),
      credentials: 'include'
    }));

    console.log(`Auth response: ${JSON.stringify(response)}`);

    if(response.status === 404) {
      dialogBody.innerHTML = 'Credential has expired, please register a new credential';

      authDialog.open();
      // removeCredential();
    }
    else {
      const assertionResponse = await response.json();

      console.log(JSON.stringify(assertionResponse));

      let resp;

      // await (resp = await fetch(`${cgwApiUrl}/credentials/userhandle?realm=${realm}&userhandle=${data.rawId}`, {
      //   method: 'GET',
      //   mode: 'cors',
      //   headers: {
      //     'Content-Type': 'application/json'
      //   },
      //   // body: JSON.stringify({realm: 'w', username: uuid, userhandle: data.response.id}),
      //   credentials: 'include'
      // })).json();

      resp = await fetch(`${cgwApiUrl}/credentials/userhandle?realm=${realm}&userhandle=${data.rawId}`);      
      console.log(JSON.stringify(resp));

      dialogBody.innerHTML = `Authentication successful - your userid is: ${resp.username}`;
      authDialog.open();
    }
  }
  catch(e) {
    console.error('authentication failed', e);

    dialogBody.innerHTML = 'Authentication failed';
    authDialog.open();
  }
  finally {
    authenticateButton.disabled = false;
    deleteButton.disabled = false;
    loader.style.display = 'none';
  }
};

const hasCredential = localStorage.getItem('credentialWatch') !== null;

if(hasCredential) {
  authenticateButton.style.display = 'block';
  deleteButton.style.display = 'block';
}
else {
  registerButton.style.display = 'block';
}

registerButton.addEventListener('click', register);
authenticateButton.addEventListener('click', authenticate);
deleteButton.addEventListener('click', removeCredential);

//
// watch special functions
//
function uuidv4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}
