const registerButton = document.querySelector('#register-button');
const loader = document.querySelector('#loader');

const bufferToBase64 = buffer => btoa(String.fromCharCode(...new Uint8Array(buffer)));
const base64ToBuffer = base64 => Uint8Array.from(atob(base64), c => c.charCodeAt(0));

const removeCredential = () => {
  localStorage.removeItem('credentialKeyGlass');
  registerButton.style.display = 'block';
};

// const apiUrl = 'https://localhost:3005/wa';
// const apiUrl = 'https://crayonic.io/wa';
const apiUrl = '/wa';

const register = async () => {
  registerButton.disabled = true;
  loader.style.display = 'block';

  try {
    const credentialCreationOptions = await (await fetch(`${apiUrl}/register-options`, {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })).json();

    credentialCreationOptions.challenge = new Uint8Array(credentialCreationOptions.challenge.data);
    credentialCreationOptions.user.id = new Uint8Array(credentialCreationOptions.user.id.data);
    credentialCreationOptions.user.name = 'KeyGlass Demo User';
    credentialCreationOptions.user.displayName = 'F4ux2ptw8bNuYGutTmIOB';
    credentialCreationOptions.rp.id = window.location.hostname;
    credentialCreationOptions.authenticatorSelection.userVerification = 'preferred';
    // credentialCreationOptions.authenticatorSelection.authenticatorAttachment = 'cross-platform';

    const credential = await navigator.credentials.create({
      publicKey: credentialCreationOptions
    });

    const credentialId = bufferToBase64(credential.rawId);

    const data = {
      rawId: credentialId,
      response: {
        attestationObject: bufferToBase64(credential.response.attestationObject),
        clientDataJSON: bufferToBase64(credential.response.clientDataJSON),
        id: credential.id,
        type: credential.type
      }
    };

    await (await fetch(`${apiUrl}/register`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({credential: data}),
      credentials: 'include'
    })).json();

    registerButton.style.display = 'none';

    localStorage.setItem('credentialKeyGlass', JSON.stringify({credentialId}));

    // dialogBody.innerHTML = 'Registration successful!';
    // authDialog.open();

    registerButton.disabled = false;
    loader.style.display = 'none';

    window.location='congratulations.html';

  }
  catch(e) {
    console.error('registration failed', e);
    alert('Registration failed');

    // dialogBody.innerHTML = 'Registration failed';
    // authDialog.open();
    
    window.location='index.html';    
  }
};


// const hasCredential = localStorage.getItem('credentialKeyGlass') !== null;

// if(hasCredential) {
//   registerButton.style.display = 'none';
// }
// else {
//   registerButton.style.display = 'block';
// }

registerButton.addEventListener('click', register);

//
// watch special functions
//
function uuidv4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}
