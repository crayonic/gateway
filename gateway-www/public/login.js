// Crayonic Gateway usernameless login
//
// included in /themes/gateway11/login/login-username.ftl
// CHM, 3/2021

//
// required! : make sure 'gateway11' theme is selected as login template
//

//
// 1st login requires username to be entered
function saveUsername() {
    const userName = document.getElementById('kc-form-login').username.value;
    if (userName) {
        localStorage.setItem('loginUsername', userName);
    }

}

function loadUsername() {
    document.getElementById('kc-form-login').username.value = localStorage.getItem('loginUsername');    
}


//
// loads username from username/userhandle pair in usernameless login
//
// TODO for PROD: remove local storage for username/userhandle pairs (should be DB only!)
//
// update 2021-11: DEPRECATED - used only with conjuction with usernameless functionality

function checkUserHandles() {

    const userHandle = localStorage.getItem('login-userHandle');
    // const userHandles = localStorage.getItem('userHandles');
    // const userHandleNames = localStorage.getItem('userHandleNames');
    // let username = '';

    const tmp = window.location.href.substring((window.location.href.search(/\/realms\//g))+8);
    const realm = tmp.substring(0, tmp.search(/\//g));

    // get username

    // if (userHandles.includes(userHandle)) {
    //     let uh = userHandles.split(',');
    //     let uhn = userHandleNames.split(',');

    //     for (var i = 0; i<uh.length; i++) {
    //         if (uh[i] == userHandle) {
    //             console.log('checkUserHandles(): userhandle/username pair found');

    //             username = uhn[i];
    //             localStorage.setItem('loginUsername', username);
    //             document.getElementById("kc-form-login").style.visibility = "hidden";
    //             document.getElementById('kc-form-login').username.value = username;

    //             break;
    //         }    
    //     }
    // }
    // else {
    //     console.log('checkUserHandles(): invalid userhandle');
    //     return;
    // }

    // debug
    // return true;
    
    // read userhandle/user pair from DB via Gateway API
    // TODO: authorization header needs to be added
    $.get(window.location.protocol + '//' + window.location.hostname + '/cgw/credentials/userhandle',
        {
            realm: realm,
            userhandle: userHandle
        },
        function( data ) {
            console.log(`checkUserHandles(): userhandle/username (${data.username}) pair loaded`);

            localStorage.setItem('loginUsername', data.username);
            localStorage.setItem('loginRealm', realm);

            // document.getElementById("kc-form-login").style.visibility = "hidden";
            // document.getElementById('kc-form-login').username.value = data.username;

            return data.username;

        }
    )
    .fail(function(jqXHR, textStatus, error){
        console.log('checkUserHandles(): Something went wrong...' + error );
        return false;
    });

}

function checkAuth() {
    let KeyVault = new Object;
 
    let OSName="Unknown OS";
    if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
    if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
    if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
    if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
 
    // document.write('Your OS: '+ OSName); 
    KeyVault.os = OSName;
 
    var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName  = navigator.appName;
    var fullVersion  = ''+parseFloat(navigator.appVersion); 
    var majorVersion = parseInt(navigator.appVersion,10);
    var nameOffset,verOffset,ix;
 
    // In Opera, the true version is after "Opera" or after "Version"
    if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
    browserName = "Opera";
    fullVersion = nAgt.substring(verOffset+6);
    if ((verOffset=nAgt.indexOf("Version"))!=-1) 
       fullVersion = nAgt.substring(verOffset+8);
    }
    // In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
    browserName = "Microsoft Internet Explorer";
    fullVersion = nAgt.substring(verOffset+5);
    }
    // In Chrome, the true version is after "Chrome" 
    else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
    browserName = "Chrome";
    fullVersion = nAgt.substring(verOffset+7);
    }
    // In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
    browserName = "Safari";
    fullVersion = nAgt.substring(verOffset+7);
    if ((verOffset=nAgt.indexOf("Version"))!=-1) 
       fullVersion = nAgt.substring(verOffset+8);
    }
    // In Firefox, the true version is after "Firefox" 
    else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
    browserName = "Firefox";
    fullVersion = nAgt.substring(verOffset+8);
    }
    // In most other browsers, "name/version" is at the end of userAgent 
    else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
             (verOffset=nAgt.lastIndexOf('/')) ) 
    {
    browserName = nAgt.substring(nameOffset,verOffset);
    fullVersion = nAgt.substring(verOffset+1);
    if (browserName.toLowerCase()==browserName.toUpperCase()) {
    browserName = navigator.appName;
    }
    }
    // trim the fullVersion string at semicolon/space if present
    if ((ix=fullVersion.indexOf(";"))!=-1)
       fullVersion=fullVersion.substring(0,ix);
    if ((ix=fullVersion.indexOf(" "))!=-1)
       fullVersion=fullVersion.substring(0,ix);
 
    majorVersion = parseInt(''+fullVersion,10);
    if (isNaN(majorVersion)) {
    fullVersion  = ''+parseFloat(navigator.appVersion); 
    majorVersion = parseInt(navigator.appVersion,10);
    }
 
    KeyVault.browserName = browserName;
    KeyVault.fullVersion = fullVersion;
    KeyVault.majorVersion = majorVersion;
    KeyVault.appVersion = navigator.appVersion;
    KeyVault.userAgent = navigator.userAgent;
 
    // webauthn support detection
    //  if (window.PublicKeyCredential) {
    //  PublicKeyCredential.isUserVerifyingPlatformAuthenticatorAvailable()
    //    .then((available) => {
    //      if (available) {
    //        document.write("WebAuthn is supported.");
    //      } else {
    //       document.write(
    //          "WebAuthn supported, Platform Authenticator *not* supported."
    //        );
    //      }
    //    })
    //    .catch((err) => document.write("Something went wrong."));
    // } else {
    //    document.write("WebAuthn is NOT supported.");
    // }
 
    KeyVault.iOS = [
     'iPad Simulator',
     'iPhone Simulator',
     'iPod Simulator',
     'iPad',
     'iPhone',
     'iPod'
   ].includes(navigator.platform)
   // iPad on iOS 13 detection
   || (navigator.userAgent.includes("Mac") && "ontouchend" in document);
   
    const ua = navigator.userAgent.toLowerCase();
    KeyVault.isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
 
    KeyVault.platform = (KeyVault.iOS) ? 'iOS' : (KeyVault.isAndroid ? 'Android' : '' );
 
    if (window.PublicKeyCredential) {
       KeyVault.supported = true;
       KeyVault.supportedText = "Crayonic KeyVault&trade; is supported.";
       console.log("Crayonic KeyVault is supported.");
    } else {
       KeyVault.supported = false;      
       KeyVault.supportedText = "Crayonic KeyVault&trade; is NOT supported.";      
       console.log("Crayonic KeyVault is NOT supported.");
    }
 
    // console.log(KeyVault.supportedText);
 
    return KeyVault;
 }
 


window.onload = (event) => {
    
    const urlParams = new URLSearchParams(window.location.search);
    const cguser = urlParams.get('cguser');

    const auth = checkAuth();

    let msg = `Detected OS: ${auth.os}, `;
    if (auth.platform)
       msg += `Platform: ${auth.platform}, `;
 
    msg += `Browser: ${auth.browserName} ${auth.fullVersion}<br>`;
    // msg += `userAgent: ${auth.userAgent}<br>`;
 
    msg += `<span style="color:${auth.supported ? 'green' : 'red'}"><b>${auth.supportedText}</b></span><br>`;
 
    $("#cgw-check").html(msg);

    // disable usernameless login if this scenario is not supported
    
    if (!auth.supported) {
        document.getElementById("webauth").style.visibility = "hidden";
        document.getElementById("kc-form-login").style.visibility = "hidden";
        console.log('login-username: WebAuthn not supported');
        return;
    }

    // localStorage.setItem('login-userHandle', '');


    // autologin - via gateway lite (cguser exists)
    let loginUsername;
    if (cguser) {
        loginUsername = cguser;
    }
    else {
        // autologin - via remember username
        loginUsername = localStorage.getItem('loginUsername');
        // const formUsername = document.getElementById('kc-form-login').username.value;
        // const isUserIdentified = localStorage.getItem('isUserIdentified');

    }


    localStorage.setItem('loginStatus', '');

    if (loginUsername) {        
        document.getElementById('kc-form-login').username.value = loginUsername;
        // localStorage.setItem('loginStatus', 'ok');
    }

    if (localStorage.getItem('login-error')) {
        localStorage.setItem('loginStatus', 'WebAuthn error');
    } 

    // Note: 'error-message' element is located in file template.ftl
    // if (document.getElementById('error-message') == null) {
    if (document.getElementById('kc-error-message')) {
        localStorage.setItem('loginStatus', document.getElementById('kc-error-message'));
    } 

    let errorUsername = '';
    try {
        errorUsername = document.getElementById('input-error-username').innerText.trim();
        localStorage.setItem('loginStatus', errorUsername);
        console.log('login error: ' + errorUsername);
    }
    catch {
        console.log('login: username ok');
    }

    if (document.getElementById('alert-error')) {
        localStorage.setItem('loginStatus', 'WebAuthn error');
    } 

    console.log('login-username loaded');

    //
    // autologin when Resident Keys = on
    //
    // if ((localStorage.getItem('loginStatus') === 'ok') && (loginUsername) && (isUserIdentified === 'yes')) {
    if ((localStorage.getItem('loginStatus') === 'ok') && (loginUsername)) {
        // document.getElementById('kc-login').focus();
        // document.getElementById('kc-form-login').focus();        
        document.getElementById('kc-form-login').submit();
    }    

    // autologin for Gateway Lite
    if (cguser) {
        document.getElementById('kc-form-login').submit();        
    }
};
