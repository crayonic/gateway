// Crayonic Gateway registration
// CHM, 3/2021

// related theme files: /themes/gateway11/login/webauthn-register.ftl


//
// 1st time registration for a given user?
//
function checkFirstTimeRegistration(usernameToRegister) {

    console.log('webauthn-register.flt: registration started');
    localStorage.removeItem('performRestore');

    // 2/2021: restore keyvault flow moved to impersonate (or gateway lite)
    return;

    // Debug/Demo - DB access required in PROD
    const existingUsername = localStorage.getItem('loginUsername');
    const backupExists = (localStorage.getItem('backupPayload')) ? (true) : (false);    // deprecated

    // just set 'performRestore' flag, actual restore operation is done in 'account'
    if (usernameToRegister) {

        // scenarios:
        // 1) returning user = already has backup in DB     -> DO restore
        // 2) first-time user = no backup in DB             -> DON'T do restore

        if (usernameToRegister == existingUsername) {
            if (backupExists) {
                // do restore next time user logs in the account
                localStorage.setItem('performRestore', 'yes');
                console.log('webauthn-register.flt: restore needed flag set');

            } else {
                // backup does not exist -> no restore
                localStorage.removeItem('performRestore');                
            }
        } else {
            // we're registering 1st time user OR different user than the last one logged in
            localStorage.removeItem('performRestore');          
        }

        localStorage.setItem('loginUsername', usernameToRegister);

    }
}

function registrationCompleted() {

    // set policy for this keyvault right after registration
    // prod
    $.get(window.location.protocol + '//' + window.location.hostname + '/cgw/policy', 
        {
            realm: getRealm()
        },
        function( data ) {
            try {
                const policy = data.kvpolicy;
                localStorage.setItem('kvpolicy', data.kvpolicy);    
                
                setPolicy(data.kvpolicy);
                return true;

            }
            catch {
                console.log('setPolicy(): invalid policy retrieved');
                return false;
            }

        }
    )
    .fail(function(jqXHR, textStatus, error){
        // $("#backupResult").html(error);
        console.log('setPolicy(): ' + error );
        return false;
    });


    console.log('webauthn-register.flt: registration completed');

}

function setPolicy(policySettings) {
    let challenge = new Uint8Array([255,24,198,82,136,187,6,153,155,8,39,200,33,59,95,194,98,190,225,187,59,153,191,129,202,152,2,51,97,164,164]);
    let rpId = window.location.hostname;
    let publicKey = {
        rpId : rpId,
        challenge: challenge
    };

    let allowCredentials = [];
    allowCredentials.push({
        // id: fromHexString("FFA10000000000"),
        id: buildSetPolicyCommand(fromHexString(policySettings), 1),
        type: 'public-key',
    });

    publicKey.allowCredentials = allowCredentials;
    publicKey.userVerification = "required";

    navigator.credentials.get({publicKey})
        .then((result) => {
            window.result = result;

            const signature = result.response.signature;            
            if (signature) {

                const payloadHex = buf2hex(signature);
                if (payloadHex.endsWith('9000')) {
                    console.log(`setPolicy(): Keyvault policy (${policySettings}) set. Status: ${payloadHex}`);
                    return true;
                } else {
                    console.log(`setPolicy error: Incorrect response. Status: ${payloadHex}`);
                    return false;
                }
                
            } else {
                console.log(`setPolicy error: Incorrect response. Missing signature.`);
                return false;
            }

        })
        .catch((err) => {
            console.log(`setPolicy error: ${err}`);
            return false;
        })
    ;

}

// helpers
const fromHexString = (hexString) =>
new Uint8Array(hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)));

function buf2hex(buffer) {
    // buffer is an ArrayBuffer
    return Array.prototype.map
        .call(new Uint8Array(buffer), (x) => ("00" + x.toString(16)).slice(-2))
        .join("");
}

// returns active gateway realm based on url
// e.g. account page for gateway realm: https://crayonic.io/auth/realms/gateway/account/#/personal-info
function getRealm() {
    const tmp = window.location.href.substring((window.location.href.search(/\/realms\//g))+8);
    const realm = tmp.substring(0, tmp.search(/\//g));
    return (realm) ? realm : '';
}

// input: settings_bytes (uint array), policy (0=settings, 1=policies)
function buildSetPolicyCommand(settings_bytes, policy) {
    const apdu_payload = Uint8Array.from([...settings_bytes]);

    const apdu_header = Uint8Array.from([
      0xff,
      0xc1,
      policy ? 0x01 : 0x00,
      0x00,
      0x00,
      apdu_payload.length / 256,
      apdu_payload.length % 256
    ]);

    return Uint8Array.from([...apdu_header, ...apdu_payload]);
  }
