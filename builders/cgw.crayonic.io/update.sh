#!/bin/bash

sed -i 's+cgw.crayonic.io+crayonic.io+g' '../../themes/base/account/account.ftl'
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/base/login/login-username.ftl
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/base/login/webauthn-register.ftl
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/gateway/account/index.ftl
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/gateway/account/src/app/content/keyvault-page/KeyvaultPage.js
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/gateway/account/resources/content/keyvault-page/KeyvaultPage.js
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/gateway/login/theme.properties
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/gateway11/account/account.ftl
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/gateway11/login/login-username.ftl
sed -i 's+cgw.crayonic.io+crayonic.io+g' ../../themes/gateway11/login/theme.properties
