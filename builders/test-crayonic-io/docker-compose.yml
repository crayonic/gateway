version: "2.2"

volumes:
  postgres_data:
      driver: local

services:

    postgres:
        image: postgres
        container_name: gateway-db
        restart: unless-stopped
        volumes:
            - postgres_data:/var/lib/postgresql/data
        environment:
            POSTGRES_DB: "gateway"
            POSTGRES_USER: "gateway"
            POSTGRES_PASSWORD: ${DB_PASS}
        ports:
            - 5432:5432
        logging:
            driver: awslogs
            options:
                awslogs-region: us-east-1
                # group must be first created in AWS Cloudwatch. stream is created automatically
                awslogs-group: ${AWSLOGS_GROUP}
                awslogs-stream: "gateway-db"
    
    gateway-local:
        # gateway v15.0.2  = based on jboss/keycloak:15.0.2
        # build: .
        image: crayonic/crayonic-gateway:20220520-01
        container_name: gateway-local
        restart: unless-stopped
        environment:
            KEYCLOAK_USER: "admin"
            KEYCLOAK_PASSWORD: ${KEYCLOAK_PASS}
            DB_VENDOR: POSTGRES
            DB_ADDR: postgres
            DB_DATABASE: gateway
            DB_USER: gateway
            DB_SCHEMA: public
            DB_PASSWORD: ${DB_PASS}
            CGWAPIKEY: ${CGWAPIKEY}
        ports:
          - "8080:8080"
          - "8443:8443"          
        depends_on:
            - postgres
        logging:
            driver: awslogs
            options:
                awslogs-region: us-east-1
                awslogs-group: ${AWSLOGS_GROUP}
                awslogs-stream: "gateway-local"
    
    persephone:
        # build: ./persephone
        image: crayonic/gateway-persephone:0.1.2
        container_name: gateway-persephone
        environment:
            KEYCLOAK_ADDRESS: gateway-local:8080
            KEYCLOAK_USER: "admin"
            KEYCLOAK_PASSWORD: ${KEYCLOAK_PASS}
        depends_on:
            - gateway-local
        logging:
            driver: awslogs
            options:
                awslogs-region: us-east-1
                awslogs-group: ${AWSLOGS_GROUP}
                awslogs-stream: "gateway-persephone"
    
    gateway-api:
        # build: ../gatewayApp/        
        image: crayonic/gateway-api:0.2.13
        container_name: gateway-api
        restart: unless-stopped        
        environment:
            NODE_ENV: "prod"
            NODE_USE_DYNAMIC_RPID: "true"
            NODE_PROTOCOL: "https"
            DB_USER: 'gateway'
            DB_PASS: ${DB_PASS}
            # DB_HOST: 'localhost' in PROD, 'crayonic.io' in DEV, 'gateway-db' in docker-compose
            DB_HOST: gateway-db
            DB_DB: "dbcgw"
            DB_PORT: "5432"
            CGWAPIKEY: ${CGWAPIKEY}
            CGWAPIKEYCHECK: "off"
            KEYCLOAK_HOST: ${KEYCLOAK_HOST}
        volumes:
            - ./tls.crt:/usr/src/app/tls.crt    # make sure tls.crt and tls.key are in the same folder as docker-compose.yml
            - ./tls.key:/usr/src/app/tls.key
        ports:
            - 3000:3000
            - 3010:3010
        depends_on:
            - postgres
        logging:
            driver: awslogs
            options:
                awslogs-region: us-east-1
                awslogs-group: ${AWSLOGS_GROUP}
                awslogs-stream: "gateway-api"
            
networks:
    default:

# prerequisites:
# - nginx set up
# - .env file created with: DB_PASS, KEYCLOAK_PASS, CGWAPIKEY, AWSLOGS_GROUP values
# - tls.crt, tls.key present in case gateway-api is used via https

# build and run:
# sudo docker-compose up --force-recreate --build

# very 1st start:  we need build react code in account theme (steps 1-3), then run docker
# 1) cd themes/gateway/account/src
# 2) npm install && npm run build
# 3) cd ../../../..
# 4) docker-compose up --force-recreate --build

# logging:
# enabled in AWS Cloudwatch/Log Groups/docker-logs/
# to disable logging in AWS, simply remove all 'logging:' sections above

# gateway-db export (sql dump):
# sudo docker exec -t gateway-db pg_dumpall -c -U gateway > dump_$(date +%Y-%m-%d_%H_%M_%S).sql
# or zipped: sudo docker exec -t gateway-db  pg_dumpall -c -U gateway | gzip > ./dump_$(date +"%Y-%m-%d_%H_%M_%S").gz

# gateway-db import (sql):
# cat dump-file.sql | docker exec -i gateway-db psql -U gateway
# or zipped: gunzip < dump-file.sql.gz | docker exec -i gateway-db psql -U gateway -d gateway
