from time import sleep
import os, sys
from pprint import pprint
import tempfile
import traceback


from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakError, KeycloakGetError

BASEDIR = os.path.dirname(os.path.realpath(__file__))

keycloak_address = os.environ.get("KEYCLOAK_ADDRESS", "http://localhost:8080/auth/")
keycloak_user = os.environ.get("KEYCLOAK_USER", "api").strip()
keycloak_password = os.environ.get("KEYCLOAK_PASSWORD", "admin").strip()

# print("Creds:", keycloak_user, keycloak_password)

def cert_config_content(challenge_password, cert_type, name, login, domain): 
    # TODO check for cnf injection by filtering newline characters

    def safestring(s):
        return s.replace('\n', ' ').replace('\r', '')

    def domain_section():
        return "\r\n".join(
            str(i) + ".domainComponent = " + safestring(part)
            for i, part 
            in enumerate(reversed(domain.split('.')))
        )

    content = f"""#
# OpenSSL configuration file
#

NAME = {safestring(name)}
LOGIN = {safestring(login)}
CHALLENGE_PASSWORD = {safestring(challenge_password)}

[ req ]
distinguished_name = req_distinguished_name
attributes = req_attributes
req_extensions = req_ext
utf8 = no
string_mask = nombstr
prompt = no

[ req_distinguished_name ]
commonName = $NAME
{domain_section()}

[ req_attributes ]
challengePassword = $CHALLENGE_PASSWORD

"""
    if cert_type == "SIGNATURE":
        content += """
[ req_ext ]
keyUsage = critical, digitalSignature 
subjectAltName = otherName:msUPN;UTF8:$LOGIN"""
    elif cert_type == "ENCRYPTION":
        content += """
[ req_ext ]
keyUsage = critical, keyEncipherment, dataEncipherment"""
    elif cert_type == "SIGNATUREENCRYPTION":
        content += """
[ req_ext ]
keyUsage = critical, digitalSignature, keyEncipherment, dataEncipherment
subjectAltName = otherName:msUPN;UTF8:$LOGIN"""

    return content  

keycloak_admin = None
admin_cache = {}
problematic_users = set()

def enroll_cert(scep_uri, challenge_password, cert_type, name, login, domain):

    # Prepare tmp directory INDOR, OUTDIR - can be same
    # TODO
    with tempfile.TemporaryDirectory() as tmpdir:
        # tmpdir = '/tmp/pers'
        SCEP_URI = scep_uri
        INDIR = tmpdir
        OUTDIR = tmpdir
        print("Creating tmpdir", tmpdir)
        # Prepare cert_config.cnf with injected SECRET(CSR secret), NAME(firstName+lastName), LOGIN(emailaddress), keyUsage (keyEncipherment, digitalSignature or keyEncipherment+digitalSignature)
        # TODO
        with open(os.path.join(INDIR, "cert_config.cnf"), "w") as cert_config:
            cert_config.write(cert_config_content(challenge_password, cert_type, name, login, domain))


        # Run with injected variables INDIR, OUTDIR, SCEP_URI
        stream = os.popen(f'{BASEDIR}/sscep/requestcert.sh "{SCEP_URI}" "{INDIR}" "{OUTDIR}"')
        output = stream.read()
        print(output)

        # Read the cert and private key, store it to user account or return
        # TODO
        with open(os.path.join(OUTDIR, "cert.key"), "r") as cert_key:
            cert_key_content = cert_key.read()
        with open(os.path.join(OUTDIR, "cert.crt"), "r") as cert_crt:
            cert_crt_content = cert_crt.read()
        
        # print("cert_key_content", cert_key_content)
        # print("cert_crt_content", cert_crt_content)

        return cert_key_content, cert_crt_content



def poll_keycloak():
    global keycloak_admin, admin_cache, problematic_users
    print(keycloak_admin, admin_cache)
    if keycloak_admin is None:
        keycloak_admin = KeycloakAdmin(
            server_url=f"{keycloak_address}",
            username=keycloak_user,
            password=keycloak_password,
            realm_name="gateway",
            user_realm_name="master",
            client_secret_key="",
            verify=False
        )
        keycloak_admin.auto_refresh_token = ['get', 'post', 'put', 'delete']
    
    # Get users Returns a list of users, filtered according to query parameters
    realms = keycloak_admin.get_realms()
    # print(realms)

    for realm in realms:
        print('Realm:', realm['realm'])
        # pprint(realm.get('attributes'))
        scep_uri = realm.get('attributes').get('certificationAuthoritiesScepConnectionUri')
        scep_enabled = realm.get('attributes').get('certificationAuthoritiesScepEnabled', "false") != "false"
        challenge_password = realm.get('attributes').get('certificationAuthoritiesScepCsrSharedPassword', '')
        domain = realm.get('attributes').get('certificationAuthoritiesScepDomain', 'domain.example.com')
        piv_mapping = {
            'SIGNATURE': realm.get('attributes').get('certificationAuthoritiesScepSignaturePivSlot', ''),
            'ENCRYPTION': realm.get('attributes').get('certificationAuthoritiesScepEncryptionPivSlot', ''),
            'SIGNATUREENCRYPTION': realm.get('attributes').get('certificationAuthoritiesScepSignatureEncryptionPivSlot', ''),
        }
        if scep_enabled and scep_uri and challenge_password:
            # Get users Returns a list of users, filtered according to query parameters
            realm_admin = admin_cache.get(realm['realm'])
            if not realm_admin:
                realm_admin = KeycloakAdmin(
                    server_url=f"{keycloak_address}",
                    username=keycloak_user,
                    password=keycloak_password,
                    realm_name=realm['realm'],
                    user_realm_name="master",
                    client_secret_key="",
                    verify=False
                )
                realm_admin.auto_refresh_token = ['get', 'post', 'put', 'delete']
                admin_cache[realm['realm']] = realm_admin
            users = realm_admin.get_users({})
            for user in users:
                if user['id'] in problematic_users:
                    continue
                # print(user.get('username'), user.get('attributes'))
                # pprint(user)
                first_name = user.get('firstName')
                last_name = user.get('lastName')
                email = user.get('email')
                userRecord = user
                userRecordAttributesUpdated = False
                try:
                    if first_name and last_name and email: # TODO also check emailVerified
                        name = f"{first_name} {last_name}"
                        login = email
                        for cert_type in piv_mapping:
                            piv_slot = piv_mapping[cert_type]
                            if piv_slot and not user.get('attributes', {}).get(f'cert_pivslot_{piv_slot}_crt'):
                                print("\n\nEnrolling user",realm['realm'], first_name, last_name, email)
                                print(scep_uri, scep_enabled)
                                cert_key_content, cert_crt_content = enroll_cert(scep_uri, challenge_password, cert_type, name, login, domain)
                                userRecord['attributes'] = {
                                    **userRecord.get('attributes', {}),
                                    f'cert_pivslot_{piv_slot}_crt': cert_crt_content,
                                    f'cert_pivslot_{piv_slot}_key': cert_key_content
                                }
                                userRecordAttributesUpdated = True
                except Exception as e:
                    print(type(e), e) 
                    traceback.print_exc(file=sys.stdout)
                if userRecordAttributesUpdated:
                    print(userRecord)
                    try:
                        realm_admin.update_user(user['id'], {"attributes": userRecord.get("attributes")})
                    except KeycloakGetError as e:
                        traceback.print_exc(file=sys.stdout)
                        print(type(e), e)
                        problematic_users.add(user['id'])
                        print(problematic_users)


def serve():
    global keycloak_admin, admin_cache
    while True:
        try:
            poll_keycloak()
        except KeycloakError as e:
            traceback.print_exc(file=sys.stdout)
            print(type(e), e) 
            keycloak_admin = None
            admin_cache = {}
        print("Persephone is going to sleep", flush=True)
        for _ in range(10):
            sleep(1)


if __name__ == "__main__":
    # enroll_cert()
    serve()
    # poll_keycloak()