#!/bin/bash
set -e

# Interface
SCEP_URI=$1
INDIR=$2
OUTDIR=$3

BASEDIR=$(dirname "$0")
echo "$BASEDIR"

# INDIR=${BASEDIR}
# OUTDIR=$(pwd)/out
PASSWD=Crayonic

echo Generating private keys
openssl genrsa -out "${OUTDIR}/cert.key" 2048

echo Generating certificate requests
openssl req -config ${INDIR}/cert_config.cnf -new -key "${OUTDIR}/cert.key" -out "${OUTDIR}/cert.csr"

# echo Showing certificate requests
# REM certutil -dump "${OUTDIR}/cert.csr"
# REM certutil -dump "${OUTDIR}/keyvault-encryption.csr"

echo "Retrieving the CA and RA certificates from SCEP/NDES (only during GW install)"
"${BASEDIR}/sscep" getca -u "${SCEP_URI}" -c "${OUTDIR}/ca.crt"
mv "${OUTDIR}/ca.crt-0" "${OUTDIR}/ra-sig.crt"
mv "${OUTDIR}/ca.crt-1" "${OUTDIR}/ra-enc.crt"
mv "${OUTDIR}/ca.crt-2" "${OUTDIR}/ca.crt"

echo Enrolling new certificates using SCEP/NDES
"${BASEDIR}/sscep" enroll -u "${SCEP_URI}" -k "${OUTDIR}/cert.key" -r "${OUTDIR}/cert.csr" -l "${OUTDIR}/cert.crt" -c "${OUTDIR}/ra-sig.crt" -e "${OUTDIR}/ra-enc.crt"

echo Combining private keys with certificates
openssl pkcs12 -export -out "${OUTDIR}/cert.pfx" -inkey "${OUTDIR}/cert.key" -in "${OUTDIR}/cert.crt" -passout "pass:%PASSWD%"