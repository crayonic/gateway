# Clean Crayonic Gateway installation on AWS EC2/Ubuntu 20.04 (docker)

### helpers:

apt list --upgradable -a

host -t ns crayonic.io
dig crayonic.io
nslookup crayonic.io

### create ec2 instance (AWS):
t3.xlarge: ubuntu 20.04 lts x64, 4 cpu, 16gb ram, 64gb disk space
t3.2xlarge: ubuntu 20.04 lts x64, 8 cpu, 32gb ram, 128gb disk space
 
ubuntu-gateway-main2 20.04:
crayonic-io-orig: ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@3.237.34.199
crayonic-io: ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-224-207-171.compute-1.amazonaws.com
cgw-crayonic-io: ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-167-189-196.compute-1.amazonaws.com


ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@3.80.87.247

sudo su -

### install docker
https://docs.docker.com/engine/install/ubuntu/

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo apt  install docker-compose

### Configure Docker to start on boot

sudo systemctl enable docker.service
sudo systemctl enable containerd.service


### install nginx
https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04

sudo apt update
sudo apt install nginx
sudo ufw app list
sudo systemctl enable nginx

curl 54.167.189.196

change config:
sudo nano /etc/nginx/conf.d/default.conf

default www:
nano /etc/nginx/sites-available/default
/var/www/html

upload gateway www to root www page:
sudo su -
cd /usr/share/nginx/
chown ubuntu:root html

copy www files from localhost to server

sudo nginx -t
sudo systemctl restart nginx


### install SSL
https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx

sudo snap install core; sudo snap refresh core
sudo apt-get remove certbot
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot --nginx

sudo certbot renew --dry-run


### install node

sudo apt update
sudo apt install nodejs
sudo apt install npm

check node version and if lower than v10.x, install v14: (tested with gateway-api and gateway-lite. note that as of 23.7.2021 node v16 does not work with gateway-lite due to fido2 library dependency issues)
node -v

cd ~
curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install -y nodejs

### install redis-server (for gateway-lite)
https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04

install netstat:
sudo apt install net-tools

sudo apt update
sudo apt install redis-server
sudo nano /etc/redis/redis.conf , ctrl+w supervised, change ‘no’ to ‘systemd’, ctrl+x
sudo systemctl restart redis.service
sudo systemctl status redis



### Gateway autostart after system reboot

sudo systemctl enable cron.service
sudo crontab- e
@reboot sleep 60 && cd /home/ubuntu/gateway && sudo docker-compose up --force-recreate --build
@reboot sleep 60 && cd /home/ubuntu/gateway-lite && npm run prod

#### test reboot

reboot instance (AWS)
ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-167-189-196.compute-1.amazonaws.com
netstat -tulpn


### copy gateway images

prepare on original server:
tar cfz gateway.tgz gateway &
tar cfz gatewayApp.tgz gatewayApp &
tar cfz gateway-lite.tgz gateway-lite &

docker commit gateway-db gateway-db-container
docker save gateway-db-container | gzip > gateway-db-container.tgz


download to local machine:

crayonic-io: sftp -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-224-207-171.compute-1.amazonaws.com
cgw-crayonic-io: sftp -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-167-189-196.compute-1.amazonaws.com
crayonic-io-original: sftp -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@3.237.34.199

lcd Downloads
get *.tgz
exit

upload from local machine to target machine:

gateway-ubuntu-main: sftp -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-167-189-196.compute-1.amazonaws.com
crayonic-io: sftp -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@3.80.87.247

lcd Downloads
put *.tgz
exit

ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-167-189-196.compute-1.amazonaws.com
tar xvf gatewayApp.tgz
tar xvf gateway-lite.tgz
tar xvf gateway.tgz

sudo su -
cd ~ubuntu

gunzip -c gateway-db-container.tgz | docker load
docker images
docker tag IMAGE_ID postgres:latest
docker rmi gateway-db-container


### backup and restore DB from docker container

on orig server:
ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@3.237.34.199
docker exec -t gateway-db pg_dumpall -c -U gateway > dump_$(date +%Y-%m-%d_%H_%M_%S).sql

restore on new server:
ssh -i "/Users/marek/.ssh/crayonic_io.pem" ubuntu@ec2-54-224-207-171.compute-1.amazonaws.com

sudo docker run --name gateway-db -e POSTGRES_USER=gateway -e POSTGRES_PASSWORD=g6Ja#9R^#gR2z%kD -e POSTGRES_DB=gateway -d postgres

cat your_dump.sql | docker exec -i gateway-db psql -U gateway -d gateway

docker stop gateway-db


### update CGW configuration

nano gateway/docker-compose.yml
follow steps in docker-compose.yml remarks section:
cd /home/ubuntu/gateway/themes/gateway/account/src
npm install && npm run build

cd /home/ubuntu/gateway/themes/keycloak/common/resources/web_modules/@patternfly/react-core/dist/styles
nano base.css
ctrl+w x2, iframe - remove line where defined height: auto (ctrl+k), ctrl+x
cd /home/ubuntu/gateway/themes/gateway/account/src/node_modules/@patternfly/react-core/dist/styles
nano base.css
ctrl+w x2, iframe - remove line where defined height: auto (ctrl+k), ctrl+x
cd ~/gateway
sudo docker-compose up --force-recreate --build


### install and run Gateway

cd ~/gateway/
sudo docker-compose up --force-recreate --build

stop all running containers:
sudo docker-compose down
or
docker kill $(docker ps -q)


### install and run Gateway Lite

cd ~/gateway-lite/
npm install
npm run prod

